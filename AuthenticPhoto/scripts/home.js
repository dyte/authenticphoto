﻿//set up urls for ajax forms
var actionProcessImage = "/Umbraco/Surface/Upload/ProcessImage";
var actionProcessImages = "/Umbraco/Surface/Upload/ProcessImages";
var actionGetImageInfo = "/Umbraco/Surface/Upload/GetImageInfo";
var actionSwitchImagePreferences = "/Umbraco/Surface/ProductTool/SwitchImagePreferences";
var actionDeleteImageFromOrder = "/Umbraco/Surface/ProductTool/DeleteImageFromOrder";
var actionSaveImagePreferences = "/Umbraco/Surface/ProductTool/SaveImagePreferences";
var actionSaveImagePreferencesCalc = "/Umbraco/Surface/ProductTool/SaveImagePreferencesCalc";
var actionInitializePreferences = "/Umbraco/Surface/ProductTool/InitializePreferences";
var actionGetOrderCount = "/Umbraco/Surface/ProductTool/GetOrderCount";
var actionGetImagesCountInOrder = "/Umbraco/Surface/ProductTool/GetImagesCountInOrder";
var actionSelectProduct = "/Umbraco/Surface/ProductTool/SelectProduct";
var actionSelectProductCalc = "/Umbraco/Surface/ProductTool/SelectProductCalc";
var actionCheckout = "/Umbraco/Surface/ProductTool/Checkout";
var actionSaveActiveImageAndCheckout = "/Umbraco/Surface/ProductTool/SaveActiveImageAndCheckout";
var actionLogin = "/Umbraco/Surface/AAccount/Login";
var actionResetPassword = "/Umbraco/Surface/AAccount/ResetPassword";
var actionEditUserForm = "/Umbraco/Surface/AAccount/EditUserForm";
var actionEditUser = "/Umbraco/Surface/AAccount/EditUser";
var actionLogout = "/Umbraco/Surface/AAccount/LogOff";
var actionResendConfirmationCodeEmail = "/Umbraco/Surface/AAccount/ResendConfirmationCodeEmail";
var actionCheckVat = "/Umbraco/Surface/ProductTool/CheckVat";
var actionGetProcessedImagesAndTotalImageCount = "/Umbraco/Surface/ProductTool/GetProcessedImagesAndTotalImageCount";
var actionUpdateOrderWithCheckoutProducts = "/Umbraco/Surface/ProductTool/UpdateOrderWithCheckoutProducts";
var actionUpdateOrderWithDeliveryOption = "/Umbraco/Surface/ProductTool/UpdateOrderWithDeliveryOption";
var actionUpdateOrderWithPaymentOption = "/Umbraco/Surface/ProductTool/UpdateOrderWithPaymentOption";
var actionCompletedAccount = "/Umbraco/Surface/ProductTool/CompletedAccount";
var actionNewsletterFormSubmitted = "/Umbraco/Surface/FormSubmission/NewsletterFormSubmitted";
var actionQuikregister = "/umbraco/Surface/AAccount/Quickregister";
//var actionOptionTypeSetCalculator = "/Umbraco/Surface/ProductTool/OptionTypeSetCalculator";
var actionStartPollingForCompletedImages = "/Umbraco/Surface/Upload/StartPollingForCompletedImages";

$(document).ready(function () {

    $('html').off().on("click", function (e) {

        var idTarget = $(e.target).attr("class");
        if (!$(e.target).hasClass("prefblock") && !$(e.target).parents(".sign_dropdownbox").length) {
            $(".menu").each(function () { $(this).removeClass("showMenu") });
        }
        if (idTarget != "authPopup" && idTarget != "loginLink" && !$(e.target).hasClass("authPopup") && !$(e.target).parents("#ui-datepicker-div").length && !$(e.target).parents("#authPopup").length && $(e.target).attr("id") != "btnCheckout") {
            hideAuthPopup();
        }
        if ($("#dpi").tooltipster("status") == "shown" && !$(e.target).closest('.tooltipster-base').length) {
            hideDpiTooltip();
        }
        if (
            !!($("#productPicker").length) && $("#productPicker").css('display') != 'none'
            && !$(e.target).parents(".productPicker").length
            && !$(e.target).parents(".orderStep").length
            && $(e.target).attr("id") != "liPrefsFinishing"
            && !$(e.target).hasClass("lnkToFinishing")
            && !$(e.target).parents(".controlArrows").length
            ) {
            hideProductSelector();
        }
        $(".optionPicker").each(function () {
            if (
                !!($(this).length) && $(this).css('display') != 'none'
                && !$(e.target).parents("#" + $(this).attr("id")).length
                && !$(e.target).hasClass("orderStep")
                && !$(e.target).parents(".orderStep").length
                && $(e.target).attr("id") != "liPrefsFinishing"
                && !$(e.target).hasClass("lnkToFinishing")
                && !$(e.target).parents(".controlArrows").length
                ) {
                hideOptionSelector($(this).data("optiontypeid"));
            }
        });
    });

    $(".a-login").on("click",
        function(e) {
            e.preventDefault();
            $.ajax({
                url: actionQuikregister,
                type: "GET",
                success: function (resp) {
                    $("#login").html(resp);
                }
            });
        });

    clientSideValidationAddition();
    updateBasket();
    initLanguageSelector();

    $(".dropdown").hover(
        function () {
            //$('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideDown("400");
            $(this).toggleClass('open');
        },
        function () {
            //$('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideUp("400");
            $(this).toggleClass('open');
        }
    );
});

function clientSideValidationAddition() {
    jQuery.validator.addMethod("enforcetrue", function (value, element, param) {
        return element.checked;
    });
    jQuery.validator.unobtrusive.adapters.addBool("enforcetrue");
}

function getTranslation(key) {
    return $("input[name='str" + key + "']").val();
}

function initLanguageSelector() {
    $("#languageSelector").change(function () {
        var homepage = $(this).find('option:selected', this).data('homepage');
        window.location.href = homepage;
    });
}

function updateBasket() {
    $.ajax({
        type: "POST",
        url: actionGetImagesCountInOrder,
        success: function (data) {
            if (data != null && data != "0") {
                $("#lblBasketCount").show();
                $("#lblBasketCount").html(data);
                $("#liBasket").css("cursor", "pointer");
                $("#liBasket").off().on("click",function () { redirectToOrderStep(2) });
                $("#btnBestel").html($("#btnBestel").data("val-connecttoexisting"));
            } else {
                $("#lblBasketCount").hide();
                $("#liBasket").click(null);
            }
        }
    });
}

function redirectToOrderStep(stepNumber) {
    var step;
    switch (stepNumber) {
        case 3:
            step = "completeorder";
            break;
        case 2:
            step = "productselection";
            break;
        default:
            step = "upload";
            break;
    }
    window.location.href = $("#umbracoLangRoot").val() + step;
}

function showAuthPopup(inCheckoutFlow) {
    //hideAuthSection();
    if (!!inCheckoutFlow) {        
        $("#authPopupMsg").html(getTranslation("Home.PleaseLogin"));
        $("#incheckoutflow").val(1);
        $("#authPopupMsg").show();
    } else {        
        $("#authPopupMsg").hide();
    }
    $("#authPopup").slideDown(500, "easeOutQuart");
}

function resetAuthPopup() {
    $.ajax({
        type: "POST",
        url: actionLogin,
        success: function (data) {
            $("#authPopupContent").empty().append(data);
            hideLoginOverlay();
            $("#authPopup").css("width", "450px");
        }
    });
}

function hideAuthPopup() {
    $("#authPopup").slideUp(200, "easeInQuart");
}

function growAuthPopup() {
    $("#authPopup").stop().animate({ width: "800px" }, 400, "easeOutQuart");
}

function shrinkAuthPopup() {
    $("#authPopup").stop().animate({ width: "450px" }, 400, "easeOutQuart");
}

function resendActivationLink(userid) {
    var data = { userId: userid };
    $.ajax({
        type: "POST",
        url: actionResendConfirmationCodeEmail,
        data: data,
        success: function (result) {
            $("#authPopupContent").empty().append(result);
            hideLoginOverlay();
            showMessage(getTranslation("Home.ConfirmationSent1") + "<br/>" + getTranslation("Home.ConfirmationSent2"), null, 13, "Ok");
        }
    });
    showLoginOverlay();
}

function showEditUserForm(UserID) {
    var data = { UserID: UserID, CheckoutContext: true };
    showAuthSection();
    $("#imgAuthSectionLoading").fadeIn();
    $("#btnSaveUserData").hide();
    $.ajax({
        type: "POST",
        data: data,
        url: actionEditUserForm,
        success: function (data) {
            $("#phEditUserForm").empty().append(data);
            $(".btnEditUserCollapse").off().on("click", function () { hideAuthSection(); });
            initEditUserForm();
            $("#imgAuthSectionLoading").fadeOut();
            $("#btnSaveUserData").show();
            $("#authSection").css("height", "auto");
        }
    });
}

function initEditUserForm() {
    $("#btnSaveUserData").off().on("click", function () {
        if (reqFieldsFeedback(false)) {
            activateReqFieldsFeedback();
        }
        else {
            var $btn = $(this);
            var $form = $(this).closest('form');
            $btn.css("background-color", "#848484");
            $("#imgEditUserLoading").show();

            var data = $form.serialize() + "&CheckoutContext=true"

            $.ajax({
                type: "POST",
                url: actionEditUser,
                data: data,
                error: function (xhr, status, error) {
                    $("#EditUserLoader").hide();
                },
                success: function (response) {
                    $btn.css("background-color", "");
                    $("#imgEditUserLoading").hide();
                    hideAuthSection();
                }
            });
        }
    });
}

function activateReqFieldsFeedback() {
    $("#selectTitle").blur(function () {
        reqFieldsFeedback(false);
    });
    $(".req-input").blur(function () {
        reqFieldsFeedback(false);
    });
}

function reqFieldsFeedback(inclOrderPrefs) {

    var reqMissing = false;

    if ($("#selectTitle").val() == "0") {
        $("#selectTitle").css("border", "1px solid red");
        $("#selectTitle").closest("tr").find(".control-label").first().css("color", "red");
        reqMissing = true;
    } else {
        $("#selectTitle").closest("tr").find(".control-label").first().css("color", "");
        $("#selectTitle").css("border", "");
    }

    $(".req-input").each(function () {
        if ($(this).val().trim() == "") {
            $(this).css("border", "1px solid red");
            $(this).closest("tr").find(".control-label").first().css("color", "red");
            reqMissing = true;
        } else {
            $(this).closest("tr").find(".control-label").first().css("color", "");
            $(this).css("border", "");
        }
    });

    if ($("#tbConfirmPassword").val() != $("#tbPassword").val()) {
        $("#tbConfirmPassword").css("border", "1px solid red");
        $("#tbConfirmPassword").closest("tr").find(".control-label").first().css("color", "red");
        $("#tbPassword").css("border", "1px solid red");
        $("#tbPassword").closest("tr").find(".control-label").first().css("color", "red");
        reqMissing = true;
    } else {
        if ($("#tbConfirmPassword").val().trim() != "") {
            $("#tbConfirmPassword").closest("tr").find(".control-label").first().css("color", "");
            $("#tbConfirmPassword").css("border", "");
        }
        if ($("#tbPassword").val().trim() != "") {
            $("#tbPassword").closest("tr").find(".control-label").first().css("color", "");
            $("#tbPassword").css("border", "");
        }
    }
    
    if (inclOrderPrefs && $("#divOrderPreferences").html().trim().length != 0) {
        if ($("input[name='delivery']:checked").length == 0) {
            $("#deliveryTitle").css("color", "red");
            $("#deliveryFeedback").show();
            reqMissing = true;
        } else {
            $("#deliveryFeedback").hide();
            $("#deliveryTitle").css("color", "");
        }
        if ($("input[name='payment']:checked").length == 0) {
            $("#paymentTitle").css("color", "red");
            $("#paymentFeedback").show();
            reqMissing = true;
        } else {
            $("#paymentFeedback").hide();
            $("#paymentTitle").css("color", "");
        }
    }
    return reqMissing;
}

function showAuthSection() {
    hideAuthPopup();
    $("#authSection").show();
    $("#authSection").animate({ height: "729px" }, 400, "easeOutQuart")
}
function hideAuthSection() {
    $("#authSection").slideUp(500, "easeOutQuart", function () {
        $("#authSection").hide();
    });
}

function showInfo(infoHtml) {
    $.msg({
        autoUnblock: false,
        clickUnblock: false,
        content: infoHtml
    });
}

function showMessage(msg, beforeUnblockCallback, fontSizePt, closeBtnTxt) {
    $.msg({
        autoUnblock: false,
        clickUnblock: false,
        content:
            '<p>' + msg + '</p>' +
               '<p class="btn-wrap">' +
                 '<span id="ok" class="btn btn-success" style="float:right; padding: 8px">' + closeBtnTxt + '</span>' +
               '</p>',
        afterBlock: function () {
            var self = this;
            $('#ok').bind('click', function () {
                self.unblock();
            });
        },
        beforeUnblock: function () {
            if (beforeUnblockCallback != null) {
                beforeUnblockCallback();
            }
        }
    });
    $("#jquery-msg-content").css("font-size", fontSizePt + "pt");
    $(window).trigger('resize');
}

function initRegisterPane(checkoutContext, country) {

    $("#selectCountry").val(country);

    $("#tbRegBirthday").datepicker({
        yearRange: "-90:-10",
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $(".tbPhoneNumberCountryCode").intlTelInput({
        preferredCountries: ["be", "fr", "nl", "lu", "de", "gb", "us"],
        nationalMode: false,
        initialCountry: "be"
    });
    $(".tbPhoneNumberCountryCode").blur(function () {
        if ($(this).val().length > 0 && $(this).val().substr(0, 1) != "+") {
            $(this).val("+" + $(this).val());
        }
    });
    $("#vatNr").blur(function () {
        checkVatIfNeeded(false);
    });
    $("#ddlCountryCodes").change(function () {
        checkVatIfNeeded(checkoutContext);
    });
    checkVatIfNeeded(checkoutContext);

    $("#btnCloseRegisterPane").off().on("click", function () {
        $("#authSection").hide();
        $("#authSection").css("height", 0);
    });
}

function checkVatIfNeeded(returnSummary) {
    var vatcode = $("#vatCC").val();
    var vatnr = $("#vatNr").val();
    if (vatcode != null && vatnr != null && vatcode.trim().length > 0 && vatnr.trim().length > 0) {
        checkVat(vatcode, vatnr, returnSummary);
    }
}

function checkVat(vatcode, vatnr, returnSummary) {
    $("#vatSuccess").hide();
    $("#vatFail").hide();
    $("#vatLoader").show();
    var data = { vatCC: vatcode, vatNr: vatnr, returnSummary: returnSummary };
    $.ajax({
        type: "POST",
        url: actionCheckVat,
        data: data,
        success: function (result) {

            var success = false;

            if (result == "0" || result == "1") { // simple content result
                success = (result == "1");
            }
            else { // multipart result
                $("#phOrderSummary").empty().append(result);
                success = $("#VatValid").val() != null && $("#VatValid").val().toLowerCase() == "true";
            }

            if (success) {
                $("#vatSuccess").show();
                $("#vatFail").hide();
            } else {
                $("#vatSuccess").hide();
                $("#vatFail").show();
            }


        },
        complete: function () {
            $("#vatLoader").hide();
        }
    });
}

function initQuickRegisterPane() {
    $("#tbRegBirthday").datepicker({
        yearRange: "-90:-10",
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $(".tbPhoneNumberCountryCode").intlTelInput({
        preferredCountries: ["be", "fr", "nl", "lu", "de", "gb", "us"],
        nationalMode: false,
        initialCountry: "be"
    });
    $(".tbPhoneNumberCountryCode").blur(function () {
        if ($(this).val().length > 0 && $(this).val().substr(0, 1) != "+") {
            $(this).val("+" + $(this).val());
        }
    });
    $("#vatNr").blur(function () {
        checkVatIfNeeded(false);
    });
    $("#ddlCountryCodes").change(function () {
        checkVatIfNeeded(false);
    });
    checkVatIfNeeded(false);
}

function showLoginOverlay() {
    $("div#overlay_login").fadeIn("fast");
}

function hideLoginOverlay() {
    $("div#overlay_login").fadeOut("fast");
}


function loginFeedback(data) {
    if (data.indexOf("lblSuccess") != -1) {
        $('#authPopupMsg').hide();
        var nextStep = data.indexOf("incheckoutflow") == -1 ? 2 : 3;
        redirectToOrderStep(nextStep);
    } else {
        hideLoginOverlay();
    }
}

function doResetPasswordFeedback(data) {
    if (data.indexOf("lblSuccess") != -1) {
        $('#authPopupMsg').hide();
        var returnviewparam = $("#returnviewparam").val();
        window.location.href = window.location.pathname;
    } else {
        hideLoginOverlay();
    }
}

function registerFeedback(data) {
    if (data.indexOf("lblSuccess") != -1) {
        $('#authPopupMsg').hide();
        hideAuthPopup();
        resetAuthPopup();
        showMessage(getTranslation("Home.ConfirmationSent1") + "<br/>" + getTranslation("Home.ConfirmationSent2"), null, 13, "Ok");
    } else {
        hideLoginOverlay();
    }
}

function forgotPasswordFeedback(data) {
    if (data.indexOf("lblSuccess") != -1) {
        $('#authPopupMsg').hide();
        hideAuthPopup();
        resetAuthPopup();
        showMessage(getTranslation("Home.ResetPasswordCheckMail"), null, 13, "Ok");
    } else {
        hideLoginOverlay();
    }
}

function MultipartialUpdate(views, callback) {
    for (v in views) {
        if (views[v].script) {
            eval(views[v].script);
        }
        else {
            $('#' + views[v].updateTargetId).empty().append(views[v].html);
        }
    }
    if (callback != null)
        callback();
    return false;
}

function showResetPasswordPopup() {
    var data = { userId: $("#rp_userId").val(), code: $("#rp_code").val() };
    $.ajax({
        type: "POST",
        url: actionResetPassword,
        data: data,
        success: function (result) {
            $("#authPopupContent").empty().append(result);
            hideLoginOverlay();
        }
    });
}

function postbackAction(action) {
    switch (action) {
        case "PasswordReset":
            showAuthPopup(false);
            showLoginOverlay();
            showResetPasswordPopup();
            break;
    }
}