﻿function initCalculator() {
    addPrototypeExtensions();
    initDimInputs();
    initExtraInputs();

    initSelectors();

    //selectActiveFamily();
    putSliderPickersInPlace();

    setupProductStepHover();
    setOrderStepHoverState("#productStep", true);
    setupOptionStepHovers();

    refreshPriceInterface();
    disableIllegalOptions();

    finetuneLayout();

}

function initSelectors() {
    initProductSelector();
    initOptionSelectors();
}

function finetuneLayout() {
    $("#col_img").css("height", $("#col_prefs").css("height"));
}

function showOptionTypesLoading(txt) {
    $("#oTypesloadingText").html(txt);
    $("#divOptionTypesLoading").fadeIn();
}
function hideOptionTypesLoading() {
    $("#divOptionTypesLoading").fadeOut();
}

//function refreshOptionTypeSet() {
//    var data = { sessionID: $("#SessionID").val(), orderID: $("#OrderID").val() };
//    $.ajax({
//        type: "POST",
//        url: actionOptionTypeSetCalculator,
//        data: data,
//        success: function (data) {
//            $("#optionTypesDiv").empty().append(data);
//        }
//    });
//}


function addPrototypeExtensions() {
    if (!String.prototype.format) {
        String.prototype.format = function () {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number) {
                return typeof args[number] != 'undefined'
                  ? args[number]
                  : match
                ;
            });
        };
    }
}

//function selectActiveFamily() {
//    var famID = $("#ActiveFamily").val();
//    $("ul.nav li a[data-familyid='" + famID + "']").toggleClass("selectedFamily");
//}

function initDimInputs() {
    $(".dimInput").each(function () {
        $(this).tooltipster(
        {
            content: getTranslation("Calculator.EnterValidDim")
        });
        $(this).tooltipster("disable");
        $(this).blur(function () {
            var inp = $(this).val().replace(",", ".");
            var err = false;
            if (inp.trim().length > 0) {
                var inpParsed = parseFloat(inp);
                if (inpParsed == inp) {
                    if (inpParsed <= 0) {
                        err = true;
                    }
                } else {
                    err = true;
                }
                if (err) {
                    $(this).tooltipster("enable");
                    $(this).tooltipster("show");
                    $(this).attr("data-valid", "0");
                } else {
                    $(this).tooltipster("disable");
                    $(this).attr("data-valid", "1");
                    refreshPriceInterface();
                    saveImagePreferencesCalc();
                }
            } else {
                $(this).attr("data-valid", "0");
            }
        });
    });
}

function initExtraInputs() {
    $(".extra-checkbox").off().on("click", function (e) {
        $(".extra-checkbox[data-extra != \"" + $(e.target).data("extra") + "\"]").prop("checked", false);
        refreshPriceInterface();
        saveImagePreferencesCalc();
    });
}

function initProductSelector() {
    $("#productStep").off().on("click", function (e) {
        e.stopPropagation();

        showProductSelector();

        $(this).find(".dropdownbox-active").removeClass("dropdownbox-active");
        $(this).unbind("hover mouseenter mouseleave mouseover mouseout");
        $(this).find(".graybordered").addClass("dropdownbox-active");

        var origProductId = $("#product-dropdownbox > p").attr("data-productid");

        $(".sliderFamilyPickerMenu li").off().on("click", function (e) {
            e.stopPropagation();
            $(".sliderFamilyPickerMenu li").each(function () {
                $(this).removeClass("selectedFamily");
            });
            $(this).addClass("selectedFamily");

            $(".productPickerMenu").hide();
            $(".productPickerMenu[data-productfamily='" + $(this).data("productfamily") + "']").show();
        });
        $(".productPickerMenu > li").off().on("click", function (e) {
            e.stopPropagation();

            if (!$(this).hasClass("disabledoption")) {

                var newProductId = $(this).data("productid");
                var fullName = $(this).parent("ul").data("productfamilynm") + " - " + $(this).find(".prodnm").first().html();

                $("#product-dropdownbox > .prefblock .preflabel").text(fullName);
                $("#productStep").attr("data-productid", newProductId);

                $("productPickerMenu .selectedOption").removeClass("selectedOption");
                $("li[data-productid='" + newProductId + "']").first().addClass("selectedOption");
                hideProductSelector();

                if (origProductId != newProductId) {
                    selectProduct(newProductId);
                    updateProductInfo(newProductId);
                } else {
                    continueWizard();
                }
            }
        });

    });
}

function continueWizard(previousOrderStepId) {
    var nextOrderStepEl = selectNextOrderStepAfter(previousOrderStepId);
    if (nextOrderStepEl != null) {
        setOrderStepHoverState(nextOrderStepEl, true);
        if (nextOrderStepEl != null) {
            $(nextOrderStepEl).trigger("click");
        }
    }
}

function selectNextOrderStepAfter(previousOrderStepId) {
    var nextOrderStep =
    previousOrderStepId ?
        $(".optionTypeStep[data-selectable='true'][data-optiontypeid='" + previousOrderStepId + "']").nextAll(".optionTypeStep:visible").first() :
        $(".optionTypeStep[data-selectable='true'][data-optionid='-1']:visible").first();
    return nextOrderStep;
}


function updateProductInfo(id) {
    $("#info").load('../Umbraco/Surface/ProductTool/ProductInfo?id=' + id);
}

function selectProduct(productID) {
    $("#optionTypesDiv").slideUp("fast");
    var data = { newProductId: productID };
    $.ajax({
        type: "POST",
        url: actionSelectProductCalc,
        data: data,
        success: function (result) {
            if (result != null && result.length > 0) {
                MultipartialUpdate(result);
                putSliderPickersInPlace();
                setupProductStepHover();
                setOrderStepHoverState("#productStep", false);
                if (productID > 0) {
                    initOptionSelectors();
                    setupOptionStepHovers();
                    setOrderStepHoverState($(".optionTypeStep[data-selectable='true']").first(), true);
                    $("#optionTypesDiv").slideDown("fast");
                    $(".optionTypeStep[data-selectable='true']").first().trigger("click");
                }
                disableIllegalOptions();
                refreshPriceInterface();
                finetuneLayout();
            }
            hideOptionTypesLoading();
        },
        error: function () {
            hideOptionTypesLoading();
        }
    });
    showOptionTypesLoading(getTranslation("ShowOrderForm.LoadingProduct"));
}

function selectOption(optiontypeid, origoptionid, newoptionid, name) {
    $(".dropdownbox[data-optiontype='" + optiontypeid + "'] > .prefblock .preflabel").text(name);
    var optionTypStep = $(".optionTypeStep[data-optiontypeid='" + optiontypeid + "']");
    $(optionTypStep).attr("data-optionid", newoptionid);
    $(optionTypStep).find(".stepInfo img").first().attr("src", "/images/ok.png");

    $("ul[data-optiontype='" + optiontypeid + "'] .selectedOption").removeClass("selectedOption");
    $("li[data-optionid='" + newoptionid + "']").first().addClass("selectedOption");

    hideOptionSelector(optiontypeid);
    if (origoptionid != newoptionid) {
        disableIllegalOptions();
        refreshPriceInterface();
        saveImagePreferencesCalc();
    }
}

function refreshPriceInterface() {
    if (validInputDims() &&
        productSelected() &&
        atLeastOneOptionProvided()) {
        $("#phPriceDetails").show();
        $("#spanImageWeight").show();
        $(".vat-info").show();
        $(".a-print").show();
    } else {
        $("#phPriceDetails").hide();
        $("#spanImageWeight").hide();
        $(".vat-info").hide();
        $(".a-print").hide();
    }
}

function saveImagePreferencesCalc() {
    if (!validInputDims()) {
        return;
    }
    var data = { preferencesSet: generatePrefSet() };
    $.ajax({
        type: "POST",
        url: actionSaveImagePreferencesCalc,
        data: data,
        success: function (result) {
            if (result != null && result.length > 0) {
                MultipartialUpdate(result);
                disableIllegalOptions();
            }
        }
    });

}

function productSelected() {
    return $("#productStep").attr("data-productid") != "0";
}
function hasIncompleteRequiredSteps() {
    return $(".optionTypeStep[data-required='True'][data-optionid='-1']").length > 0;
}
function atLeastOneOptionProvided() {
    return $(".optionTypeStep[data-optionid!='-1'][data-optionid!='0']").length > 0;
}
function validInputDims() {
    return $(".dimInput[data-valid='0']").length == 0;
}

function initOptionSelectors() {
    $(".optionTypeStep[data-selectable='true']").each(function (e) {
        $(this).off().on("click", function () {
            var optiontypeid = $(this).data("optiontypeid");
            showOptionSelector(optiontypeid);

            $(this).find(".dropdownbox-active").removeClass("dropdownbox-active");
            $(this).unbind("hover mouseenter mouseleave mouseover mouseout");
            $(this).find(".graybordered").addClass("dropdownbox-active");

            var origOptionId = $(".optionTypeStep[data-optiontypeid='" + optiontypeid + "']").attr("data-optionid");

            $("ul[data-optiontype='" + optiontypeid + "'] .sliderPickerMenu li").off().on("click", function (event) {
                event.stopPropagation();

                if (!$(this).hasClass("disabledoption")) {

                    var newOptionId = $(this).data("optionid");

                    var name = newOptionId == 0 ? getTranslation("ShowPreferences.NoOptionPrefix") + " " + $(".dropdownbox[data-optiontype='" + optiontypeid + "']").data("optiontypename") : $(this).text();

                    selectOption(optiontypeid, origOptionId, newOptionId, name);

                    setTimeout(function () { disableIllegalOptions(); continueWizard(optiontypeid); }, 200);
                }
            });
        });


    });
}

function showProductSelector() {
    hideOptionSelectors();
    $("#productPicker").show();
    $("#productPicker").animate({
        left: $("#finishingPrefsDiv").position().left + $("#finishingPrefsDiv").width(),
        opacity: 1
    });
}

function showOptionSelector(optiontypeid) {
    hideProductSelector();
    hideOptionSelectors();
    $(".sliderPicker[data-optiontypeid='" + optiontypeid + "']").show();
    $(".sliderPicker[data-optiontypeid='" + optiontypeid + "']").animate({
        left: $("#finishingPrefsDiv").position().left + $("#finishingPrefsDiv").width(),
        opacity: 1
    });
}

function hideProductSelector() {
    $("#productPicker").animate({
        left: 500,
        opacity: 0
    }, "fast", function () {
        $("#productPicker").hide();
        $("#productPicker").css("marginLeft", "0px");

        setupProductStepHover();
        $("#productStep").find(".dropdownbox-active").removeClass("dropdownbox-active");
    });
}

function hideOptionSelectors() {
    $(".optionPicker").each(function () {
        if ($(this).css("display") != "none") {
            hideOptionSelector($(this).data("optiontypeid"));
        }
    });
}

function hideOptionSelector(optiontypeid) {
    $(".sliderPicker[data-optiontypeid='" + optiontypeid + "']").animate({
        left: 500,
        opacity: 0
    }, "fast", function () {
        $(".sliderPicker[data-optiontypeid='" + optiontypeid + "']").hide();
        $(".sliderPicker[data-optiontypeid='" + optiontypeid + "']").css("marginLeft", "0px");

        var orderStepEl = $(".optionTypeStep[data-optiontypeid='" + optiontypeid + "']");
        setupOptionStepHover(orderStepEl);
        $(orderStepEl).find(".dropdownbox-active").removeClass("dropdownbox-active");

    });
}

function disableIllegalOptions() {
    
    $(".sliderPickerMenu .option").show();
    enableAllOptions();



    // hide illegal combinations
    var selected_optionids_arr = []; // first let's gather all currently selected option id's
    $(".optionTypeStep[data-optionid!='-1'][data-optionid!='0']").each(function () {
        selected_optionids_arr.push($(this).data("optionid"));
    });
    for (var k = 0; k < selected_optionids_arr.length ; k++) {
        var optionId = selected_optionids_arr[k];
        var optionName = $(".optionTypeStep[data-optionid='" + optionId + "']").find(".preflabel").html();

        // figure out which optiontypes to check (the ones succeeding the type of the option in scope.. = always work downward when checking to disable options)
        var next_optiontypeids_arr = [];
        $(".optionTypeStep[data-optionid='" + optionId + "'").nextAll(".optionTypeStep").each(function () {
            next_optiontypeids_arr.push($(this).attr("data-optiontypeid"))
        });

        // now let's hide all options who include this in their uncombinablewith, taking next_optiontypeids_arr into account
        for (var i = 0; i < next_optiontypeids_arr.length ; i++) {
            var next_optiontypeid = next_optiontypeids_arr[i];
            $(".sliderPicker[data-optiontypeid='" + next_optiontypeid + "']").find(".option[data-uncombinablewith]").each(function () {

                if ($(this).data("uncombinablewith").indexOf(optionId + ',') >= 0) {
                    var optionToDisable = $(this);

                    var optionTypeStepWithThisOptionSelected = $(".optionTypeStep[data-optionid='" + $(optionToDisable).data("optionid") + "']");
                    if ($(optionTypeStepWithThisOptionSelected).length > 0) {
                        resetOptionTypeStep($(optionTypeStepWithThisOptionSelected));
                    }

                    if ($(optionToDisable).data("showonillcombo") == "1") {
                        disableOption($(optionToDisable), getTranslation("ShowPreferences.OptionUncombinableWith").format(optionName));
                    } else {
                        $(optionToDisable).hide();
                    }
                }
            });
        }
    }

    // hide illegal "no <optiontype>"
    var empty_optiontypeids_arr = []; // first let's gather the id's of all currently empty optiontypes
    $(".optionTypeStep[data-optionid='-1'], .optionTypeStep[data-optionid='0']").each(function () {
        empty_optiontypeids_arr.push($(this).data("optiontypeid"));
    });

    for (var k = 0; k < empty_optiontypeids_arr.length ; k++) {
        var emptyTypeId = empty_optiontypeids_arr[k];
        var emptyTypeName = $(".optionTypeStep[data-optiontypeid='" + emptyTypeId + "']").find(".optiontype-label").html();
        // now let's hide all options who include this in their uncombinablewithempty
        $(".sliderPickerMenu .option[data-uncombinablewithempty]").each(function () {
            if ($(this).data("uncombinablewithempty").indexOf(emptyTypeId + ',') >= 0) {
                var optionToDisable = $(this);

                var optionTypeStepWithThisOptionSelected = $(".optionTypeStep[data-optionid='" + $(optionToDisable).data("optionid") + "']");
                if ($(optionTypeStepWithThisOptionSelected).length > 0) {
                    resetOptionTypeStep($(optionTypeStepWithThisOptionSelected));
                }
                if ($(optionToDisable).data("showonillcombo") == "1") {
                    disableOption($(optionToDisable), getTranslation("ShowPreferences.ChooseOptionFirst").format(emptyTypeName));
                } else {
                    $(optionToDisable).hide();
                }
            }
        });
    }


    //hide unsupportive options (weight)
    $(".sliderPickerMenu .option[data-supportsmax]").each(function () {
        if ($(this).css("display") == "none") {
            return;
        }
        var width = parseFloat($("#dim1").val());
        var height = parseFloat($("#dim2").val());
        var widthInM = width / 1000;
        var heightInM = height / 1000;

        var currentWeight = parseFloat($("#lblImageWeight").html().replace(",", "."));

        if (currentWeight != null && currentWeight != "" && $(this).data('weight') != null && $(this).data('weight') != "") {

            var candidateWeight = calculateHangingWeight(widthInM, heightInM, $(this).data('weight'), $(this).data('weighttype'));
            var supportsMax = parseFloat($(this).data('supportsmax'));

            if ((widthInM * supportsMax) < currentWeight + candidateWeight) {
                var optionTypeStep = $(".optionTypeStep[data-optionid='" + $(this).data("optionid") + "']");
                if ($(optionTypeStep).length > 0) {
                    resetOptionTypeStep(optionTypeStep);
                }
                if ($(this).data("showonmweightexc") == "1") {
                    disableOption($(this), getTranslation("ShowPreferences.OptionTooLight"));
                } else {
                    $(this).hide();
                }
            }
        }

    });

    //hide unsupportive products (size)
    $(".sliderPickerMenu .product[data-m1-mm]").each(function () {
        var mdim1MM = parseFloat($(this).data('m1-mm'));
        var mdim2MM = parseFloat($(this).data('m2-mm'));
        var currentdim1MM = parseFloat($("#dim1").val()) * 10;
        var currentdim2MM = parseFloat($("#dim2").val()) * 10;
        if ((currentdim1MM > mdim1MM || currentdim2MM > mdim2MM) && (currentdim1MM > mdim2MM || currentdim2MM > mdim1MM)) {
            var productStep = $("#productStep[data-productid='" + $(this).data("productid") + "']");
            if ($(productStep).length > 0) {
                resetProductStep();
                $("#optionTypePickers").empty(); // this way they're not checked (with hide unsupported options hereunder), which would cause lots of unnecessary ajax save calls for all (recursive) optiontypes to reset
            }
            var disabledMessage = getTranslation("ShowPreferences.ProductMaximumSize").replace("{0}", $(this).data("m1-mm") + " x " + $(this).data("m2-mm"));
            disableOption($(this), disabledMessage);
        }
    });

    //hide unsupportive options (size)
    $(".sliderPickerMenu .option[data-m1-mm]:not(.product)").each(function () {
        if ($(this).css("display") == "none") {
            return;
        }
        var mdim1MM = parseFloat($(this).data('m1-mm'));
        var mdim2MM = parseFloat($(this).data('m2-mm'));
        var currentdim1MM = parseFloat($("#dim1").val()) * 10;
        var currentdim2MM = parseFloat($("#dim2").val()) * 10;
        if ((currentdim1MM > mdim1MM || currentdim2MM > mdim2MM) && (currentdim1MM > mdim2MM || currentdim2MM > mdim1MM)) {
            var optionTypeStep = $(".optionTypeStep[data-optionid='" + $(this).data("optionid") + "']");
            if ($(optionTypeStep).length > 0) {
                resetOptionTypeStep(optionTypeStep);
            }

            if ($(this).data("showonmsizeexc") == "1") {
                disableOption($(this), unescape($(this).data("mdesc")));
            } else {
                $(this).hide();
            }
        }

    });



    hideEmptySelectors();
}

function calculateHangingWeight(width, height, weight, weightType) {
    var kg = parseFloat(weight.replace(",", "."));
    switch (weightType) {
        case "len": return width * kg; break;
        case "cir": return (2 * (width * kg)) + (2 * (height * kg)); break;
        default: return width * height * kg; break;
    }
}

function enableAllOptions() {
    $(".option").removeClass("disabledoption");
    $(".option").find(".optionimage .optionimageblanket").hide();
    $(".option").find(".optionimage .optionimageblanket .notallowedfeedback").html("");
}

function disableOption(option, txt) {
    if (!$(option).hasClass("disabledoption")) {
        $(option).addClass("disabledoption");
        $(option).find(".optionimage .optionimageblanket").show();
        $(option).find(".optionimage .optionimageblanket .notallowedfeedback").html(txt);
    }
}

function resetProductStep() {
    console.log("resetprodu");
    $("#productStep").attr("data-productid", 0);
    selectProduct(0);
    $("#productStep .preflabel").text("Kies een product");
    $("#productStep .stepInfo img").attr("src", "/images/ok-empty.png");
    $("#optionTypesDiv").empty();
}

function resetOptionTypeStep(optionTypeStep) {

    if (!optionTypeStep)
        return false;
    var optionTypeId = $(optionTypeStep).attr("data-optiontypeid");
    var newOptionId = -1;
    var name = "Kies een " + $(".dropdownbox[data-optiontype='" + optionTypeId + "']").data("optiontypename");
    var origOptionId = $(optionTypeStep).attr("data-optionid");
    selectOption(optionTypeId, origOptionId, newOptionId, name);

    // alle actieve .options afgaan: heeft er een een illegalempty met deze optiontype in?
    $(".sliderPickerMenu .option[data-uncombinablewithempty]").each(function () {
        if ($(this).css("display") == "none") {
            return;
        }
        if ($(this).data('uncombinablewithempty').indexOf(optionTypeId) != -1) {
            resetOptionTypeStep($(".optionTypeStep[data-optionid='" + $(this).data("optionid") + "']"));
            if ($(this).data("showonillcombo") == "1") {
                disableOption($(this));
            } else {
                $(this).hide();
            }
        }
    });
}


function setupProductStepHover() {
    setOrderStepHoverState("#productStep", false);

    $("#productStep").on("mouseover", function (e) {
        setOrderStepHoverState("#productStep", true);
    });
    $("#productStep").on("mouseout", function (e) {
        setOrderStepHoverState("#productStep", false);
    });
}

function setupOptionStepHovers() {
    $(".optionTypeStep[data-selectable='true']").hover(function (e) {
        setupOptionStepHover($(this));
    });
}


function setOrderStepHoverState(optionstep, active) {
    $(optionstep).find(".optiontype-label").css({
        "color": active ? "#D34743" : "",
        "border-bottom": active ? "6px solid #D34743" : "6px solid #ddd"
    });
    $(optionstep).find(".stepNumber").css({
        "color": active ? "#D34743" : "",
        "font-weight": active ? "bold" : "normal"
    });
    $(optionstep).find(".graybordered").css({
        "color": active ? "#D34743" : "",
        "background": active ? "#F8F8F8" : ""
    });
}

function setupOptionStepHover(optionstep) {
    setOrderStepHoverState(optionstep, false);
    $(optionstep).on("mouseover", function (e) {
        setOrderStepHoverState(optionstep, true);
    });
    $(optionstep).on("mouseout", function (e) {
        setOrderStepHoverState(optionstep, false);
    });
}

function putSliderPickersInPlace() {
    $("#optionTypePickers").empty();
    $(".sliderPicker").each(function () {
        $("#optionTypePickers").append($(this));
    });
}

function hideEmptySelectors() {
    $(".sliderPicker").each(function () {
        var optiontypeid = $(this).data("optiontypeid");
        var optiontypestep = $(".optionTypeStep[data-optiontypeid='" + optiontypeid + "']");

        var lis = false;
        $(this).find("li.option:not(.nooption)").each(function () {
            if ($(this).css("display") != "none") {
                lis = true;
            }
        });
        if (lis) {
            $(optiontypestep).show();
        } else {
            $(optiontypestep).hide();
        }
    });
}
function MultipartialUpdate(views) {
    for (v in views)
        if (views[v].script) {
            eval(views[v].script);
        }
        else {
            $('#' + views[v].updateTargetId).empty().append(views[v].html);
        }
    return false;
}

function generatePrefSet() {
    prefSet = new Object();
    $(".optionTypeStep").each(function () {
        var optionid = $(this).attr("data-optionid");
        if (optionid == parseInt(optionid)) {
            prefSet[$(this).attr("data-optiontypeid").toString()] = optionid;
        }
    });
    prefSet["dim1"] = $("#dim1").val();
    prefSet["dim2"] = $("#dim2").val();

    var extraCb = $(".extra-checkbox:checked");
    if (extraCb) {
        prefSet["extra"] = $(extraCb).data("extra");
    }

    prefSet["product"] = $("#productStep").attr("data-productid");
    return JSON.stringify(prefSet);
}