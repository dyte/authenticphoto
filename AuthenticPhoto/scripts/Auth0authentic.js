﻿var lock = new Auth0Lock('qUfqMh6cTrjQQgUvrn8LB8PaFGplBtGf', 'AuthenticPhoto.eu.auth0.com', {
    assetsUrl: 'https://cdn.eu.auth0.com/'
});


function showLoginPopup(querystring) {
    if (querystring == null) querystring = "";
    lock.show({
        dict: 'nl'
      , socialBigButtons: true
      /*, container: 'root'*/
      , callbackURL: 'http://AuthenticPhoto.local/LoginCallback.ashx' + querystring
      , responseType: 'code'
      , authParams: {
          scope: 'openid profile'
      }
    });
}
