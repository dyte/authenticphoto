﻿var processingQueue = []; // holds fileid's
var processing = 0;
var actionProcessImage;

onmessage = function (e) { // fileid was added
    var info = e.data;
    actionProcessImage = info.actionProcessImage;
    processingQueue.push(info.fileid);
    toConsole("added fileid " + info.fileid);
    checkProcessingQueue();
}

function toConsole(msg) {
    postMessage({ "action": "console", "message": msg });
}

function checkProcessingQueue() {
    if (processing >= 2) {
        toConsole("Worker busy.");
        setTimeout("checkProcessingQueue()", 5000);
    } else if (processingQueue.length > 0) {        
        var fileid = processingQueue.shift();
        toConsole("Entry found! Processing img with id " + fileid);
        postMessage({ "action": "started", "fileid": fileid });
        processImage(fileid);
    }
}

function processImage(fileid) {
    processing++;

    var xhr;

    if (typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
    else {
        var versions = ["MSXML2.XmlHttp.5.0",
			 	"MSXML2.XmlHttp.4.0",
			 	"MSXML2.XmlHttp.3.0",
			 	"MSXML2.XmlHttp.2.0",
			 	"Microsoft.XmlHttp"]

        for (var i = 0, len = versions.length; i < len; i++) {
            try {
                xhr = new ActiveXObject(versions[i]);
                break;
            }
            catch (e) { }
        } // end for
    }

    xhr.onreadystatechange = ensureReadiness;

    function ensureReadiness() {
        if (xhr.readyState < 4) {
            return;
        }

        if (xhr.status !== 200) {
            return;
        }

        // all is well	
        if (xhr.readyState === 4) {
            callback(xhr);
        }
    }

    var params = "id=" + fileid;

    xhr.open('POST', actionProcessImage, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("Content-length", params.length);
    xhr.setRequestHeader("Connection", "close");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {

            var responseText = xhr.responseText;
            var responseJson = JSON.parse(responseText);
            responseJson.action = "completed";

            postMessage(responseJson);
            processing--;
            checkProcessingQueue();
        }
    }
    xhr.send(params);

    //var data = { id: fileId };
    //$.ajax({
    //    type: "POST",
    //    url: actionProcessImage,
    //    data: data,
    //    success: function (data) {

    //        var info = JSON.parse(data);

    //        var src = info.thumbsource;
    //        var width = info.thumbwidth;
    //        var height = info.thumbheight;
    //        var thumbwidth = parseInt(parseInt(width) / 5);
    //        var thumbheight = parseInt(parseInt(height) / 5);

    //        if (src != null && src != "") {

    //            var thumb = $(".thumb[data-imgid='" + fileId + "']");
    //            var prev = $(".prev[data-imgid='" + fileId + "']");

    //            $(thumb).css("background-image", "url(" + src + ")");
    //            $(thumb).css("background-size", thumbwidth + "px" + " " + thumbheight + "px");
    //            $(thumb).css("width", thumbwidth + "px")
    //            $(thumb).css("height", thumbheight + "px")

    //            $(prev).attr("src", src);
    //            $(prev).css("width", width + "px")
    //            $(prev).css("height", height + "px")
    //        }

    //        $(".progressbarimage[data-imgid='" + fileId + "']").fadeOut();
    //        $(".processingimg[data-imgid='" + fileId + "']").fadeOut();

            
    //    }
    //});
}