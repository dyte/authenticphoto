﻿var sections = $(".hover-section")
  , nav = $("nav")
  , nav_height = nav.outerHeight();

$(window).on("scroll", function () {
    nav.find("li").removeClass("active");
    sections.removeClass("active");

    var currentPosition = $(this).scrollTop();

    sections.each(function (index, item) {
        var top = $(this).offset().top - nav_height,
            bottom = top + $(item).outerHeight();

        if (currentPosition >= top && currentPosition <= bottom) {

            $(item).addClass("active");
            console.log($(item).attr("id"));
            nav.find('[href="#' + $(item).attr("id") + '"]').closest("li").addClass("active");
        }
    });
});