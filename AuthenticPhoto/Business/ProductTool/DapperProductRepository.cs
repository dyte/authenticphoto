﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Dapper;

namespace AuthenticPhoto.Business.ProductTool
{
    public class DapperProductRepository : IProductRepository
    {
        private static string CONNECTIONSTRING = ConfigurationManager.ConnectionStrings["DapperConnection"].ConnectionString;

        private string _SelectProductFamilies = "SELECT [ProductFamilyID],[NameXml],[seq] FROM [ProductFamilies] ORDER BY [seq]";
        private string _SelectProducts = "SELECT [ProductID],[NameXml],[ProductFamilyID],[seq] FROM [Products] ORDER BY [seq]";
        private string _SelectOptionTypes = "SELECT [OptionTypeID],[NameXml],[ProductID],[seq] FROM [OptionTypes] ORDER BY [seq]";
        private string _SelectOptionCategories = "SELECT [OptionCategoryID],[NameXml],[OptionTypeID],[seq] FROM [OptionCategories] ORDER BY [seq]";
        private string _SelectOptions = "SELECT [OptionID],[NameXml],[OptionCategoryID],[seq] FROM [Options] ORDER BY [seq]";
        private string _SelectLanguages = "SELECT [LanguageID],[Name],[Code],[Seq] FROM [Languages] ORDER BY [seq]";
        private string _SelectCheckoutProducts = "SELECT [CheckoutProductID],[NameXml],[Price],[seq] FROM [CheckoutProducts] ORDER BY [seq]";

        private string _SelectDeliveryAddresses = "SELECT [DeliveryAddressID],[FirstName],[FamilyName],[Email],[Address],[PostalCode]," +
            "[AddressIndication],[Place],[Country],[PhoneNumberCountryCode],[PhoneNumber],[Company] FROM [DeliveryAddresses] WHERE AuthenticUserID = @AuthenticUserID";

        private string _SelectProductFamily = "SELECT [ProductFamilyID],[NameXml],[seq] FROM [ProductFamilies] WHERE ProductFamilyID =@ProductFamilyID ORDER BY [seq]";
        private string _SelectCheckoutProduct = "SELECT [CheckoutProductID],[NameXml],[Price],[seq] FROM [CheckoutProducts] WHERE CheckoutProductID =@CheckoutProductID ORDER BY [seq]";
        private string _SelectProduct = "SELECT [ProductID],[NameXml],[ProductFamilyID],[seq],[PreselectOptions],[SectionNameXml],[ShortNameXml] FROM [Products] WHERE ProductID =@ProductID ORDER BY [seq]";
        private string _SelectOptionType = "SELECT [OptionTypeID],[NameXml],[ProductID],[seq],[Required],[SectionNameXml],[Data1],[OptionTypeType] FROM [OptionTypes] WHERE OptionTypeID =@OptionTypeID ORDER BY [seq]";
        private string _SelectOptionCategory = "SELECT [OptionCategoryID],[NameXml],[seq],[OptionTypeID] FROM [OptionCategories] WHERE OptionCategoryID =@OptionCategoryID ORDER BY [seq]";
        private string _SelectOption =
            "SELECT [OptionID],[NameXml],[OptionCategoryID],[seq],[RollSizeActive],[MaxSizeDescXml],[Kg],[TestprintA4],[Testprint11]," +
            "[Certificate],[IllegalCombinations],[MaxSizeDim1],[MaxSizeDim2],[LastEdit],[PromoStart],[PromoEnd]," +
            "[PromoPercentage],[PromoMinPrice],[PromoDescXml],[WeightType],[SupportsMaxActive],[SupportsMaxKg],[SupportsMaxExceededDescXml]," +
            "[IllegalEmptyOptionTypesCombinations],[CheckoutProductsOnSelected],[CheckoutProductsOnNotSelected],[ShortNameXml],[ShowOnMaxSizeExceeded]," +
            "[ShowOnMaxSupportExceeded],[ShowOnIllegalCombo] FROM [Options] WHERE OptionID =@OptionID ORDER BY [seq]";

        private string _SelectRollSizes = "SELECT [RollSizesJson] FROM [Options] WHERE OptionID = @OptionID ORDER BY [seq]";
        private string _SelectSheetSizes = "SELECT [SheetSizesJson] FROM [Options] WHERE OptionID = @OptionID ORDER BY [seq]";

        private string _SelectProductFamiliesWithChildListing =
            //"SELECT " +
            //"pf.[ProductFamilyID], " +
            //"pf.[NameXml], " +
            //"pf.[seq], " +
            //"p.[ProductID] AS 'Products_ProductID', " +
            //"p.[NameXml] AS 'Products_NameXml', " +
            //"p.[seq] AS 'Products_seq'" +
            //"FROM [ProductFamilies] pf " +
            //"LEFT JOIN [Products] p ON pf.ProductFamilyID = p.ProductFamilyID ORDER BY pf.[seq], p.[seq]";
            "SELECT " +
            "pf.[ProductFamilyID], " +
            "pf.[NameXml], " +
            "pf.[seq], " +
            "p.[ProductID] AS 'Products_ProductID', " +
            "p.[NameXml] AS 'Products_NameXml', " +
            "p.[seq] AS 'Products_seq', " +
            "(SELECT TOP (1) CONCAT([MaxSizeDim1],',',[MaxSizeDim2]) FROM [Options] o WHERE o.OptionCategoryID IN " +
	            "( SELECT oc.OptionCategoryID FROM [OptionCategories] oc WHERE oc.OptionTypeID = " +
		            "(SELECT TOP (1) [OptionTypeID] FROM [OptionTypes] ot2 WHERE ot2.ProductID = p.ProductID ORDER BY seq, ot2.OptionTypeID) " +
	            ") " +
	            "ORDER BY (o.MaxSizeDim1 + o.MaxSizeDim2) DESC " +
            ") AS 'Products_MaxSizeDim1And2' " +            
            "FROM [ProductFamilies] pf " +
            "LEFT JOIN " +
	        "[Products] p " +
	        "ON pf.ProductFamilyID = p.ProductFamilyID";

        private string _SelectProductWithPopulatedChildren =
            "SELECT " +
            "p.[ProductID], " +
            "p.[NameXml], " +
            "p.[seq], " +
            "p.[PreselectOptions], " +
            "p.[ProductFamilyID]," +
            "ot.[OptionTypeID] AS 'OptionTypes_OptionTypeID', " +
            "ot.[NameXml] AS 'OptionTypes_NameXml', " +
            "ot.[seq] AS 'OptionTypes_seq', " +
            "ot.[Required] AS 'OptionTypes_Required', " +
            "ot.[OptionTypeType] AS 'OptionTypes_OptionTypeType', " +
            "ot.[Data1] AS 'OptionTypes_Data1', " +
            "oc.[OptionCategoryID] AS 'OptionTypes_OptionCategories_OptionCategoryID', " +
            "oc.[NameXml] AS 'OptionTypes_OptionCategories_NameXml', " +
            "oc.[seq] AS 'OptionTypes_OptionCategories_seq', " +
            "o.[OptionID] AS 'OptionTypes_OptionCategories_Options_OptionID', " +
            "o.[NameXml] AS 'OptionTypes_OptionCategories_Options_NameXml', " +
            "o.[ShortNameXml] AS 'OptionTypes_OptionCategories_Options_ShortNameXml', " +
            "o.[seq] AS 'OptionTypes_OptionCategories_Options_seq', " +
            "o.[MaxSizeDescXml] AS 'OptionTypes_OptionCategories_Options_MaxSizeDescXml', " +
            "o.[Kg] AS 'OptionTypes_OptionCategories_Options_Kg', " +
            "o.[TestprintA4] AS 'OptionTypes_OptionCategories_Options_TestprintA4', " +
            "o.[Testprint11] AS 'OptionTypes_OptionCategories_Options_Testprint11', " +
            "o.[Certificate] AS 'OptionTypes_OptionCategories_Options_Certificate', " +
            "o.[WeightType] AS 'OptionTypes_OptionCategories_Options_WeightType', " +
            "o.[RollSizeActive] AS 'OptionTypes_OptionCategories_Options_RollSizeActive', " +
            "o.[IllegalCombinations] AS 'OptionTypes_OptionCategories_Options_IllegalCombinations', " +
            "o.[IllegalEmptyOptionTypesCombinations] AS 'OptionTypes_OptionCategories_Options_IllegalEmptyOptionTypesCombinations', " +
            "o.[MaxSizeDim1] AS 'OptionTypes_OptionCategories_Options_MaxSizeDim1', " +
            "o.[MaxSizeDim2] AS 'OptionTypes_OptionCategories_Options_MaxSizeDim2', " +
            "o.[SupportsMaxActive] AS 'OptionTypes_OptionCategories_Options_SupportsMaxActive', " +
            "o.[SupportsMaxKg] AS 'OptionTypes_OptionCategories_Options_SupportsMaxKg', " +
            "o.[SupportsMaxExceededDescXml] AS 'OptionTypes_OptionCategories_Options_SupportsMaxExceededDescXml', " +
            "o.[ShowOnMaxSizeExceeded] AS 'OptionTypes_OptionCategories_Options_ShowOnMaxSizeExceeded', " +
            "o.[ShowOnMaxSupportExceeded] AS 'OptionTypes_OptionCategories_Options_ShowOnMaxSupportExceeded', " +
            "o.[ShowOnIllegalCombo] AS 'OptionTypes_OptionCategories_Options_ShowOnIllegalCombo' " +
            "FROM [Products] p " +
            "LEFT JOIN( " +
               "[OptionTypes] ot " +
               "LEFT JOIN( " +
                  "[OptionCategories] oc " +
                  "LEFT JOIN[Options] o ON oc.OptionCategoryID = o.OptionCategoryID " +
               ") ON ot.OptionTypeID = oc.OptionTypeID " +
            ") ON p.ProductID = ot.ProductID " +
            "WHERE p.ProductID = @ProductID ORDER BY p.[seq], ot.[seq], o.[seq]";


        private string _SelectOptionTypeWithPopulatedChildrenInclSizes =
            "SELECT " +
            "ot.[OptionTypeID]," +
            "ot.[NameXml]," +
            "ot.[seq]," +
            "ot.[Required]," +
            "ot.[OptionTypeType]," +
            "ot.[Data1]," +
            "oc.[OptionCategoryID]," +
            "oc.[NameXml] AS 'OptionCategories_NameXml', " +
            "oc.[seq] AS 'OptionCategories_seq', " +
            "o.[OptionID] AS 'OptionCategories_Options_OptionID', " +
            "o.[NameXml] AS 'OptionCategories_Options_NameXml', " +
            "o.[ShortNameXml] AS 'OptionCategories_Options_ShortNameXml', " +
            "o.[seq] AS 'OptionCategories_Options_seq', " +
            "o.[MaxSizeDescXml] AS 'OptionCategories_Options_MaxSizeDescXml', " +
            "o.[Kg] AS 'OptionCategories_Options_Kg', " +
            "o.[TestprintA4] AS 'OptionCategories_Options_TestprintA4', " +
            "o.[Testprint11] AS 'OptionCategories_Options_Testprint11', " +
            "o.[Certificate] AS 'OptionCategories_Options_Certificate', " +
            "o.[WeightType] AS 'OptionCategories_Options_WeightType', " +
            "o.[RollSizeActive] AS 'OptionCategories_Options_RollSizeActive', " +
            "o.[IllegalCombinations] AS 'OptionCategories_Options_IllegalCombinations', " +
            "o.[IllegalEmptyOptionTypesCombinations] AS 'OptionCategories_Options_IllegalEmptyOptionTypesCombinations', " +
            "o.[MaxSizeDim1] AS 'OptionCategories_Options_MaxSizeDim1', " +
            "o.[MaxSizeDim2] AS 'OptionCategories_Options_MaxSizeDim2', " +
            "o.[SupportsMaxActive] AS 'OptionCategories_Options_SupportsMaxActive', " +
            "o.[SupportsMaxKg] AS 'OptionCategories_Options_SupportsMaxKg', " +
            "o.[SupportsMaxExceededDescXml] AS 'OptionCategories_Options_SupportsMaxExceededDescXml', " +
            "o.[ShowOnMaxSizeExceeded] AS 'OptionCategories_Options_ShowOnMaxSizeExceeded', " +
            "o.[ShowOnMaxSupportExceeded] AS 'OptionCategories_Options_ShowOnMaxSupportExceeded', " +
            "o.[ShowOnIllegalCombo] AS 'OptionCategories_Options_ShowOnIllegalCombo', " +
            "o.[RollSizesJson] AS 'OptionCategories_Options_RollSizesJson', " +
            "o.[SheetSizesJson] AS 'OptionCategories_Options_SheetSizesJson' " +
            "FROM [OptionTypes] ot " +
               "LEFT JOIN( " +
                  "[OptionCategories] oc " +
                  "LEFT JOIN[Options] o ON oc.OptionCategoryID = o.OptionCategoryID " +
               ") ON ot.OptionTypeID = oc.OptionTypeID " +
            "WHERE ot.OptionTypeID = @OptionTypeID ORDER BY ot.[seq], o.[seq]";

        public IEnumerable<AuthenticUser> AuthenticUsers
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IEnumerable<CheckoutProduct> CheckoutProducts
        {
            get
            {
                using (var conn = new SqlConnection(CONNECTIONSTRING))
                {
                    conn.Open();
                    {
                        List<CheckoutProduct> checkoutProducts = (List<CheckoutProduct>)conn.Query<CheckoutProduct>(_SelectCheckoutProducts);
                        return checkoutProducts;
                    }
                }
            }
        }

        public IEnumerable<DeliveryAddress> DeliveryAddresses
        {
            get
            {
                return new List<DeliveryAddress>();
            }
        }

        public IEnumerable<OptionCategory> OptionCategories
        {
            get
            {
                using (var conn = new SqlConnection(CONNECTIONSTRING))
                {
                    conn.Open();
                    {
                        List<OptionCategory> optionCategories = (List<OptionCategory>)conn.Query<OptionCategory>(_SelectOptionCategories);
                        return optionCategories;
                    }
                }
            }
        }

        public IEnumerable<Option> Options
        {
            get
            {
                using (var conn = new SqlConnection(CONNECTIONSTRING))
                {
                    conn.Open();
                    {
                        List<Option> options = (List<Option>)conn.Query<Option>(_SelectOptions);
                        foreach(Option option in options)
                        {
                            OptionCategory optionCategory = conn.Query<OptionCategory>(_SelectOptionCategory, new { OptionCategoryID = option.OptionCategoryID }).SingleOrDefault();
                            optionCategory.OptionType = conn.Query<OptionType>(_SelectOptionType, new { OptionTypeID = optionCategory.OptionTypeID }).SingleOrDefault();
                            option.OptionCategory = optionCategory;
                        }
                        return options;
                    }
                }
            }
        }

        public IEnumerable<OptionType> OptionTypes
        {
            get
            {
                using (var conn = new SqlConnection(CONNECTIONSTRING))
                {
                    conn.Open();
                    {
                        List<OptionType> optionTypes = (List<OptionType>)conn.Query<OptionType>(_SelectOptionTypes);
                        return optionTypes;
                    }
                }
            }
        }

        public IEnumerable<ProductFamily> ProductFamilies
        {
            get
            {
                using (var conn = new SqlConnection(CONNECTIONSTRING))
                {
                    conn.Open();
                    {
                        List<ProductFamily> productFamilies = (List<ProductFamily>)conn.Query<ProductFamily>(_SelectProductFamilies);
                        return productFamilies;
                    }
                }
            }
        }

        public IEnumerable<Product> Products
        {
            get
            {
                using (var conn = new SqlConnection(CONNECTIONSTRING))
                {
                    conn.Open();
                    {
                        List<Product> products = (List<Product>)conn.Query<Product>(_SelectProducts);
                        return products;
                    }
                }
            }
        }

        public IEnumerable<Translation> Translations
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IEnumerable<Language> Languages
        {
            get
            {
                using (var conn = new SqlConnection(CONNECTIONSTRING))
                {
                    conn.Open();
                    {
                        List<Language> languages = (List<Language>)conn.Query<Language>(_SelectLanguages);
                        return languages;
                    }
                }
            }
        }

        public void SaveDeliveryAddress(DeliveryAddress deliveryAddress)
        {
            throw new NotImplementedException();
            //if (deliveryAddress.DeliveryAddressID == 0)
            //{
            //    context.DeliveryAddresses.Add(deliveryAddress);
            //}
            //else
            //{
            //    DeliveryAddress dbEntry = context.DeliveryAddresses.Find(deliveryAddress.DeliveryAddressID);
            //    if (dbEntry != null)
            //    {
            //        dbEntry.FirstName = deliveryAddress.FirstName;
            //        dbEntry.FamilyName = deliveryAddress.FamilyName;
            //        dbEntry.Email = deliveryAddress.Email;
            //        dbEntry.Address = deliveryAddress.Address;
            //        dbEntry.PostalCode = deliveryAddress.PostalCode;
            //        dbEntry.AddressIndication = deliveryAddress.AddressIndication;
            //        dbEntry.Place = deliveryAddress.Place;
            //        dbEntry.Country = deliveryAddress.Country;
            //        dbEntry.PhoneNumberCountryCode = deliveryAddress.PhoneNumberCountryCode;
            //        dbEntry.PhoneNumber = deliveryAddress.PhoneNumber;
            //        dbEntry.Company = deliveryAddress.Company;
            //    }
            //}
            //context.SaveChanges();
        }

        public DeliveryAddress DeleteDeliveryAddress(int deliveryAddressId)
        {
            throw new NotImplementedException();
            //DeliveryAddress dbEntry = context.DeliveryAddresses.Find(deliveryAddressId);
            //if (dbEntry != null)
            //{
            //    context.DeliveryAddresses.Remove(dbEntry);
            //    context.SaveChanges();
            //}
            //return dbEntry;
        }

        public CheckoutProduct GetCheckoutProduct(int checkoutProductID)
        {
            using (var conn = new SqlConnection(CONNECTIONSTRING))
            {
                conn.Open();
                {
                    return conn.Query<CheckoutProduct>(_SelectCheckoutProduct, new { CheckoutProductID = checkoutProductID }).SingleOrDefault();
                }
            }
        }
        public Product GetProduct(int productID)
        {
            using (var conn = new SqlConnection(CONNECTIONSTRING))
            {
                conn.Open();
                {
                    return conn.Query<Product>(_SelectProduct, new { ProductID = productID }).SingleOrDefault();
                }
            }
        }
        public Option GetOption(int optionID)
        {
            using (var conn = new SqlConnection(CONNECTIONSTRING))
            {
                conn.Open();
                {
                    Option option = conn.Query<Option>(_SelectOption, new { OptionID = optionID }).SingleOrDefault();
                    OptionCategory optionCategory = conn.Query<OptionCategory>(_SelectOptionCategory, new { OptionCategoryID = option.OptionCategoryID }).SingleOrDefault();
                    optionCategory.OptionType = conn.Query<OptionType>(_SelectOptionType, new { OptionTypeID = optionCategory.OptionTypeID }).SingleOrDefault();
                    option.OptionCategory = optionCategory;
                    return option;
                }
            }
        }

        public List<ProductFamily> GetProductFamiliesWithChildListing()
        {
            using (var conn = new SqlConnection(CONNECTIONSTRING))
            {
                conn.Open();
                {
                    dynamic flatResult = conn.Query<dynamic>(_SelectProductFamiliesWithChildListing);
                    Slapper.AutoMapper.Configuration.AddIdentifiers(typeof(ProductFamily), new List<string> { "ProductFamilyID" });
                    Slapper.AutoMapper.Configuration.AddIdentifiers(typeof(Product), new List<string> { "ProductID" });
                    List<ProductFamily> productFamilies = (Slapper.AutoMapper.MapDynamic<ProductFamily>(flatResult) as IEnumerable<ProductFamily>).ToList();
                    foreach(Product product in productFamilies.SelectMany(pf => pf.Products))
                    {
                        int[] dims = product.MaxSizeDim1And2.Split(',').Select(int.Parse).ToArray();
                        product.MaxSizeDim1 = dims[0];
                        product.MaxSizeDim2 = dims[1];
                    }
                    return productFamilies;
                }
            }
        }

        public Product GetProductWithPopulatedChildren(int productID)
        {
            using (var conn = new SqlConnection(CONNECTIONSTRING))
            {
                conn.Open();
                {
                    dynamic flatResult = conn.Query<dynamic>(_SelectProductWithPopulatedChildren, new { ProductID = productID });
                    Slapper.AutoMapper.Configuration.AddIdentifiers(typeof(Product), new List<string> { "ProductID" });
                    Slapper.AutoMapper.Configuration.AddIdentifiers(typeof(OptionType), new List<string> { "OptionTypeID" });
                    Slapper.AutoMapper.Configuration.AddIdentifiers(typeof(OptionCategory), new List<string> { "OptionCategoryID" });
                    Slapper.AutoMapper.Configuration.AddIdentifiers(typeof(Option), new List<string> { "OptionID" });
                    List<Product> products = (Slapper.AutoMapper.MapDynamic<Product>(flatResult) as IEnumerable<Product>).ToList();
                    if (products.Count > 0)
                    {
                        Product product = products[0];
                        product.ProductFamily = conn.Query<ProductFamily>(_SelectProductFamily, new { ProductFamilyID = product.ProductFamilyID }).SingleOrDefault();
                        return product;
                    }
                    return null;
                }
            }
        }

        public OptionType GetOptionTypeWithPopulatedChildrenInclSizes(int optionTypeID)
        {
            using (var conn = new SqlConnection(CONNECTIONSTRING))
            {
                conn.Open();
                {
                    dynamic flatResult = conn.Query<dynamic>(_SelectOptionTypeWithPopulatedChildrenInclSizes, new { OptionTypeID = optionTypeID });
                    Slapper.AutoMapper.Configuration.AddIdentifiers(typeof(OptionType), new List<string> { "OptionTypeID" });
                    Slapper.AutoMapper.Configuration.AddIdentifiers(typeof(OptionCategory), new List<string> { "OptionCategoryID" });
                    Slapper.AutoMapper.Configuration.AddIdentifiers(typeof(Option), new List<string> { "OptionID" });
                    List<OptionType> optionTypes = (Slapper.AutoMapper.MapDynamic<OptionType>(flatResult) as IEnumerable<OptionType>).ToList();
                    if (optionTypes.Count > 0)
                    {
                        OptionType optionType = optionTypes[0];
                        optionType.Product = conn.Query<Product>(_SelectProduct, new { ProductID = optionType.ProductID }).SingleOrDefault();
                        return optionType;
                    }
                    return null;
                }
            }
        }

        public List<RollSize> GetRollSizesForOption(int optionID)
        {
            using (var conn = new SqlConnection(CONNECTIONSTRING))
            {
                conn.Open();
                {
                    string json = conn.Query<string>(_SelectRollSizes, new { OptionID = optionID }).SingleOrDefault();
                    return Option.DecryptRollSizes(optionID, json);
                }
            }
        }
        public List<SheetSize> GetSheetSizesForOption(int optionID)
        {
            using (var conn = new SqlConnection(CONNECTIONSTRING))
            {
                conn.Open();
                {
                    string json = conn.Query<string>(_SelectSheetSizes, new { OptionID = optionID }).SingleOrDefault();
                    return Option.DecryptSheetSizes(optionID, json);
                }
            }
        }

        public List<DeliveryAddress> GetDeliveryAddresses(int userID)
        {
            using (var conn = new SqlConnection(CONNECTIONSTRING))
            {
                conn.Open();
                {
                    List<DeliveryAddress> deliveryAddresses = (List<DeliveryAddress>)conn.Query<DeliveryAddress>(_SelectDeliveryAddresses, new { AuthenticUserID = userID });
                    return deliveryAddresses;
                }
            }
        }
        public DeliveryAddress GetDeliveryAddress(int deliveryAddressID)
        {
            throw new NotImplementedException();
        }
    }
}
 