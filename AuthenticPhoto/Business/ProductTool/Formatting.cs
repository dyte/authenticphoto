﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Business.ProductTool
{
    public static class Formatting
    {
        public static string PrintAsMm(decimal val) {
            return $"{Math.Round(val)} mm";
        }
        public static string PrintAsEuro(decimal val)
        {
            return val > 0 ? $"€ {val.ToString("0.00")}" : "---";
        }
        public static string PrintAsKg(double val)
        {
            return $"{val.ToString("0.00")} kg";
        }

    }
}