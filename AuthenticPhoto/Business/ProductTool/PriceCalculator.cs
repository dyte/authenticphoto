﻿using AuthenticPhoto.Business.ProductTool.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace AuthenticPhoto.Business.ProductTool
{
    public class PriceCalculator
    {
        private decimal dim1;
        private decimal dim2;

        public PriceCalculator(decimal dim1, decimal dim2)
        {
            this.dim1 = dim1;
            this.dim2 = dim2;
        }

        public decimal Calculate(Option option)
        {
            if (option.RollSizeActive)
            {
                return CalculateRoll(option);
            }
            else
            {
                return CalculateSheet(option);
            }
        }

        private decimal CalculateRoll(Option option)
        {
            decimal result1 = 0;
            decimal result2 = 0;

            foreach (RollSize rollSize in option.RollSizes.OrderBy(rs => rs.Price))
            {
                if(dim1 <= rollSize.MaxWidth && dim2 <= rollSize.MaxLength)
                {
                    result1 = rollSize.Price * (dim2 < rollSize.MinLength? rollSize.MinLength : dim2);
                    break;
                }
            }
            // change orientation and try again
            foreach (RollSize rollSize in option.RollSizes.OrderBy(rs => rs.Price))
            {
                if (dim1 <= rollSize.MaxLength && dim2 <= rollSize.MaxWidth)
                {
                    result2 = rollSize.Price * (dim1 < rollSize.MinLength ? rollSize.MinLength : dim1);
                    break;
                }
            }

            if (result1 == 0) return result2;
            if (result2 == 0) return result1;
            return (result1 < result2) ? result1 : result2;
        }

        private decimal CalculateSheet(Option option)
        {
            decimal result1 = 0;
            decimal result2 = 0;

            foreach (SheetSize sheetSize in option.SheetSizes.OrderBy(ss => ss.Price))
            {
                if (dim1 <= sheetSize.Side1 && dim2 <= sheetSize.Side2)
                {
                    result1 = sheetSize.Price;
                    break;
                }
            }
            // change orientation and try again
            foreach (SheetSize sheetSize in option.SheetSizes.OrderBy(ss => ss.Price))
            {
                if (dim1 <= sheetSize.Side2 && dim2 <= sheetSize.Side1)
                {
                    result2 = sheetSize.Price;
                    break;
                }
            }

            if (result1 == 0) return result2;
            if (result2 == 0) return result1;
            return (result1 < result2) ? result1 : result2;
        }

    }
    
 

}