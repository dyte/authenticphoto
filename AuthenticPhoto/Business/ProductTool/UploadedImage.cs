﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using AuthenticPhoto.Business.ProductTool.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace AuthenticPhoto.Business.ProductTool
{
    public class UploadedImage
    {
        public Dictionary<int, Option> preferences = new Dictionary<int, Option>();
        public Dictionary<int, Price> prices = new Dictionary<int, Price>();
        public Dictionary<string, Price> extraPrices = new Dictionary<string, Price>();

        private decimal dim1 = 0;
        private decimal dim2 = 0;

        private int quantity = 1;
        private int borderWidth = 0;
        private TestPrintOption testPrint = TestPrintOption.None;

        private Signature signature = new Signature
        {
            SignatureActive = false,
            SignatureLocation = SignLocation.OnBackBehindPlexi,
            SignaturePosition = SignPosition.BelowLeft,
            SignatureLabelSize = SignLabelSize.S105x70,
            SignatureDelivery = SignDelivery.SelfSignInImage,
            SignatureNoWarrantyLabel = false
        };

        public enum TestPrintOption
        {
            None,
            A4,
            OneOne
        }

        //public string UploaderID { get; set; } // id fineuploader assigned to it
        public string ID { get; set; }
        public string OrderID { get; set; }
        public string OriginalFilename { get; set; }

        private string filenameAnnex = "";
        public string FilenameAnnex { get { return filenameAnnex; } set { filenameAnnex = value; } }

        public int ThumbWidth { get; set; }
        public int ThumbHeight { get; set; }

        //public string PrevSource { get; set; }

        public int Quantity { get { return quantity; } set { quantity = value; } }

        // IN CM
        public int BorderWidth { get { return borderWidth; } set { borderWidth = value; } }

        public int DimPx1 { get; set; }
        public int DimPx2 { get; set; }
        public int XResolution { get; set; }

        // IN MM
        public decimal Dim1 { get { return Math.Round(dim1); } set { dim1 = value; } }
        public decimal Dim2 { get { return Math.Round(dim2); } set { dim2 = value; } }
        
        public decimal Dim1Cm { get { return Dim1 / 10; } set { Dim1 = value * 10; } }
        public decimal Dim2Cm { get { return Dim2 / 10; } set { Dim2 = value * 10;} }

        public bool UsingCustomDimensions { get; set; }

        public TestPrintOption Testprint { get { return testPrint; } set { testPrint = value; } }

        public bool IsProcessed { get; set; }

        public bool Softproof { get; set; }
        public bool InspectBeforePrint { get; set; }
        public bool DontCut { get; set; }

        public decimal OrigDim1 { get; set; }
        public decimal OrigDim2 { get; set; }

        public decimal OrigDim1Cm { get { return OrigDim1 / 10; } set { OrigDim1 = value * 10; } }
        public decimal OrigDim2Cm { get { return OrigDim2 / 10; } set { OrigDim2 = value * 10; } }

        public int OrigXResolution { get; set; }

        public string ICCProfile { get; set; }

        public Product SelectedProduct { get; set; }
        public Dictionary<int, Option> Preferences { get { return preferences; } set { this.preferences = value; } }
        public Dictionary<int, Price> Prices { get { return prices; } set { this.prices = value; } }

        public Dictionary<string, Price> ExtraPrices { get { return extraPrices; } set { this.extraPrices = value; } }

        public Signature Signature { get { return signature; } set { signature = value; } }

        public string LabMessage { get; set; }

        public DateTime DateAdded { get; set; }

        public decimal GetTestPrintPrice() // hacky, return testprint price of first option with this filled in (should only be one - media)
        {
            foreach (Option o in Preferences.Values)
            {
                decimal tpPrice = (Testprint == TestPrintOption.A4) ? o.TestprintA4 : o.Testprint11;
                if (tpPrice > 0)
                {
                    return tpPrice;
                }
            }
            return 0;
        }
        public decimal GetSoftProofPrice()
        {
            return 4; //TODO: in admin
        }

        public decimal GetTotalPrice()
        {
            decimal price = 0;
            foreach (Option option in Preferences.Values.Where(v => v != null))
            {
                price += Prices.ContainsKey(option.OptionID) ? Prices[option.OptionID].GetValue() : 0;
            }
            foreach(decimal extraPrice in ExtraPrices.Values.Select(ep => ep.StandardPrice))
            {
                price += extraPrice;
            }
            return price;
        }
        public decimal GetPriceOfRequiredOptions()
        {
            decimal price = 0;
            foreach (Option option in Preferences.Values.Where(v => v != null && v.OptionCategory.OptionType.Required))
            {
                price += Prices.ContainsKey(option.OptionID) ? Prices[option.OptionID].GetValue() : 0;
            }
            return price;
        }
        public void ResetPrices()
        {
            prices.Clear();
            ExtraPrices.Clear();
        }

        public string GetWorkName()
        {
            return ID + ".jpg";
        }
        public string GetThumbName()
        {
            return "thumb_" + ID + ".jpg";
        }

        public string GetThumbSource()
        {
            if (IsProcessed)
            {
                return ConfigurationManager.AppSettings["ProcessedSource"] + OrderID + "/0/" + GetThumbName();
            }
            else
            {
                return ConfigurationManager.AppSettings["InProcessImg"];
            }
        }

        public void ResetDimensions()
        {
            Dim1 = OrigDim1;
            Dim2 = OrigDim2;
            XResolution = OrigXResolution;
        }


        public double GetWeight()
        {
            double tot = 0;
            foreach (Option option in Preferences.Values.Where(v => v != null))
            {
                tot += option.CalculateWeight(Dim1, Dim2);
            }
            return tot;
        }

        public bool IsReadyForCheckout()
        {
            if (SelectedProduct == null)
            {
                return false;
            }
            else if (SelectedProduct.PreselectOptions)
            {
                return true;
            }
            else
            {
                foreach (OptionType reqOptionType in SelectedProduct.OptionTypes.Where(ot => ot.Required == true))
                {
                    if (!preferences.ContainsKey(reqOptionType.OptionTypeID))
                    {
                        return false;
                    }
                    else if (preferences[reqOptionType.OptionTypeID] == null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public string GetFullName()
        {
            return (OriginalFilename + " " + FilenameAnnex).Trim();
        }

        public void CalculatePrice(string discountsOnProducts = "")
        {
            ResetPrices();

            decimal TotalDim1 = Dim1 + (BorderWidth * 10);
            decimal TotalDim2 = Dim2 + (BorderWidth * 10);

            PriceCalculator priceCalculator = new PriceCalculator(TotalDim1, TotalDim2);

            Dictionary<string, string> discountsDict = string.IsNullOrWhiteSpace(discountsOnProducts) ? new Dictionary<string, string>() : JsonConvert.DeserializeObject<Dictionary<string, string>>(discountsOnProducts);

            if (SelectedProduct == null)
                return;

            List<int> activeOptionTypeIds = SelectedProduct.OptionTypes.Select(ot => ot.OptionTypeID).ToList();

            foreach (Option option in Preferences.Values.Where(o => o != null && activeOptionTypeIds.Contains(o.OptionCategory.OptionType.OptionTypeID)))
            {
                string optId = Convert.ToString(option.OptionID);                                
                Price price = new Price { StandardPrice = priceCalculator.Calculate(option) };

                if (discountsDict.ContainsKey(optId))
                {
                    decimal discount;
                    if (decimal.TryParse(discountsDict[optId], out discount))
                    {
                        price.DiscountPrice = ApplyDiscount(price.StandardPrice, discount);
                        price.HasDiscount = true;
                        price.DiscountPerc = discount;
                    }
                }
                if (Prices.ContainsKey(option.OptionID))
                {
                    Prices[option.OptionID] = price;
                }
                else
                {
                    Prices.Add(option.OptionID, price);
                }
            }
            if(Testprint != TestPrintOption.None)
            {
                ExtraPrices["tp"] = new Price { StandardPrice = GetTestPrintPrice() };
            }
            if (Softproof)
            {
                ExtraPrices["sp"] = new Price { StandardPrice = GetSoftProofPrice() };
            }
        }

        private decimal ApplyDiscount(decimal price, decimal discount)
        {
            return Math.Round(price * (discount/100), 2);
        }

        internal static UploadedImage CreateCalcImage()
        {
            UploadedImage CalcImage = new UploadedImage();
            return CalcImage;
        }

    }
    public class Price
    {
        public decimal StandardPrice { get; set; }
        public decimal DiscountPrice { get; set; }
        public bool HasDiscount { get; set; }
        public decimal DiscountPerc { get; set; }

        public decimal GetValue()
        {
            return HasDiscount ? DiscountPrice : StandardPrice;
        }
        public string ToJson()
        {
            var json = new JObject();
            json["StandardPrice"] = StandardPrice;
            json["DiscountPrice"] = DiscountPrice;
            json["HasDiscount"] = HasDiscount;
            json["DiscountPerc"] = DiscountPerc;
            return json.ToString();
        }

        internal static Price FromPriceJson(string json)
        {
            Price price = new Price();
            JObject priceJson = JObject.Parse(json);
            decimal stdPrice;
            decimal discPrice;
            decimal discountPerc;
            foreach (JProperty priceProp in priceJson.Properties())
            {
                switch (priceProp.Name)
                {
                    case "StandardPrice": price.StandardPrice = decimal.TryParse(priceProp.Value.ToString(), out stdPrice) ? stdPrice : 0; break;
                    case "DiscountPrice": price.DiscountPrice = decimal.TryParse(priceProp.Value.ToString(), out discPrice) ? discPrice : 0; break;
                    case "HasDiscount": price.HasDiscount = (priceProp.Value.ToString().ToLower() == "true" || priceProp.Value.ToString() == "1"); break;
                    case "DiscountPerc": price.DiscountPerc = decimal.TryParse(priceProp.Value.ToString(), out discountPerc) ? discountPerc : 0; break;
                }
            }
            return price;
        }
    }


}