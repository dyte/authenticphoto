﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using AuthenticPhoto.Business.ProductTool.Entities;
using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Controllers;
using AuthenticPhoto.Business.AuthenticOrder;

namespace AuthenticPhoto.Business.ProductTool
{
    public class DbConnector
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString;
        const string GET_PROCESSED_IMGS = "SELECT UploadedImageID, ThumbWidth, ThumbHeight, OrigDim1, OrigDim2, OrigXResolution, OriginalFilename, FilenameAnnex FROM UploadedImages WHERE OrderID = @OrderID AND IsProcessed = 1 AND UploadedImageID NOT IN ({0})";

        
        const string GET_IMG = "SELECT * FROM UploadedImages WHERE UploadedImageID = @UploadedImageID";
        const string GET_CHECKOUT_PRODUCT = "SELECT * FROM CheckoutProducts WHERE CheckoutProductID = @CheckoutProductID";
        const string DELETE_IMG = "DELETE FROM UploadedImages WHERE UploadedImageID = @UploadedImageID";

        const string GET_IMGS_IN_ORDER = "SELECT * FROM UploadedImages WHERE OrderID = @OrderID";

        const string GET_IMAGE_IDS_FOR_ORDER = "SELECT UploadedImageID, IsProcessed FROM UploadedImages WHERE OrderID = @OrderID AND UploadedImageID IN ({0})";

        const string UPDATE_IMG =
            "UPDATE UploadedImages SET ThumbWidth = @ThumbWidth, " +
            "ThumbHeight = @ThumbHeight, Quantity = @Quantity, DimPx1 = @DimPx1, DimPx2 = @DimPx2, XResolution = @XResolution, " +
            "Dim1 = @Dim1, Dim2 = @Dim2, UsingCustomDimensions = @UsingCustomDimensions, Testprint = @Testprint, " +
            "Softproof = @Softproof, InspectBeforePrint = @InspectBeforePrint, DontCut = @DontCut, " +
            "OrigDim1 = @OrigDim1, OrigDim2 = @OrigDim2, OrigXResolution = @OrigXResolution, ICCProfile = @ICCProfile, " +
            "ProductID = @ProductID, DateAdded = @DateAdded, Preferences = @Preferences, Prices = @Prices, OrderID = @OrderID, " +
            "Signature = @Signature, LabMessage = @LabMessage, BorderWidth = @BorderWidth WHERE UploadedImageID = @UploadedImageID";

        const string UPDATE_IMG_EXCL_SIZES =
            "UPDATE UploadedImages SET Quantity = @Quantity, Testprint = @Testprint, " +
            "Softproof = @Softproof, InspectBeforePrint = @InspectBeforePrint, DontCut = @DontCut, BorderWidth = @BorderWidth, " +
            "ICCProfile = @ICCProfile, ProductID = @ProductID, DateAdded = @DateAdded, Preferences = @Preferences, " +
            "Prices = @Prices, OrderID = @OrderID, " +
            "Signature = @Signature, LabMessage = @LabMessage WHERE UploadedImageID = @UploadedImageID";

        const string ADD_PLACEHOLDER_IMG =
            "INSERT INTO UploadedImages (UploadedImageID, OriginalFilename, OrderID, DateAdded) VALUES (@UploadedImageID, @OriginalFilename, @OrderID, CURRENT_TIMESTAMP)";

        const string ADD_IMG =
            "INSERT INTO UploadedImages (UploadedImageID, OriginalFilename, FilenameAnnex, ThumbWidth, " +
            "ThumbHeight, Quantity, DimPx1, DimPx2, XResolution, Dim1, Dim2, UsingCustomDimensions, Testprint, " +
            "Softproof, InspectBeforePrint, DontCut, OrigDim1, OrigDim2, OrigXResolution, ICCProfile, " +
            "ProductID, DateAdded, Preferences, Prices, Signature, LabMessage, BorderWidth, OrderID) " +
            "VALUES (@UploadedImageID, @OriginalFilename, @FilenameAnnex, @ThumbWidth, " +
            "@ThumbHeight, @Quantity, @DimPx1, @DimPx2, @XResolution, @Dim1, @Dim2, @UsingCustomDimensions, @Testprint, " +
            "@Softproof, @InspectBeforePrint, @DontCut, @OrigDim1, @OrigDim2, @OrigXResolution, @ICCProfile, " +
            "@ProductID, @DateAdded, @Preferences, @Prices, @Signature, @LabMessage, @BorderWidth, @OrderID)";

        const string GET_ORDER = "SELECT * FROM Orders WHERE OrderID = @OrderID";

        const string ADD_ORDER =
            "INSERT INTO Orders (OrderID, Delivery, Payment, Submitted, CreationDate) VALUES (@OrderID, @Delivery, @Payment, 0, @CreationDate)";

        const string UPDATE_ORDER_CHECKOUTPRODUCTS = "UPDATE Orders SET CheckoutProductIds = @CheckoutProductIds WHERE OrderID = @OrderID";
        const string UPDATE_ORDER_DELIVERYOPTION = "UPDATE Orders SET Delivery = @DeliveryOption WHERE OrderID = @OrderID";
        const string UPDATE_ORDER_PAYMENTOPTION = "UPDATE Orders SET Payment = @PaymentOption WHERE OrderID = @OrderID";

        const string UPDATE_ORDER_ON_SUBMIT = "UPDATE Orders SET SubmissionDate = @SubmissionDate, PublicOrderID = @PublicOrderID, DeliveryAddress = @DeliveryAddress, Reference = @Reference, DiscountCode = @DiscountCode, Submitted = 1, UserID = @UserID WHERE OrderID = @OrderID";

        private IProductRepository repository;

        public DbConnector(IProductRepository productRepository)
        {
            repository = productRepository;
        }

        public void UpdateOrderWithCheckoutProducts(string orderID, List<CheckoutProduct> selectedCheckoutProducts)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(UPDATE_ORDER_CHECKOUTPRODUCTS, connection))
                {
                    cmd.Parameters.AddWithValue("CheckoutProductIds", CheckoutProductsToIdString(selectedCheckoutProducts));
                    cmd.Parameters.AddWithValue("OrderID", int.Parse(orderID));
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }
        public void UpdateOrderOnSubmit(AuthenticOrder order, long userID)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(UPDATE_ORDER_ON_SUBMIT, connection))
                {
                    cmd.Parameters.AddWithValue("OrderID", order.OrderID);
                    cmd.Parameters.AddWithValue("PublicOrderID", order.PublicOrderID);
                    if (order.Delivery.Equals("transport") && order.DeliveryAddress != null)
                    {
                        cmd.Parameters.AddWithValue("DeliveryAddress", order.DeliveryAddress.ToJson());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("DeliveryAddress", "");
                    }
                    cmd.Parameters.AddWithValue("Reference", order.Reference);
                    cmd.Parameters.AddWithValue("DiscountCode", order.DiscountCode);
                    cmd.Parameters.AddWithValue("SubmissionDate", DateTime.Now);
                    cmd.Parameters.AddWithValue("UserID", userID);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }


        public void UpdateOrderWithDeliveryOption(string orderID, string delivery)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(UPDATE_ORDER_DELIVERYOPTION, connection))
                {
                    cmd.Parameters.AddWithValue("DeliveryOption", delivery);
                    cmd.Parameters.AddWithValue("OrderID", int.Parse(orderID));
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }
        public void UpdateOrderWithPaymentOption(string orderID, string payment)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(UPDATE_ORDER_PAYMENTOPTION, connection))
                {
                    cmd.Parameters.AddWithValue("PaymentOption", payment);
                    cmd.Parameters.AddWithValue("OrderID", int.Parse(orderID));
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public void DeleteImage(string imageID)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(DELETE_IMG, connection))
            {
                cmd.Parameters.AddWithValue("UploadedImageID", imageID);
                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }
        public UploadedImage GetImage(string imageID)
        {
            UploadedImage img = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_IMG, connection))
            {
                cmd.Parameters.AddWithValue("UploadedImageID", imageID);
                connection.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        img = ExtractImageFromReader(reader);
                    }
                }
            }
            return img;
        }

        public List<UploadedImage> GetImagesInOrder(string orderID)
        {
            List<UploadedImage> uploadedImages = new List<UploadedImage>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                uploadedImages = GetImagesInOrder(orderID, connection);
                connection.Close();
            }
            return uploadedImages;
        }
        private List<UploadedImage> GetImagesInOrder(string orderID, SqlConnection connection)
        {
            List<UploadedImage> uploadedImages = new List<UploadedImage>();
            using (SqlCommand cmd = new SqlCommand(GET_IMGS_IN_ORDER, connection))
            {
                cmd.Parameters.AddWithValue("OrderID", orderID);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            uploadedImages.Add(ExtractImageFromReader(reader));
                        }
                    }
                }
            }
            return uploadedImages;
        }
        
        private bool ImageExists(SqlConnection connection, string ImageID)
        {
            using (SqlCommand cmd = new SqlCommand(GET_IMG, connection))
            {
                cmd.Parameters.AddWithValue("UploadedImageID", ImageID);
                connection.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    bool exists = reader.HasRows;
                    connection.Close();
                    return exists;
                }
            }
        }

        private AuthenticOrder ExtractOrderFromReader(SqlDataReader reader)
        {
            AuthenticOrder order = new AuthenticOrder();
            order.OrderID = reader.GetString(reader.GetOrdinal("OrderID"));
            if (!reader.IsDBNull(reader.GetOrdinal("UploadedImageIds")))
                order.Images = ImagesFromIdsString(reader.GetString(reader.GetOrdinal("UploadedImageIds")));
            if (!reader.IsDBNull(reader.GetOrdinal("CheckoutProductIds")))
                order.SelectedCheckoutProducts = CheckoutProductsFromIdsString(reader.GetString(reader.GetOrdinal("CheckoutProductIds")));
            if (!reader.IsDBNull(reader.GetOrdinal("Delivery")))
                order.Delivery = reader.GetString(reader.GetOrdinal("Delivery"));
            if (!reader.IsDBNull(reader.GetOrdinal("Payment")))
                order.Payment = reader.GetString(reader.GetOrdinal("Payment"));
            if (!reader.IsDBNull(reader.GetOrdinal("CreationDate")))
                order.CreationDate = reader.GetDateTime(reader.GetOrdinal("CreationDate"));

            return order;
        }

        private UploadedImage ExtractImageFromReader(SqlDataReader reader)
        {
            UploadedImage img = new UploadedImage();
            img.ID = reader.GetString(reader.GetOrdinal("UploadedImageID"));
            img.OrderID = reader.GetString(reader.GetOrdinal("OrderID"));
            if (!reader.IsDBNull(reader.GetOrdinal("OriginalFileName")))
                img.OriginalFilename = reader.GetString(reader.GetOrdinal("OriginalFileName"));
            if (!reader.IsDBNull(reader.GetOrdinal("FilenameAnnex")))
                img.FilenameAnnex = reader.GetString(reader.GetOrdinal("FilenameAnnex"));
            if (!reader.IsDBNull(reader.GetOrdinal("ThumbWidth")))
                img.ThumbWidth = reader.GetInt32(reader.GetOrdinal("ThumbWidth"));
            if (!reader.IsDBNull(reader.GetOrdinal("ThumbHeight")))
                img.ThumbHeight = reader.GetInt32(reader.GetOrdinal("ThumbHeight"));
            if (!reader.IsDBNull(reader.GetOrdinal("Quantity")))
                img.Quantity = reader.GetInt32(reader.GetOrdinal("Quantity"));
            if (!reader.IsDBNull(reader.GetOrdinal("BorderWidth")))
                img.BorderWidth = reader.GetInt32(reader.GetOrdinal("BorderWidth"));
            if (!reader.IsDBNull(reader.GetOrdinal("DimPx1")))
                img.DimPx1 = reader.GetInt32(reader.GetOrdinal("DimPx1"));
            if (!reader.IsDBNull(reader.GetOrdinal("DimPx2")))
                img.DimPx2 = reader.GetInt32(reader.GetOrdinal("DimPx2"));
            if (!reader.IsDBNull(reader.GetOrdinal("Dim1")))
                img.Dim1 = reader.GetDecimal(reader.GetOrdinal("Dim1"));
            if (!reader.IsDBNull(reader.GetOrdinal("Dim2")))
                img.Dim2 = reader.GetDecimal(reader.GetOrdinal("Dim2"));
            if (!reader.IsDBNull(reader.GetOrdinal("XResolution")))
                img.XResolution = reader.GetInt32(reader.GetOrdinal("XResolution"));
            if (!reader.IsDBNull(reader.GetOrdinal("UsingCustomDimensions")))
                img.UsingCustomDimensions = reader.GetBoolean(reader.GetOrdinal("UsingCustomDimensions"));
            if (!reader.IsDBNull(reader.GetOrdinal("InspectBeforePrint")))
                img.InspectBeforePrint = reader.GetBoolean(reader.GetOrdinal("InspectBeforePrint"));
            if (!reader.IsDBNull(reader.GetOrdinal("DontCut")))
                img.DontCut = reader.GetBoolean(reader.GetOrdinal("DontCut"));
            if (!reader.IsDBNull(reader.GetOrdinal("OrigDim1")))
                img.OrigDim1 = reader.GetDecimal(reader.GetOrdinal("OrigDim1"));
            if (!reader.IsDBNull(reader.GetOrdinal("OrigDim2")))
                img.OrigDim2 = reader.GetDecimal(reader.GetOrdinal("OrigDim2"));
            if (!reader.IsDBNull(reader.GetOrdinal("OrigXResolution")))
                img.OrigXResolution = reader.GetInt32(reader.GetOrdinal("OrigXResolution"));
            if (!reader.IsDBNull(reader.GetOrdinal("ICCProfile")))
                img.ICCProfile = reader.GetString(reader.GetOrdinal("ICCProfile"));
            if (!reader.IsDBNull(reader.GetOrdinal("IsProcessed")))
                img.IsProcessed = reader.GetBoolean(reader.GetOrdinal("IsProcessed"));
            if (!reader.IsDBNull(reader.GetOrdinal("ProductID")))
            {
                int productID = reader.GetInt32(reader.GetOrdinal("ProductID"));
                if (productID != 0)
                    img.SelectedProduct = repository.GetProductWithPopulatedChildren(productID);
            }
            if (!reader.IsDBNull(reader.GetOrdinal("DateAdded")))
                img.DateAdded = reader.GetDateTime(reader.GetOrdinal("DateAdded"));

            if (!reader.IsDBNull(reader.GetOrdinal("Preferences")))
                img.Preferences = ParsePrefsJson(reader.GetString(reader.GetOrdinal("Preferences")));

            if (!reader.IsDBNull(reader.GetOrdinal("Prices")))
                img.Prices = ParsePricesJson(reader.GetString(reader.GetOrdinal("Prices")));

            if (!reader.IsDBNull(reader.GetOrdinal("Signature")))
                img.Signature = new Signature(reader.GetString(reader.GetOrdinal("Signature")));

            if (!reader.IsDBNull(reader.GetOrdinal("LabMessage")))
                img.LabMessage = reader.GetString(reader.GetOrdinal("LabMessage"));

            if (!reader.IsDBNull(reader.GetOrdinal("Testprint")))
            {
                string tp = reader.GetString(reader.GetOrdinal("Testprint"));
                img.Testprint = (UploadedImage.TestPrintOption)Enum.Parse(typeof(UploadedImage.TestPrintOption), tp);
            }
            if (!reader.IsDBNull(reader.GetOrdinal("Softproof")))
                img.Softproof = reader.GetBoolean(reader.GetOrdinal("Softproof"));

            return img;
        }

        public void SaveImage(UploadedImage image, SqlConnection connection)
        {
            if (ImageExists(connection, image.ID))
            {
                UpdateImage(image, connection);
            }
            else
            {
                AddImage(image, connection);
            }
        }
        public void SaveImage(UploadedImage image)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SaveImage(image, connection);
            }
        }

        public AuthenticOrder AddOrder()
        {
            AuthenticOrder newOrder = new AuthenticOrder { OrderID = AuthenticOrderIDManager.GenerateOrderID(), CreationDate = DateTime.Now };
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ADD_ORDER, connection))
                {
                    cmd.Parameters.AddWithValue("OrderID", newOrder.OrderID);
                    cmd.Parameters.AddWithValue("Delivery", newOrder.Delivery ?? "");
                    cmd.Parameters.AddWithValue("Payment", newOrder.Payment ?? "");
                    cmd.Parameters.AddWithValue("CreationDate", newOrder.CreationDate);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            return newOrder;
        }

        public void AddImages(List<UploadedImage> images)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                foreach (UploadedImage image in images)
                {
                    AddImage(image, connection);
                }
            }
        }

        private void UpdateImage(UploadedImage image, SqlConnection connection)
        {
            if (image.IsProcessed)
            {
                using (SqlCommand cmd = new SqlCommand(UPDATE_IMG, connection))
                {
                    cmd.Parameters.AddWithValue("UploadedImageID", image.ID);
                    cmd.Parameters.AddWithValue("OrderID", image.OrderID);
                    cmd.Parameters.AddWithValue("ThumbWidth", image.ThumbWidth);
                    cmd.Parameters.AddWithValue("ThumbHeight", image.ThumbHeight);
                    cmd.Parameters.AddWithValue("Quantity", image.Quantity);
                    cmd.Parameters.AddWithValue("BorderWidth", image.BorderWidth);
                    cmd.Parameters.AddWithValue("Testprint", image.Testprint.ToString() ?? "");
                    cmd.Parameters.AddWithValue("Softproof", image.Softproof);
                    cmd.Parameters.AddWithValue("InspectBeforePrint", image.InspectBeforePrint);
                    cmd.Parameters.AddWithValue("DontCut", image.DontCut);
                    cmd.Parameters.AddWithValue("ICCProfile", image.ICCProfile ?? "");
                    cmd.Parameters.AddWithValue("ProductID", image.SelectedProduct != null ? image.SelectedProduct.ProductID : 0);
                    cmd.Parameters.AddWithValue("DateAdded", image.DateAdded);
                    cmd.Parameters.AddWithValue("Preferences", ToPrefsJson(image.Preferences));
                    cmd.Parameters.AddWithValue("Prices", ToPricesJson(image.Prices));
                    cmd.Parameters.AddWithValue("Signature", image.Signature.ToJson());
                    cmd.Parameters.AddWithValue("LabMessage", image.LabMessage ?? "");
                    cmd.Parameters.AddWithValue("OrigDim1", image.OrigDim1);
                    cmd.Parameters.AddWithValue("OrigDim2", image.OrigDim2);
                    cmd.Parameters.AddWithValue("DimPx1", image.DimPx1);
                    cmd.Parameters.AddWithValue("DimPx2", image.DimPx2);
                    cmd.Parameters.AddWithValue("XResolution", image.XResolution);
                    cmd.Parameters.AddWithValue("Dim1", image.Dim1);
                    cmd.Parameters.AddWithValue("Dim2", image.Dim2);
                    cmd.Parameters.AddWithValue("UsingCustomDimensions", image.UsingCustomDimensions);
                    cmd.Parameters.AddWithValue("OrigXResolution", image.OrigXResolution);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            else // don't update sizes if image isn't fully processed (potential conflict with imageprocessor service db access (should be ui prevented anyway))
            {
                using (SqlCommand cmd = new SqlCommand(UPDATE_IMG_EXCL_SIZES, connection))
                {
                    cmd.Parameters.AddWithValue("UploadedImageID", image.ID);
                    cmd.Parameters.AddWithValue("OrderID", image.OrderID);
                    cmd.Parameters.AddWithValue("OriginalFilename", image.OriginalFilename ?? "");
                    cmd.Parameters.AddWithValue("Quantity", image.Quantity);
                    cmd.Parameters.AddWithValue("BorderWidth", image.BorderWidth);
                    cmd.Parameters.AddWithValue("Testprint", image.Testprint.ToString() ?? "");
                    cmd.Parameters.AddWithValue("Softproof", image.Softproof);
                    cmd.Parameters.AddWithValue("InspectBeforePrint", image.InspectBeforePrint);
                    cmd.Parameters.AddWithValue("DontCut", image.DontCut);
                    cmd.Parameters.AddWithValue("ICCProfile", image.ICCProfile ?? "");
                    cmd.Parameters.AddWithValue("ProductID", image.SelectedProduct != null ? image.SelectedProduct.ProductID : 0);
                    cmd.Parameters.AddWithValue("DateAdded", image.DateAdded);
                    cmd.Parameters.AddWithValue("Preferences", ToPrefsJson(image.Preferences));
                    cmd.Parameters.AddWithValue("Prices", ToPricesJson(image.Prices));
                    cmd.Parameters.AddWithValue("Signature", image.Signature.ToJson());
                    cmd.Parameters.AddWithValue("LabMessage", image.LabMessage ?? "");
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }

            }


        }

        public void AddPlaceholderImage(string imageID, string originalFilename, string orderID)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ADD_PLACEHOLDER_IMG, connection))
                {
                    cmd.Parameters.AddWithValue("UploadedImageID", imageID);
                    cmd.Parameters.AddWithValue("OriginalFilename", originalFilename);
                    cmd.Parameters.AddWithValue("OrderID", orderID);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        private void AddImage(UploadedImage image, SqlConnection connection)
        {
            using (SqlCommand cmd = new SqlCommand(ADD_IMG, connection))
            {
                cmd.Parameters.AddWithValue("UploadedImageID", image.ID);
                cmd.Parameters.AddWithValue("OrderID", image.OrderID);
                cmd.Parameters.AddWithValue("OriginalFilename", image.OriginalFilename ?? "");
                cmd.Parameters.AddWithValue("FilenameAnnex", image.FilenameAnnex);
                cmd.Parameters.AddWithValue("ThumbWidth", image.ThumbWidth);
                cmd.Parameters.AddWithValue("ThumbHeight", image.ThumbHeight);
                cmd.Parameters.AddWithValue("Quantity", image.Quantity);
                cmd.Parameters.AddWithValue("BorderWidth", image.BorderWidth);
                cmd.Parameters.AddWithValue("Testprint", image.Testprint.ToString() ?? "");
                cmd.Parameters.AddWithValue("Softproof", image.Softproof);
                cmd.Parameters.AddWithValue("InspectBeforePrint", image.InspectBeforePrint);
                cmd.Parameters.AddWithValue("DontCut", image.DontCut);
                cmd.Parameters.AddWithValue("ICCProfile", image.ICCProfile ?? "");
                cmd.Parameters.AddWithValue("ProductID", image.SelectedProduct != null ? image.SelectedProduct.ProductID : 0);
                cmd.Parameters.AddWithValue("DateAdded", image.DateAdded);
                cmd.Parameters.AddWithValue("Preferences", ToPrefsJson(image.Preferences));
                cmd.Parameters.AddWithValue("Prices", ToPricesJson(image.Prices));
                cmd.Parameters.AddWithValue("Signature", image.Signature.ToJson());
                cmd.Parameters.AddWithValue("LabMessage", image.LabMessage ?? "");
                cmd.Parameters.AddWithValue("OrigDim1", image.OrigDim1);
                cmd.Parameters.AddWithValue("OrigDim2", image.OrigDim2);
                cmd.Parameters.AddWithValue("DimPx1", image.DimPx1);
                cmd.Parameters.AddWithValue("DimPx2", image.DimPx2);
                cmd.Parameters.AddWithValue("XResolution", image.XResolution);
                cmd.Parameters.AddWithValue("Dim1", image.Dim1);
                cmd.Parameters.AddWithValue("Dim2", image.Dim2);
                cmd.Parameters.AddWithValue("UsingCustomDimensions", image.UsingCustomDimensions);
                cmd.Parameters.AddWithValue("OrigXResolution", image.OrigXResolution);                
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        private void UpdateOrder(AuthenticOrder order, SqlConnection connection, string query, bool insert)
        {

        }

        public string ToIdsString(List<UploadedImage> images)
        {
            return string.Join(",", images.Select(i => i.ID));
        }
        public List<UploadedImage> ImagesFromIdsString(string imageIds)
        {
            List<UploadedImage> images = new List<UploadedImage>();
            foreach (string id in imageIds.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
            {
                images.Add(GetImage(id));
            }
            return images;
        }
        public string CheckoutProductsToIdString(List<CheckoutProduct> coProducts)
        {
            string idstring = "";
            foreach (CheckoutProduct product in coProducts)
            {
                for (int i = 0; i < product.Qty; i++)
                {
                    idstring += product.CheckoutProductID + ",";
                }
            }
            if (idstring.EndsWith(","))
            {
                idstring = idstring.Substring(0, idstring.Length - 1);
            }
            return idstring;
        }
        public List<CheckoutProduct> CheckoutProductsFromIdsString(string coProductIds)
        {
            List<CheckoutProduct> coProducts = new List<CheckoutProduct>();
            foreach (string id in coProductIds.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (coProducts.Any(cop => cop.CheckoutProductID == int.Parse(id)))
                {
                    coProducts.FirstOrDefault(cop => cop.CheckoutProductID == int.Parse(id)).Qty++;
                }
                else
                {
                    CheckoutProduct newProduct = repository.GetCheckoutProduct(int.Parse(id));
                    newProduct.Qty = 1;
                    coProducts.Add(newProduct);
                }
            }
            return coProducts;
        }

        public string ToPrefsJson(Dictionary<int, Option> preferences)
        {
            var prefsJson = new JObject();
            foreach (int id in preferences.Keys)
            {
                prefsJson[Convert.ToString(id)] = preferences[id] == null ? "0" : Convert.ToString(preferences[id].OptionID);
            }
            return prefsJson.ToString();
        }



        public Dictionary<int, Option> ParsePrefsJson(string json)
        {
            Dictionary<int, Option> preferences = new Dictionary<int, Option>();

            JObject idJson = JObject.Parse(json);
            foreach (JProperty pref in idJson.Properties())
            {
                int id;
                int optionid;

                if (int.TryParse(pref.Name, out id))
                {
                    if (pref.Value.ToString().Trim().Equals("0"))
                    {
                        preferences[id] = null;
                    }
                    else if (int.TryParse(pref.Value.ToString(), out optionid))
                    {
                        preferences[id] = repository.GetOption(optionid);
                    }
                }
            }
            return preferences;
        }

        public string ToPricesJson(Dictionary<int, Price> prices)
        {
            var pricesJson = new JObject();
            foreach (int id in prices.Keys)
            {
                pricesJson[Convert.ToString(id)] = prices[id].ToJson();
            }
            return pricesJson.ToString();
        }
        public Dictionary<int, Price> ParsePricesJson(string json)
        {
            Dictionary<int, Price> prices = new Dictionary<int, Price>();

            JObject pricesJson = JObject.Parse(json);
            foreach (JProperty priceSet in pricesJson.Properties())
            {
                int id;
                if (int.TryParse(priceSet.Name, out id))
                {
                    string priceJson = priceSet.Value.ToString();
                    prices[id] = Price.FromPriceJson(priceJson);
                }
            }
            return prices;
        }


        public string[] GetUnprocessedImages(string orderID, string[] candidateIds)
        {
            List<string> unprocessedIds = candidateIds.ToList();

            string candidateIdsString = string.Join(",", candidateIds);
            candidateIdsString = "'" + candidateIdsString.Replace(",", "','") + "'";
            string stmt = string.Format(GET_IMAGE_IDS_FOR_ORDER, candidateIdsString);

            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(stmt, connection))
            {
                cmd.Parameters.AddWithValue("OrderID", orderID);
                connection.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string id = reader.GetString(reader.GetOrdinal("UploadedImageID"));
                            if (!reader.IsDBNull(reader.GetOrdinal("IsProcessed")))
                            {
                                if (reader.GetBoolean(reader.GetOrdinal("IsProcessed"))) {
                                    unprocessedIds.Remove(id);
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new NoImagesInOrderException();
                    }
                }
            }
            return unprocessedIds.ToArray();
        }

        public AuthenticOrder GetOrder(string orderID)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(GET_ORDER, connection))
                {
                    cmd.Parameters.AddWithValue("OrderID", orderID);
                    connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            {
                                AuthenticOrder order = ExtractOrderFromReader(reader);
                                return order;
                            }
                        }
                    }
                    connection.Close();
                }
                return null;
            }
        }

        public string GetProcessedImagesJson(string orderID, string[] alreadyProcessed)
        {
            JObject processedImages = new JObject();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string alreadyProcessedString = string.Join(",", alreadyProcessed);
                alreadyProcessedString = "'" + alreadyProcessedString.Replace(",", "','") + "'";

                string stmt = string.Format(GET_PROCESSED_IMGS, alreadyProcessedString);
                using (SqlCommand cmd = new SqlCommand(stmt, connection))
                {
                    cmd.Parameters.AddWithValue("OrderID", int.Parse(orderID));
                    connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var processedImage = new JObject();

                                string imgID = reader.GetString(reader.GetOrdinal("UploadedImageID"));

                                if (!reader.IsDBNull(reader.GetOrdinal("ThumbWidth")))
                                    processedImage["thumbwidth"] = reader.GetInt32(reader.GetOrdinal("ThumbWidth"));
                                if (!reader.IsDBNull(reader.GetOrdinal("ThumbHeight")))
                                    processedImage["thumbheight"] = reader.GetInt32(reader.GetOrdinal("ThumbHeight"));
                                if (!reader.IsDBNull(reader.GetOrdinal("UploadedImageID")))
                                    processedImage["id"] = imgID;
                                if (!reader.IsDBNull(reader.GetOrdinal("OrigDim1")))
                                    processedImage["origdim1"] = reader.GetDecimal(reader.GetOrdinal("OrigDim1"));
                                if (!reader.IsDBNull(reader.GetOrdinal("OrigDim2")))
                                    processedImage["origdim2"] = reader.GetDecimal(reader.GetOrdinal("OrigDim2"));
                                if (!reader.IsDBNull(reader.GetOrdinal("OrigXResolution")))
                                    processedImage["origresolution"] = reader.GetInt32(reader.GetOrdinal("OrigXResolution"));
                                if (!reader.IsDBNull(reader.GetOrdinal("OriginalFilename")))
                                    processedImage["originalfilename"] = reader.GetString(reader.GetOrdinal("OriginalFilename"));
                                if (!reader.IsDBNull(reader.GetOrdinal("FilenameAnnex")))
                                    processedImage["filenameannex"] = reader.GetString(reader.GetOrdinal("FilenameAnnex"));

                                processedImages.Add(new JProperty(imgID, processedImage.ToString()));
                            }
                        }
                    }
                }
            }
            return JsonConvert.SerializeObject(processedImages);
        }
    }
}