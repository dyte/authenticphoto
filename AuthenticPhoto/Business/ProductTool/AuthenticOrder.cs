﻿using AuthenticPhoto.Business.ProductTool.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace AuthenticPhoto.Business.ProductTool
{
    public class AuthenticOrder
    {
        private List<CheckoutProduct> selectedCheckoutProducts = new List<CheckoutProduct>();
        private List<UploadedImage> images = new List<UploadedImage>();
        private DateTime creationDate = DateTime.Now;

        public string OrderID { get; set; }
        public string PublicOrderID { get; set; }
                
        public List<UploadedImage> Images { get { return images; } set { images = value; } }
        public List<CheckoutProduct> SelectedCheckoutProducts { get { return selectedCheckoutProducts; } set { selectedCheckoutProducts = value; } }
        public string Delivery { get; set; }
        public DeliveryAddress DeliveryAddress { get; set; }

        public string Payment { get; set; }
        
        public string Reference { get; set; }
        public string DiscountCode { get; set; }

        public DateTime CreationDate { get { return creationDate; } set { creationDate = value; } }

        public bool Submitted { get; set; }

        public void GeneratePublicOrderID()
        {
            PublicOrderID = DateTime.Now.ToString("ddMMyyHmmss");
        }

        public decimal GetTotalPriceExclVatHandling()
        {
            decimal price = 0;
            foreach (UploadedImage image in Images)
            {
                if (image.SelectedProduct != null)
                {
                    price += image.GetTotalPrice() * image.Quantity;
                }
            }
            foreach (CheckoutProduct checkoutProduct in SelectedCheckoutProducts)
            {
                price += checkoutProduct.Qty * checkoutProduct.Price;
            }
            return price;
        }

        public string GenerateSummaryHtml()
        {
            decimal totalPriceExclVatHandling = GetTotalPriceExclVatHandling();
            bool transport = Delivery.Equals("transport");
            string html =
                "<table id='tblOrderFinalPrices' width='900' style='border: 1px solid rgb(196,0,14)'>" +
                "<tr><td style='width: 50%'>Totaal bestelling:</td><td style='width: 50%'>" +  Formatting.PrintAsEuro(totalPriceExclVatHandling) + "</td></tr>";

            string priceHandling = "", priceVAT, priceTotal;
            if (transport)
            {
                priceHandling = "Te bevestigen";
                priceVAT = "Te bevestigen";
                priceTotal = "Te bevestigen";
            }
            else
            {
                decimal vat = Math.Round(totalPriceExclVatHandling * (decimal)0.21, 2);
                priceVAT = Formatting.PrintAsEuro(vat);
                priceTotal = Formatting.PrintAsEuro(totalPriceExclVatHandling + vat);
            }

            if (!string.IsNullOrWhiteSpace(priceHandling))
            {
                html += "<tr><td style='width: 50%'>Verpakkings- en leveringskosten:</td><td style='width: 50%'>" + priceHandling + "</td></tr>";
            }
            html += "<tr><td style='width: 50%'>BTW:</td><td style='width: 50%'>" + priceVAT + "</td></tr>";
            html += "<tr><td style='width: 50%'>Totaal te betalen:</td><td style='width: 50%'>" + priceTotal + "</td></tr>";

            html += "</table>";
            return html;
        }
        
    }
}