﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Business.ProductTool
{

    public enum SignLocation
    {
        OnBackBehindPlexi, OnBack, InImage
    }
    public enum SignPosition
    {
        BelowLeft, BelowRight
    }
    public enum SignLabelSize
    {
        S105x42, S105x70, S200x143
    }
    public enum SignDelivery
    {
        SelfSignOnBackBehindPlexi,
        SelfSignOnBack,
        SelfSignInImage,
        UploadNow,
        SendLater,
    }

    public class Signature
    {
        public bool SignatureActive { get; set; }
        public SignLocation SignatureLocation { get; set; }
        public SignPosition SignaturePosition { get; set; }
        public SignLabelSize SignatureLabelSize { get; set; }
        public SignDelivery SignatureDelivery { get; set; }
        public bool SignatureNoWarrantyLabel { get; set; }
        public Signature() { }
        public Signature(string signJson)
        {
            if(string.IsNullOrWhiteSpace(signJson))
            {
                return;
            }
            JObject signObj = JObject.Parse(signJson);
            foreach (JProperty signProp in signObj.Properties())
            {
                switch (signProp.Name)
                {
                    case "SignatureActive": SignatureActive = signProp.Value.ToString().ToLower().Equals("true"); break;
                    case "SignatureLocation":
                        SignatureLocation = (SignLocation)Enum.Parse(typeof(SignLocation), signProp.Value.ToString()); break;
                    case "SignaturePosition":
                        SignaturePosition = (SignPosition)Enum.Parse(typeof(SignPosition), signProp.Value.ToString()); break;
                    case "SignatureLabelSize":
                        SignatureLabelSize = (SignLabelSize)Enum.Parse(typeof(SignLabelSize), signProp.Value.ToString()); break;
                    case "SignatureDelivery":
                        SignatureDelivery = (SignDelivery)Enum.Parse(typeof(SignDelivery), signProp.Value.ToString()); break;
                    case "SignatureNoWarrantyLabel":
                        SignatureNoWarrantyLabel = signProp.Value.ToString().ToLower().Equals("true"); break;
                }
            }
        }

        public string ToJson()
        {
            var signJson = new JObject();
            signJson["SignatureActive"] = SignatureActive;
            signJson["SignatureLocation"] = SignatureLocation.ToString();
            signJson["SignaturePosition"] = SignaturePosition.ToString();
            signJson["SignatureLabelSize"] = SignatureLabelSize.ToString();
            signJson["SignatureDelivery"] = SignatureDelivery.ToString();
            signJson["SignatureNoWarrantyLabel"] = SignatureNoWarrantyLabel;
            return signJson.ToString();
        }

        public string GetSignatureThumb(string orderID, string imageID)
        {
            return "/media/uploads/processed/" + orderID + "/0/thumb_signature_" + imageID + ".jpg";
        }

    }
}