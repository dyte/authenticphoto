﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Business.ProductTool
{
    public class TranslatedString
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public bool RenderInClient { get; set; }
    }
}