﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AuthenticPhoto.Business.ProductTool.Ingest
{
    [ModelBinder(typeof(ModelBinder))]
    public class FineUpload
    {
        public string Filename { get; set; }
        public Stream InputStream { get; set; }
        public string PartIndex { get; set; }
        public int TotalParts { get; set; }
        public string UuidName { get; set; }
        public string OrigFilename { get; set; }
        public string TotalFileSizeName { get; set; }
        public string OrderID { get; set; }

        public bool IsSignature { get; set; }
        public string AccompanyingImageID { get; set; }

        public void SaveAs(string destination, bool overwrite = false, bool autoCreateDirectory = true)
        {
            if (autoCreateDirectory)
            {
                var directory = new FileInfo(destination).Directory;
                if (directory != null) directory.Create();
            }

            using (var file = new FileStream(destination, overwrite ? FileMode.Create : FileMode.CreateNew))
                InputStream.CopyTo(file);
        }

        public class ModelBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                var request = controllerContext.RequestContext.HttpContext.Request;
                var formUpload = request.Files.Count > 0;

                // find filename
                var xFileName = request.Headers["X-File-Name"];
                var qqFile = request["qqfile"];
                var formFilename = formUpload ? request.Files[0].FileName : null;
                /*
                byte[] multiPartDataBytes = request.BinaryRead(request.TotalBytes);
                System.Text.Encoding myEncoding = System.Text.Encoding.GetEncoding("utf-8");
                string multiPartDataString = myEncoding.GetString(multiPartDataBytes, 0, multiPartDataBytes.Length);
                */

                string partIndex = request.Params["qqpartindex"];
                int totalParts = Convert.ToInt32(request.Params["qqtotalparts"]);
                string uuidName = request.Params["qquuid"];
                string qqfilename = request.Params["qqfilename"];
                string totalFileSizeName = request.Params["qqtotalfilesize"];
                string orderID = request.Params["orderid"];
                bool isSignature = request.Params["issignature"] == "1";
                string accompanyingImageID = request.Params["accompanyingimageiD"];

                var upload = new FineUpload
                {
                    PartIndex = partIndex,
                    TotalParts = totalParts,
                    UuidName = uuidName,
                    OrigFilename = qqfilename,
                    TotalFileSizeName = totalFileSizeName,
                    Filename = xFileName ?? qqFile ?? formFilename,
                    InputStream = formUpload ? request.Files[0].InputStream : request.InputStream,
                    OrderID = orderID,
                    IsSignature = isSignature,
                    AccompanyingImageID = accompanyingImageID
                };

                return upload;
            }
        }

    }
}