﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace AuthenticPhoto.Business.ProductTool.Entities
{
    public class ProductFamily
    {


        public int ProductFamilyID { get; set; }

        private int seq = Int32.MaxValue;

        private MultilingualString _name;
        public string NameXml { get { return Name.Value; } set { Name.Value = value; } }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString Name // added backing property to avoid null
        {
            get { return _name ?? (_name = new MultilingualString()); }
            set { _name = value; }
        }

        public int Seq { get { return seq; } set { seq = value; } }
        public virtual ICollection<Product> Products { get; set; }


        public bool fillEmptyNames(IEnumerable<string> langCodes)
        {
            string filledInVal = null;

            foreach (string code in langCodes)
            {
                if (!String.IsNullOrWhiteSpace(Name[code]))
                {
                    filledInVal = Name[code];
                }
            }

            if (String.IsNullOrWhiteSpace(filledInVal))
            {
                return false;
            }

            foreach (string code in langCodes)
            {
                if (String.IsNullOrWhiteSpace(Name[code]))
                {
                    Name[code] = filledInVal;
                }
            }

            return true;
        }
    }
}
