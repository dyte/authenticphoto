﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace AuthenticPhoto.Business.ProductTool.Entities
{
    public class Product
    {
        public int ProductID { get; set; }

        private int seq = Int32.MaxValue;
        private bool preselectOptions = false;
        private Dictionary<string, string> properties = new Dictionary<string, string>();

        private MultilingualString _name;
        public string NameXml { get { return Name.Value; } set { Name.Value = value; } }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString Name // added backing property to avoid null
        {
            get { return _name ?? (_name = new MultilingualString()); }
            set { _name = value; }
        }
        private MultilingualString _sectionName;
        public string SectionNameXml { get { return SectionName.Value; } set { SectionName.Value = value; } }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString SectionName // added backing property to avoid null
        {
            get { return _sectionName ?? (_sectionName = new MultilingualString()); }
            set { _sectionName = value; }
        }
        private MultilingualString _shortName;
        public string ShortNameXml { get { return ShortName.Value; } set { ShortName.Value = value; } }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString ShortName // added backing property to avoid null
        {
            get { return _shortName ?? (_shortName = new MultilingualString()); }
            set { _shortName = value; }
        }
        public Dictionary<string, string> Properties { get { return properties; } set { properties = value; } }

        public int Seq { get { return seq; } set { seq = value; } }
        public int ProductFamilyID { get; set; }
        public virtual ProductFamily ProductFamily { get; set; }

        public bool PreselectOptions { get { return preselectOptions; } set { preselectOptions = value; } }

        public virtual ICollection<OptionType> OptionTypes { get; set; }

        public string GetShortName(string langCode)
        {
            return string.IsNullOrWhiteSpace(ShortName[langCode]) ? Name[langCode] : ShortName[langCode]; // fallback on regular name
        }

        public string MaxSizeDim1And2 { get; set; } // temp parameter to make dapper operate
        public int? MaxSizeDim1 { get; set; } // within first optiontype (usually media)
        public int? MaxSizeDim2 { get; set; } // within first optiontype (usually media)
        


        public bool fillEmptyNames(IEnumerable<string> langCodes)
        {
            string filledInVal = null;

            foreach (string code in langCodes)
            {
                if (!String.IsNullOrWhiteSpace(Name[code]))
                {
                    filledInVal = Name[code];
                }
            }
            if (String.IsNullOrWhiteSpace(filledInVal))
            {
                return false;
            }
            foreach (string code in langCodes)
            {
                if (String.IsNullOrWhiteSpace(Name[code]))
                {
                    Name[code] = filledInVal;
                }
            }
            return true;
        }

    }
}
