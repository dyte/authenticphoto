﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthenticPhoto.Business.ProductTool.Entities

{
    public class OptionCategory
    {

        private ICollection<Option> options = new List<Option>();
        private int seq = Int32.MaxValue;

        public static string NEW_NAME = "New category";

        public int OptionCategoryID { get; set; }

        private MultilingualString _name;
        public string NameXml { get { return Name.Value; } set { Name.Value = value; } }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString Name // added backing property to avoid null
        {
            get { return _name ?? (_name = new MultilingualString()); }
            set { _name = value; }
        }

        public int Seq { get { return seq; } set { seq = value; } }
        public int OptionTypeID { get; set; }
        public virtual OptionType OptionType { get; set; }

        public string OldID { get; set; } // for xml import

        public virtual ICollection<Option> Options { get { return options; } set { options = value; } }

        public void fillEmptyNames(IEnumerable<string> langCodes)
        {
            string filledInVal = null;

            foreach (string code in langCodes)
            {
                if (!Name[code].Equals(NEW_NAME) && !String.IsNullOrWhiteSpace(Name[code]))
                {
                    filledInVal = Name[code];
                }
            }

            foreach (string code in langCodes)
            {
                if (Name[code].Equals(NEW_NAME) || String.IsNullOrWhiteSpace(Name[code]))
                {
                    Name[code] = String.IsNullOrWhiteSpace(filledInVal) ? NEW_NAME : filledInVal;
                }
            }
        }
    }
}