﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace AuthenticPhoto.Business.ProductTool.Entities
{
    public class Translation
    {
        public int TranslationID { get; set; }

        public string Key { get; set; }

        public int LanguageID { get; set; }
        public virtual Language Language { get; set; }

        public string Content { get; set; }

    }
}
