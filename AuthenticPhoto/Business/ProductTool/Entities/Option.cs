﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace AuthenticPhoto.Business.ProductTool.Entities

{
    public class Option : ICloneable
    {
        private ICollection<RollSize> rollSizes = new List<RollSize>();
        private ICollection<SheetSize> sheetSizes = new List<SheetSize>();

        private string illegalCombinations = "";
        private string checkoutProductsOnSelected = "";
        private string checkoutProductsOnNotSelected = "";
        private string illegalEmptyOptionTypesCombinations = "";
        private int seq = Int32.MaxValue;
        private string certificate = "";
        private string weightType = "srf"; // srf (surface), len (length), cir (circumference)
        private bool supportsMaxActive = false;
        private double supportsMaxKg = 0;
        private string lastEdit = DateTime.Now.ToString("dd-MM-yyyy HH:mm");
        private bool showOnMaxSizeExceeded = true;
        private bool showOnMaxSupportExceeded = true;
        private bool showOnIllegalCombo = true;

        private string comboTemp = ""; // only used in import function

        public int OptionID { get; set; }

        private MultilingualString _name;
        public string NameXml { get { return Name.Value; } set { Name.Value = value; } }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString Name // added backing property to avoid null
        {
            get { return _name ?? (_name = new MultilingualString()); }
            set { _name = value; }
        }

        private MultilingualString _shortName;
        public string ShortNameXml { get { return ShortName.Value; } set { ShortName.Value = value; } }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString ShortName // added backing property to avoid null
        {
            get { return _shortName ?? (_shortName = new MultilingualString()); }
            set { _shortName = value; }
        }

        private MultilingualString _maxSizeDesc;
        private MultilingualString _promoDesc;
        private MultilingualString _supportsMaxExceededDesc;
        public static string NEW_NAME = "New option";
        public string MaxSizeDescXml { get { return MaxSizeDesc.Value; } set { MaxSizeDesc.Value = value; } }
        public string PromoDescXml { get { return PromoDesc.Value; } set { PromoDesc.Value = value; } }
        public string SupportsMaxExceededDescXml { get { return SupportsMaxExceededDesc.Value; } set { SupportsMaxExceededDesc.Value = value; } }

        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString MaxSizeDesc // added backing property to avoid null
        {
            get { return _maxSizeDesc ?? (_maxSizeDesc = new MultilingualString()); }
            set { _maxSizeDesc = value; }
        }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString PromoDesc // added backing property to avoid null
        {
            get { return _promoDesc ?? (_promoDesc = new MultilingualString()); }
            set { _promoDesc = value; }
        }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString SupportsMaxExceededDesc // added backing property to avoid null
        {
            get { return _supportsMaxExceededDesc ?? (_supportsMaxExceededDesc = new MultilingualString()); }
            set { _supportsMaxExceededDesc = value; }
        }

        public string IllegalCombinations
        {
            get
            {
                return illegalCombinations;
            }
            set
            {
                illegalCombinations = value;
            }
        }
        public string IllegalEmptyOptionTypesCombinations
        {
            get
            {
                return illegalEmptyOptionTypesCombinations;
            }
            set
            {
                illegalEmptyOptionTypesCombinations = value;
            }
        }
        public string CheckoutProductsOnSelected
        {
            get
            {
                return checkoutProductsOnSelected;
            }
            set
            {
                checkoutProductsOnSelected = value;
            }
        }
        public string CheckoutProductsOnNotSelected
        {
            get
            {
                return checkoutProductsOnNotSelected;
            }
            set
            {
                checkoutProductsOnNotSelected = value;
            }
        }


        [NotMapped]
        public string ComboTemp { get { return comboTemp; } set { comboTemp = value; } }

        public int Seq { get { return seq; } set { seq = value; } }

        public double Kg { get; set; }
        public string WeightType { get { return weightType; } set { weightType = value; } }

        public bool SupportsMaxActive { get { return supportsMaxActive; } set { supportsMaxActive = value; } }
        public double SupportsMaxKg { get { return supportsMaxKg; } set { supportsMaxKg = value; } }

        public decimal TestprintA4 { get; set; }
        public decimal Testprint11 { get; set; }
        public string Certificate { get { return certificate; } set { certificate = value; } }

        public bool ShowOnMaxSizeExceeded { get { return showOnMaxSizeExceeded; } set { showOnMaxSizeExceeded = value; } }
        public bool ShowOnMaxSupportExceeded { get { return showOnMaxSupportExceeded; } set { showOnMaxSupportExceeded = value; } }
        public bool ShowOnIllegalCombo { get { return showOnIllegalCombo; } set { showOnIllegalCombo = value; } }

        public int? MaxSizeDim1 { get; set; }
        public int? MaxSizeDim2 { get; set; }

        public int OptionCategoryID { get; set; }
        public virtual OptionCategory OptionCategory { get; set; }
        public bool RollSizeActive { get; set; }

        public string LastEdit { get { return lastEdit; } set { lastEdit = value; } }
        public DateTime? PromoStart { get; set; }
        public DateTime? PromoEnd { get; set; }
        public int? PromoPercentage { get; set; }
        public decimal? PromoMinPrice { get; set; }


        public string PromoStartFormatted()
        {
            return FormatDate(PromoStart);
        }
        public string PromoEndFormatted()
        {
            return FormatDate(PromoEnd);
        }

        private string FormatDate(DateTime? datetime)
        {
            return datetime == null ? "" : ((DateTime)datetime).ToString("dd-MM-yyyy HH:mm");
        }

        public string GetShortName(string langCode)
        {
            return string.IsNullOrWhiteSpace(ShortName[langCode]) ? Name[langCode] : ShortName[langCode]; // fallback on regular name
        }

        public virtual List<RollSize> RollSizes { get; set; }
        public virtual List<SheetSize> SheetSizes { get; set; }

        private string rollSizesJson;
        private string sheetSizesJson;

        public string RollSizesJson { get { return rollSizesJson; } set { rollSizesJson = value; RollSizes = DecryptRollSizes(OptionID, rollSizesJson); } }
        public string SheetSizesJson { get { return sheetSizesJson; } set { sheetSizesJson = value; SheetSizes = DecryptSheetSizes(OptionID, sheetSizesJson); } }

        public object Clone()
        {
            Option clone = new Option();
            clone.Name = Name;
            clone.RollSizeActive = RollSizeActive;
            clone.MaxSizeDesc = MaxSizeDesc;
            clone.Kg = Kg;
            clone.TestprintA4 = TestprintA4;
            clone.Testprint11 = Testprint11;
            clone.Certificate = Certificate;
            clone.Seq = Seq;
            clone.MaxSizeDim1 = MaxSizeDim1;
            clone.MaxSizeDim2 = MaxSizeDim2;
            clone.WeightType = WeightType;
            clone.SupportsMaxActive = SupportsMaxActive;
            clone.SupportsMaxKg = SupportsMaxKg;
            clone.SupportsMaxExceededDesc = SupportsMaxExceededDesc;

            clone.RollSizes = RollSizes.Select(item => (RollSize)item.Clone()).ToList();
            clone.SheetSizes = SheetSizes.Select(item => (SheetSize)item.Clone()).ToList();

            clone.illegalCombinations = IllegalCombinations;
            clone.illegalEmptyOptionTypesCombinations = IllegalEmptyOptionTypesCombinations;

            return clone;
        }

        public void fillEmptyNames(IEnumerable<string> langCodes)
        {
            string filledInVal = null;

            foreach (string code in langCodes)
            {
                if (!Name[code].Equals(NEW_NAME) && !String.IsNullOrWhiteSpace(Name[code]))
                {
                    filledInVal = Name[code];
                }
            }

            foreach (string code in langCodes)
            {
                if (Name[code].Equals(NEW_NAME) || String.IsNullOrWhiteSpace(Name[code]))
                {
                    Name[code] = String.IsNullOrWhiteSpace(filledInVal) ? NEW_NAME : filledInVal;
                }
            }

        }

        public bool IsCombinableWith(int optionIdToCheck)
        {
            return illegalCombinations == null || !illegalCombinations.Split(',').Contains(Convert.ToString(optionIdToCheck));
        }

        public bool IsCombinableWithEmptyType(int optionTypeIdToCheck)
        {
            return illegalEmptyOptionTypesCombinations == null || !illegalEmptyOptionTypesCombinations.Split(',').Contains(Convert.ToString(optionTypeIdToCheck));
        }

        public bool HasCheckoutProductOnSelected(int coProductId)
        {
            return checkoutProductsOnSelected != null && checkoutProductsOnSelected.Split(',').Contains(Convert.ToString(coProductId));
        }
        public bool HasCheckoutProductOnNotSelected(int coProductId)
        {
            return checkoutProductsOnNotSelected != null && checkoutProductsOnNotSelected.Split(',').Contains(Convert.ToString(coProductId));
        }

        public void AddIllegalCombo(int id)
        {
            if (illegalCombinations == null)
            {
                illegalCombinations = id + ",";
            }
            if (!illegalCombinations.Split(',').Contains(Convert.ToString(id)))
            {
                illegalCombinations += id + ",";
            }
        }
        public void RemoveIllegalCombo(int id)
        {
            if (illegalCombinations != null && illegalCombinations.Split(',').Contains(Convert.ToString(id)))
            {
                illegalCombinations = illegalCombinations.Replace(id + ",", "");
            }
        }

        public bool IsIdenticalTo(Option candidate) // ignoring lastedit, ShowOn.. settings, OptionCategoryId, promo settings, Id (obviously..)
        {
            return
            XmlEquals(candidate.NameXml, NameXml) &&
            XmlEquals(candidate.ShortNameXml, ShortNameXml) &&
            XmlEquals(candidate.MaxSizeDescXml, MaxSizeDescXml) &&
            XmlEquals(candidate.PromoDescXml, PromoDescXml) &&
            XmlEquals(candidate.SupportsMaxExceededDescXml, SupportsMaxExceededDescXml) &&
            string.Join(",", candidate.RollSizes) == string.Join(",", RollSizes) &&
            candidate.RollSizeActive == RollSizeActive &&
            string.Join(",", candidate.SheetSizes) == string.Join(",", SheetSizes) &&
            candidate.IllegalCombinations == IllegalCombinations &&
            candidate.CheckoutProductsOnSelected == CheckoutProductsOnSelected &&
            candidate.CheckoutProductsOnNotSelected == CheckoutProductsOnNotSelected &&
            candidate.IllegalEmptyOptionTypesCombinations == IllegalEmptyOptionTypesCombinations &&
            candidate.Seq == seq &&
            candidate.Certificate == Certificate &&
            candidate.WeightType == WeightType &&
            candidate.Kg == Kg &&
            candidate.TestprintA4 == TestprintA4 &&
            candidate.Testprint11 == Testprint11 &&
            candidate.MaxSizeDim1 == MaxSizeDim1 &&
            candidate.MaxSizeDim2 == MaxSizeDim2 &&
            candidate.SupportsMaxActive == SupportsMaxActive &&
            candidate.SupportsMaxKg == SupportsMaxKg;

        }

        private bool XmlEquals(string xml1, string xml2)
        {
            if (xml1 == null || xml2 == null)
            {
                if (xml1 == null && xml2 == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return
                xml1.Replace(" ", "").Replace("<v></v>", "<v/>").Trim().Equals(
                    xml2.Replace(" ", "").Replace("<v></v>", "<v/>").Trim()
                );
        }

        public void AddIllegalEmptyTypeCombo(int optionTypeId)
        {
            if (illegalEmptyOptionTypesCombinations == null)
            {
                illegalEmptyOptionTypesCombinations = optionTypeId + ",";
            }
            if (!illegalEmptyOptionTypesCombinations.Split(',').Contains(Convert.ToString(optionTypeId)))
            {
                illegalEmptyOptionTypesCombinations += optionTypeId + ",";
            }
        }
        public void RemoveIllegalEmptyTypeCombo(int optionTypeId)
        {
            if (illegalEmptyOptionTypesCombinations != null && illegalEmptyOptionTypesCombinations.Split(',').Contains(Convert.ToString(optionTypeId)))
            {
                illegalEmptyOptionTypesCombinations = illegalEmptyOptionTypesCombinations.Replace(optionTypeId + ",", "");
            }
        }

        public void AddComboTemp(int id)
        {
            if (!comboTemp.Split(',').Contains(Convert.ToString(id)))
            {
                comboTemp += id + ",";
            }
        }

        public static string EncryptSheetSizes(List<SheetSize> sheetSizes)
        {
            return JsonConvert.SerializeObject(sheetSizes);
        }

        public static string EncryptRollSizes(List<RollSize> rollSizes)
        {
            return JsonConvert.SerializeObject(rollSizes);
        }

        public static List<SheetSize> DecryptSheetSizes(int optionID, string sheetSizesJson)
        {
            List<SheetSize> sheetSizes = new List<SheetSize>();
            if (!string.IsNullOrWhiteSpace(sheetSizesJson))
            {
                sheetSizes = JsonConvert.DeserializeObject<List<SheetSize>>(sheetSizesJson);
                foreach (SheetSize sheetSize in sheetSizes)
                {
                    sheetSize.OptionID = optionID;
                }
            }
            return sheetSizes;
        }
        public static List<RollSize> DecryptRollSizes(int optionID, string rollSizesJson)
        {
            List<RollSize> rollSizes = new List<RollSize>();
            if (!string.IsNullOrWhiteSpace(rollSizesJson))
            {
                rollSizes = JsonConvert.DeserializeObject<List<RollSize>>(rollSizesJson);
                foreach (RollSize rollSize in rollSizes)
                {
                    rollSize.OptionID = optionID;
                }
            }
            return rollSizes;
        }
        public double CalculateWeight(decimal width, decimal height) // input in mm
        {
            decimal size;
            switch (weightType)
            {
                case "len":
                    size = (width / 1000);
                    break;
                case "cir":
                    size = ((width / 1000) * 2) + ((height / 1000) * 2);
                    break;
                default: // "srf"
                    size = (width / 1000) * (height / 1000);
                    break;
            }
            return (double)size * Kg;
        }
    }
}
