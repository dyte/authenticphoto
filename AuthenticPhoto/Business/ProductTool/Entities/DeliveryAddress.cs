﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace AuthenticPhoto.Business.ProductTool.Entities
{
    public class DeliveryAddress
    {
        public int DeliveryAddressID { get; set; }
        public string FirstName { get; set; }
        public string FamilyName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string AddressIndication { get; set; }
        public string Place { get; set; }
        public string Country { get; set; }
        public string PhoneNumberCountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Company { get; set; }
        public Int64 AuthenticUserID { get; set; }

        public override string ToString()
        {
            return
                (string.IsNullOrWhiteSpace(FirstName) ? "" : FirstName) + " " +
                (string.IsNullOrWhiteSpace(FamilyName) ? "" : FamilyName) + " " +
                (string.IsNullOrWhiteSpace(Company) ? "" : " - " + Company) + " " +
                (string.IsNullOrWhiteSpace(Address) ? "" : Address) + " " +
                (string.IsNullOrWhiteSpace(PostalCode) ? "" : PostalCode) + " " +
                (string.IsNullOrWhiteSpace(Place) ? "" : Place) + " " +
                (string.IsNullOrWhiteSpace(Country) ? "" : Country);
        }

        public string GetPhoneNumber()
        {
            return PhoneNumberCountryCode + " " + PhoneNumber;
        }

        public string ToJson()
        {
            var result = new JObject();
            result["DeliveryAddressID"] = DeliveryAddressID;
            result["FirstName"] = FirstName;
            result["FamilyName"] = FamilyName;
            result["Email"] = Email;
            result["Address"] = Address;
            result["PostalCode"] = PostalCode;
            result["AddressIndication"] = AddressIndication;
            result["Place"] = Place;
            result["Country"] = Country;
            result["PhoneNumberCountryCode"] = PhoneNumberCountryCode;
            result["PhoneNumber"] = PhoneNumber;
            result["Company"] = Company;
            result["AuthenticUserID"] = AuthenticUserID;
            return result.ToString();
        }
        public string GenerateInfoTableHtml()
        {
            return "<table width='900' style='border: 1px dashed rgb(196,0,14)'>" +
                "<tr><td style='width: 25%'>Voornaam:</td><td style='width: 25%'>" + FirstName + "</td><td style='width: 25%'>Familienaam:</td><td style='width: 25%'>" + FamilyName + "</td></tr>" +
                "<tr><td>Bedrijf:</td><td>" + Company + "</td><td>Telefoonnummer:</td><td>" + GetPhoneNumber() + "</td></tr>" +
                "<tr><td>Adres:</td><td>" + Address + "</td><td>Postcode</td><td>" + PostalCode + "</td></tr>" +
                "<tr><td>Plaats:</td><td>" + Place + "</td><td>Land:</td><td>" + Country + "</td></tr>" +
                "<tr><td>Email:</td><td>" + Email + "</td><td></td><td></td></tr>" +
                "</table>";
        }
    }
}
