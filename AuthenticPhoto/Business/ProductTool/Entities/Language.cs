﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace AuthenticPhoto.Business.ProductTool.Entities
{
    public class Language
    {
        [HiddenInput(DisplayValue = false)]
        public int LanguageID { get; set; }

        private int seq = Int32.MaxValue;

        public string Name { get; set; }
        public int Seq { get { return seq; } set { seq = value; } }
        public string Code { get; set; }

        [NotMapped]
        public bool Unpublished { get; set; }
        [NotMapped]
        public bool IsNew { get; set; }


    }
}
