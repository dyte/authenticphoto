﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthenticPhoto.Business.ProductTool.Entities

{
    public class OptionType
    {

        private int seq = int.MaxValue;
        public string OptionTypeType { get; set; }
        public string Data1 { get; set; } // in case of OptionTypeType = "borders", this is a csv with the border widths
        private bool required = false;

        public int OptionTypeID { get; set; }

        private MultilingualString _name;
        public string NameXml { get { return Name.Value; } set { Name.Value = value; } }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString Name // added backing property to avoid null
        {
            get { return _name ?? (_name = new MultilingualString()); }
            set { _name = value; }
        }
        private MultilingualString _sectionName;
        public string SectionNameXml { get { return SectionName.Value; } set { SectionName.Value = value; } }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString SectionName // added backing property to avoid null
        {
            get { return _sectionName ?? (_sectionName = new MultilingualString()); }
            set { _sectionName = value; }
        }

        public int Seq { get { return seq; } set { seq = value; } }
        public int ProductID { get; set; }
        public virtual Product Product { get; set; }

        public bool Required { get { return required; } set { required = value; } }

        public List<Option> getAllOptions()
        {
            List<Option> allOptions = new List<Option>();
            foreach (OptionCategory category in OptionCategories)
            {
                foreach (Option option in category.Options)
                {
                    allOptions.Add(option);
                }
            }
            return allOptions;
        }

        public virtual ICollection<OptionCategory> OptionCategories { get; set; }

        public bool fillEmptyNames(IEnumerable<string> langCodes)
        {
            string filledInVal = null;

            foreach (string code in langCodes)
            {
                if (!String.IsNullOrWhiteSpace(Name[code]))
                {
                    filledInVal = Name[code];
                }
            }
            if (String.IsNullOrWhiteSpace(filledInVal))
            {
                return false;
            }
            foreach (string code in langCodes)
            {
                if (String.IsNullOrWhiteSpace(Name[code]))
                {
                    Name[code] = filledInVal;
                }
            }
            return true;
        }
    }
}
