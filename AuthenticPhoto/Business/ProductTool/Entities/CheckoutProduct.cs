﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AuthenticPhoto.Business.ProductTool.Entities
{
    public class CheckoutProduct
    {
        public int CheckoutProductID { get; set; }

        private int seq = Int32.MaxValue;

        [NotMapped]
        public bool Unpublished { get; set; }
        [NotMapped]
        public bool IsNew { get; set; }

        public int Seq { get { return seq; } set { seq = value; } }
        public decimal Price { get; set; }

        private MultilingualString _name;
        public string NameXml { get { return Name.Value; } set { Name.Value = value; } }
        [NotMapped] // don't map MultilingualStrings. EF will eat your guts.
        public virtual MultilingualString Name // added backing property to avoid null
        {
            get { return _name ?? (_name = new MultilingualString()); }
            set { _name = value; }
        }

        [NotMapped] // used in checkout flow
        public int Qty { get; set; }

        public bool fillEmptyNames(IEnumerable<string> langCodes)
        {
            string filledInVal = null;

            foreach (string code in langCodes)
            {
                if (!string.IsNullOrWhiteSpace(Name[code]))
                {
                    filledInVal = Name[code];
                }
            }

            if (string.IsNullOrWhiteSpace(filledInVal))
            {
                return false;
            }

            foreach (string code in langCodes)
            {
                if (string.IsNullOrWhiteSpace(Name[code]))
                {
                    Name[code] = filledInVal;
                }
            }

            return true;
        }

    }
}
