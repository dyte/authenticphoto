﻿using Newtonsoft.Json;
using System;

namespace AuthenticPhoto.Business.ProductTool.Entities
{
    public class RollSize : Size, ICloneable
    {
        private int maxWidth = 0;
        private int minLength = 0;
        private int maxLength = 0;
        private decimal price = 0;
        private int seq = int.MaxValue;

        [JsonProperty(PropertyName = "id")]
        public int RollSizeID { get; set; }
        [JsonProperty(PropertyName = "maw")]
        public int MaxWidth { get { return maxWidth; } set { maxWidth = value; } }
        [JsonProperty(PropertyName = "mil")]
        public int MinLength { get { return minLength; } set { minLength = value; } }
        [JsonProperty(PropertyName = "mal")]
        public int MaxLength { get { return maxLength; } set { maxLength = value; } }
        [JsonProperty(PropertyName = "p")]
        public decimal Price { get { return price; } set { price = value; } }
        [JsonProperty(PropertyName = "sq")]
        public int Seq { get { return seq; } set { seq = value; } }
        [JsonIgnore]
        public int OptionID { get; set; }
        
        public object Clone()
        {
            RollSize clone = new RollSize();
            clone.MaxWidth = MaxWidth;
            clone.MinLength = MinLength;
            clone.MaxLength = MaxLength;
            clone.Price = Price;
            clone.Seq = Seq;
            return clone;
        }

        public override string ToString() // used to compare in IsIdenticalTo (Option.cs)
        {
            return "" + maxWidth + minLength + maxLength + price + seq;
        }
    }
}
