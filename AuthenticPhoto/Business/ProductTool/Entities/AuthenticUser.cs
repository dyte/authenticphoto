﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthenticPhoto.Business.ProductTool.Entities
{
    public class AuthenticUser
    {
        public long Id { get; set; }
        public string FileMakerId { get; set; }
        public string Firstname { get; set; }
        public string Familyname { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public bool? Female { get; set; }
        public string LanguageCode { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string AddressIndication { get; set; }
        public string Place { get; set; }
        public string Country { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? FilemakerTimestamp { get; set; }
        public string StatusEmail { get; set; }
        public string OriginalData { get; set; }
        public string FilemakerContactKey { get; set; }
        public string CompanyKey { get; set; }
        public string PhoneNumberCountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Company { get; set; }
        public string VatCountryCode { get; set; }
        public string Vat { get; set; }
        public string AcceptedTermsVersion { get; set; }
        public string PastOrders { get; set; }
        public string Password { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public bool IsLocked { get; set; }
        public bool EmailConfirmed { get; set; }
        public string SecurityStamp { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public bool IsRegistered { get; set; }
        public bool ContactPermission { get; set; }
        public string DiscountsOnProducts { get; set; }
        public bool VatValid { get; set; }

    }
}
