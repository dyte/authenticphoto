﻿using Newtonsoft.Json;
using System;

namespace AuthenticPhoto.Business.ProductTool.Entities
{
    public class SheetSize : Size, ICloneable
    {
        private int side1 = 0;
        private int side2 = 0;
        private decimal price = 0;
        private int seq = int.MaxValue;

        [JsonProperty(PropertyName = "id")]
        public int SheetSizeID { get; set; }
        [JsonProperty(PropertyName = "s1")]
        public int Side1 { get { return side1; } set { side1 = value; } }
        [JsonProperty(PropertyName = "s2")]
        public int Side2 { get { return side2; } set { side2 = value; } }
        [JsonProperty(PropertyName = "p")]
        public decimal Price { get { return price; } set { price = value; } }
        [JsonProperty(PropertyName = "sq")]
        public int Seq { get { return seq; } set { seq = value; } }
        [JsonIgnore]
        public int OptionID { get; set; }

        public object Clone()
        {
            SheetSize clone = new SheetSize();
            clone.Side1 = Side1;
            clone.Side2 = Side2;
            clone.Price = Price;
            clone.Seq = Seq;
            return clone;
        }
    }
}
