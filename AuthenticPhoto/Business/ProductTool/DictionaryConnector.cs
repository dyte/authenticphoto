﻿using AuthenticPhoto.Business.ProductTool.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;

namespace AuthenticPhoto.Business.ProductTool
{

    public class DictionaryConnector
    {
        private HttpContext Context { get; set; }

        public DictionaryConnector(HttpContext context)
        {
            this.Context = context;
        }

        public const string UmbracoCultureNL = "nl";
        public const string UmbracoCultureFR = "fr";
        public const string UmbracoCultureEN = "en-US";

        public Dictionary<string, TranslatedString> GetDictionaryFor(string section)
        {
            switch (section)
            {
                case "ShowOrderForm":
                    return GetTranslations(new string[,] {
                    {"General.Yes", "0" },
                    {"General.Cancel", "0" },
                    {"ShowOrderForm.UploadImages", "0" },
                    {"ShowOrderForm.AddImages", "1" },
                    {"ShowOrderForm.ProcessingDropped", "0" },
                    {"ShowOrderForm.Retry", "0" },
                    {"ShowOrderForm.AcceptedFiles", "1" },
                    {"ShowOrderForm.ConnectingExisting", "1" },
                    {"ShowOrderForm.AddingImages", "1" },
                    {"General.Loading", "1" },
                    {"ShowOrderForm.InitializingSession", "1" },
                    {"ShowOrderForm.Intro.StartOrder", "0" },
                    });
                case "Finishing":
                    return GetTranslations(new string[,] {
                    {"ShowPreferences.Tabs.ImageData", "0" },
                    {"ShowPreferences.Tabs.Finishing", "0" },
                    {"ShowPreferences.Tabs.Signature", "0" },
                    {"ShowPreferences.Tabs.Price", "0" },
                    {"ShowPreferences.Tabs.Signature.Header", "0" },
                    {"ShowPreferences.Tabs.Signature.Location", "0" },
                    {"ShowPreferences.Tabs.Signature.Position", "0" },
                    {"ShowPreferences.Tabs.Signature.LabelSize", "0" },
                    {"ShowPreferences.Tabs.Signature.Delivery", "0" },
                    {"ShowPreferences.Tabs.Signature.SignatureFile", "0" },
                    {"ShowPreferences.Tabs.Signature.NoCertificate", "0" },
                    {"PriceDetails.ExclVAT.Explanation", "0" },
                    {"ShowPreferences.Tabs.Signature.OnBackBehindPlexi", "0" },
                    {"ShowPreferences.Tabs.Signature.OnBack", "0" },
                    {"ShowPreferences.Tabs.Signature.InImage", "0" },
                    {"ShowPreferences.Tabs.Signature.BelowLeft", "0" },
                    {"ShowPreferences.Tabs.Signature.BelowRight", "0" },
                    {"ShowPreferences.Tabs.Signature.S105x42", "0" },
                    {"ShowPreferences.Tabs.Signature.S105x70", "0" },
                    {"ShowPreferences.Tabs.Signature.S200x143", "0" },
                    {"ShowPreferences.Tabs.Signature.SelfSignOnBackBehindPlexi", "0" },
                    {"ShowPreferences.Tabs.Signature.SelfSignOnBack", "0" },
                    {"ShowPreferences.Tabs.Signature.SelfSignInImage", "0" },
                    {"ShowPreferences.Tabs.Signature.UploadNow", "0" },
                    {"ShowPreferences.Tabs.Signature.SendLater", "0" },
                    {"ShowPreferences.Tabs.ImageData.SoftproofPerEmail", "0"},
                    {"ShowPreferences.Tabs.Price.Header", "0" },
                    {"ShowPreferences.Tabs.Price.CompletePreferencesWarning", "0" },
                    {"ShowPreferences.Tabs.ImageData.Qty", "0" },
                    {"ShowPreferences.Tabs.ImageData.Dims", "0" },
                    {"ShowPreferences.Tabs.ImageData.Dims.AsFile", "0" },
                    {"ShowPreferences.Tabs.ImageData.Dims.Adapted", "0" },
                    {"ShowPreferences.Tabs.ImageData.Width", "0" },
                    {"ShowPreferences.Tabs.ImageData.Height", "0" },
                    {"ShowPreferences.Tabs.ImageData.Resolution", "0" },
                    {"ShowPreferences.Tabs.ImageData.Testprint", "0" },
                    {"ShowPreferences.Tabs.ImageData.Testprint.Explanation", "1" },
                    {"ShowPreferences.Tabs.ImageData.Softproof", "0" },
                    {"ShowPreferences.Tabs.ImageData.Softproof.Explanation", "1" },
                    {"ShowPreferences.Tabs.ImageData.SoftproofDetail", "0" },
                    {"ShowPreferences.Tabs.ImageData.Inspect", "0" },
                    {"ShowPreferences.Tabs.ImageData.DontCut", "0" },
                    {"ShowPreferences.Tabs.ImageData.LabMessage", "0" },
                    {"ShowPreferences.Tabs.ImageData.Product", "0" },
                    {"ShowPreferences.Tabs.Signature.SelectFile", "0" },
                    {"ShowPreferences.OptionUncombinableWith", "1" },
                    {"ShowPreferences.ChooseOptionFirst", "1" },
                    {"ShowPreferences.OptionTooLight", "1" },
                    {"ShowPreferences.CompletePreferencesForImages", "1" },
                    {"ShowPreferences.NoDimsAdaptDuringUpload", "1" },
                    {"ShowPreferences.InSufficientResolution", "1" },
                    {"ShowPreferences.ResolutionTooHigh", "1" },
                    {"ShowOrderForm.ProcessingDropped", "0" },
                    {"ShowOrderForm.Retry", "0" },
                    {"General.Close", "0" },
                    {"ShowPreferences.ChooseProduct", "0" },
                    {"ShowPreferences.ChooseOptionPrefix", "1" },
                    {"ShowPreferences.NoOptionPrefix", "1" },
                    {"Calculator.Title", "0" },
                    {"Calculator.CompletePreferences", "0" },
                    {"Calculator.EnterValidDim", "0" },
                    {"ShowPrice.Weight", "0" },
                    {"ShowOrderForm.LoadingProduct", "1" },
                    {"ShowOrderForm.LoadingProducts", "1" },
                    {"ShowPreferences.ProductMaximumSize", "1" }
                    });
                case "OrderStep2":
                    return GetTranslations(new string[,] {
                    {"Preferences.FinishOrder", "0" },
                    {"ShowOrderForm.AddImages", "0" },
                    });
                case "PriceDetails":
                    return GetTranslations(new string[,] {
                    {"PriceDetails.PriceDetail", "0" },
                    {"ShowPreferences.Tabs.ImageData.Product", "0" },
                    {"ShowPreferences.Tabs.ImageData.TestprintA4", "0" },
                    {"ShowPreferences.Tabs.ImageData.Testprint11", "0" },
                    {"ShowPreferences.Tabs.ImageData.Softproof", "0" },
                    {"ShowPreferences.Tabs.ImageData.Inspect", "0" },
                    {"ShowPreferences.Tabs.ImageData.DontCut", "0" },
                    {"PriceDetails.Extras", "0" },
                    {"PriceDetails.Signature", "0" },
                    {"PriceDetails.Identification", "0" },
                    {"PriceDetails.Total", "0" },
                    {"ShowPreferences.Tabs.ImageData.LabMessage", "0" },
                    {"ShowPreferences.Tabs.ImageData.Dims", "0" },
                    {"ShowPreferences.Tabs.Signature.OnBackBehindPlexi", "0" },
                    {"ShowPreferences.Tabs.Signature.OnBack", "0" },
                    {"ShowPreferences.Tabs.Signature.InImage", "0" },
                    {"ShowPreferences.Tabs.Signature.BelowLeft", "0" },
                    {"ShowPreferences.Tabs.Signature.BelowRight", "0" },
                    {"ShowPreferences.Tabs.Signature.S105x42", "0" },
                    {"ShowPreferences.Tabs.Signature.S105x70", "0" },
                    {"ShowPreferences.Tabs.Signature.S200x143", "0" },
                    {"ShowPreferences.Tabs.Signature.SelfSignOnBackBehindPlexi", "0" },
                    {"ShowPreferences.Tabs.Signature.SelfSignOnBack", "0" },
                    {"ShowPreferences.Tabs.Signature.SelfSignInImage", "0" },
                    {"ShowPreferences.Tabs.Signature.UploadNow", "0" },
                    {"ShowPreferences.Tabs.Signature.SendLater", "0" },
                    {"ShowPreferences.Tabs.Signature.NoCertificate", "0" },
                    {"ShowPreferences.Tabs.ImageData.Dims.PrintDims", "0" }                        
                    });
                case "TotalPriceDetails":
                    return GetTranslations(new string[,]
                    {
                        { "PriceDetails.TotalOrder", "0" }                        
                    });
                case "TotalPriceDetailsRow":
                    return GetTranslations(new string[,]
                    {
                        {"ShowPreferences.Tabs.ImageData.Qty", "0" },
                        {"ShowPreferences.Tabs.ImageData.Product", "0" },
                        {"ShowPreferences.Tabs.ImageData.Testprint", "0" },
                        {"ShowPreferences.Tabs.ImageData.TestprintA4", "0" },
                        {"ShowPreferences.Tabs.ImageData.Testprint11", "0" },
                        {"ShowPreferences.Tabs.ImageData.Softproof", "0" },
                        {"ShowPreferences.Tabs.ImageData.Inspect", "0" },
                        {"ShowPreferences.Tabs.ImageData.DontCut", "0" },
                        {"PriceDetails.Extras", "0" },
                        {"PriceDetails.Signature", "0" },
                        {"PriceDetails.Identification", "0" },
                        {"PriceDetails.TotalPerImage", "0" },
                        {"PriceDetails.Total", "0" },
                        {"ShowPreferences.Tabs.ImageData.LabMessage", "0" },
                        {"ShowPreferences.Tabs.Signature.OnBackBehindPlexi", "0" },
                        {"ShowPreferences.Tabs.Signature.OnBack", "0" },
                        {"ShowPreferences.Tabs.Signature.InImage", "0" },
                        {"ShowPreferences.Tabs.Signature.BelowLeft", "0" },
                        {"ShowPreferences.Tabs.Signature.BelowRight", "0" },
                        {"ShowPreferences.Tabs.Signature.S105x42", "0" },
                        {"ShowPreferences.Tabs.Signature.S105x70", "0" },
                        {"ShowPreferences.Tabs.Signature.S200x143", "0" },
                        {"ShowPreferences.Tabs.Signature.SelfSignOnBackBehindPlexi", "0" },
                        {"ShowPreferences.Tabs.Signature.SelfSignOnBack", "0" },
                        {"ShowPreferences.Tabs.Signature.SelfSignInImage", "0" },
                        {"ShowPreferences.Tabs.Signature.UploadNow", "0" },
                        {"ShowPreferences.Tabs.Signature.SendLater", "0" },
                        {"ShowPreferences.Tabs.Signature.NoCertificate", "0" },
                        {"ShowPreferences.Tabs.ImageData.Dims", "0"},
                        {"ShowPrice.Weight", "0" },
                        {"ShowPreferences.Tabs.ImageData.Dims.PrintDims", "0" }
                    });
                case "TotalPriceDetailsCheckoutProductRow":
                    return GetTranslations(new string[,]
                    {
                        { "ShowPreferences.Tabs.ImageData.Qty", "0" }
                    });
                case "Checkout":
                    return GetTranslations(new string[,]
                        {
                            { "Checkout.BackToPreferences", "0" },
                            { "Checkout.MyBillingInfo", "0" },
                            { "Checkout.ModifyInfo", "0" },
                            { "Checkout.CompleteInfo", "0" },
                            { "General.Next", "0" },
                            { "Checkout.Overview", "0" },
                            { "Checkout.DeliveryAddress", "0" },
                            { "Checkout.DeliveryAddressSame", "0" },
                            { "Checkout.EnterNewAddress", "0" },
                            { "UserData.Firstname", "0" },
                            { "UserData.Lastname", "0" },
                            { "UserData.Company", "0" },
                            { "UserData.Postalcode", "0" },
                            { "UserData.Country", "0" },
                            { "UserData.Email", "0" },
                            { "UserData.Email.Explanation", "0" },
                            { "UserData.Address", "0" },
                            { "UserData.Place", "0" },
                            { "UserData.Telephone", "0" },
                            { "UserData.Mobile", "0" },
                            { "UserData.Mobile.Explanation", "0" },
                            { "Checkout.Payment", "0" },
                            { "Checkout.Payment.Online", "0" },
                            { "Checkout.Payment.Transfer", "0" },
                            { "Checkout.Payment.Cash", "0" },
                            { "Checkout.ChoosePayment", "0" },
                            { "Checkout.Reference", "0" },
                            { "Checkout.Reference.Explanation", "0" },
                            { "Checkout.DiscountCode", "0" },
                            { "Checkout.YourOrder", "0" },
                            { "Checkout.RecommendedExtras", "0" },
                            { "ShowPreferences.Tabs.ImageData.Qty", "0" },
                            { "Checkout.Recap", "0" },
                            { "Checkout.PlaceOrder", "0" },
                            {"Checkout.VatExplanation", "1" },
                            {"Checkout.OrderInProcess", "1" },
                            {"Checkout.ClickToRestartOrder", "1" },
                            {"Checkout.ClickToHomepage", "1" },
                            {"Checkout.Thanks", "1" }
                        });
                case "OrderSummary":
                    return GetTranslations(new string[,]
                        {
                            { "OrderSummary.TotalOrder", "0" },
                            { "OrderSummary.HandlingCharges", "0" },
                            { "OrderSummary.ToConfirm", "0" },
                            { "OrderSummary.VAT", "0" }
                        });
                case "ShowPrice":
                    return GetTranslations(new string[,]
                        {
                                { "ShowPrice.PriceImage", "0" },
                                { "PriceDetails.ExclVAT", "0" },
                                { "PriceDetails.Detail", "0" },
                                { "ShowPrice.TotalPrice", "0" },
                                { "ShowPrice.Weight", "0" },
                                { "ShowPrice.TotalWeight", "0" }
                        });
                case "ThumbsList":
                    return GetTranslations(new string[,]
                        {
                                { "ShowPreferences.CompletePreferencesForImage", "0" },
                                { "ShowPreferences.PreferencesComplete", "0" }
                        });

                case "Account":
                    return GetTranslations(new string[,]
                            {
                                { "UserData.Title", "0" },
                                { "UserData.Title.Choose", "0" },
                                { "UserData.Firstname", "0" },
                                { "UserData.Lastname", "0" },
                                { "UserData.Company", "0" },
                                { "UserData.Postalcode", "0" },
                                { "UserData.Country", "0" },
                                { "UserData.Email", "0" },
                                { "UserData.Email.Explanation", "0" },
                                { "UserData.Address", "0" },
                                { "UserData.Place", "0" },
                                { "UserData.Telephone", "0" },
                                { "UserData.Mobile", "0" },
                                { "UserData.Mobile.Explanation", "0" },
                                { "UserData.Country.BE", "0" },
                                { "UserData.Country.FR", "0" },
                                { "UserData.Country.NL", "0" },
                                { "UserData.Country.LU", "0" },
                                { "UserData.Country.DE", "0" },
                                { "UserData.Country.GB", "0" },
                                { "UserData.Country.US", "0" },
                                { "UserData.RequiredFields", "0" },
                                { "UserData.Birthday", "0" },
                                { "UserData.Birthday.Explanation", "0" },
                                { "UserData.VatExplanation", "0" },
                                { "UserData.VatNumber", "0" },
                                { "Account.RegisterNow", "0" },
                                { "Account.RegisterNow.YourData", "0" },
                                { "Account.LoginExisting", "0" },
                                {"Account.LoginSuccesful", "0" },
                                {"Account.NotActivated","0" },
                                { "Account.InvalidLogin", "0" },
                                { "Account.InformationUpdated", "0" },
                                { "Account.ConfirmYourAccount", "0" },
                                { "Account.Registration.Success", "0" },
                                { "Account.Registration.Error", "0" },
                                {"Account.RegisterNow2", "0" },
                                {"Account.ForgotPassword", "0" },
                                {"Account.ForgotPassword.EnterEmail", "0" },
                                {"Account.ForgotPassword.SendLink", "0" },
                                {"Account.ForgotPassword.EnterNewPassword", "0" },
                                {"Account.AcceptTerms1", "0" },
                                {"Account.AcceptTerms2", "0" },
                                {"Account.InscribeToNewsletter", "0" },
                                {"Account.ResendCode", "0" },
                                {"UserData.Password", "0" },
                                {"UserData.RepeatPassword", "0" },
                                {"UserData.Validation.ReqEmail", "0" },
                                {"UserData.Validation.ReqPassword", "0" },
                                {"UserData.Validation.ReqPasswordConfirmation", "0" },
                                {"UserData.Validation.PasswordLength", "0" },
                                {"UserData.Validation.PasswordConfirmationMatch", "0" }
                            });
                case "Thankyou":
                    return GetTranslations(new string[,]
                            {
                                { "Thankyou.Title", "0" },
                                { "Thankyou.Subtitle", "0" },
                                { "Thankyou.MailsCaption", "0" },
                                { "Thankyou.MailsCsv", "0" }
                            });
                case "MailFooter":
                    return GetTranslations(new string[,]
                            {
                                { "Email.Footer.SentBy", "0" },
                                { "Company.FullName", "0" },
                                { "Company.ProductsOverview", "0" },
                                { "Company.StreetNr", "0" },
                                { "Company.CityPostalCode", "0" },
                                { "Company.Country", "0" },
                                { "Company.TelephoneCommercial", "0" }
                            });
            }
            return null;
        }

        public string GetUmbracoCulture()
        {
            object cult = Context.Session["umbracoculture"];
            return cult == null ? "nl" : (string)cult;
        }

        public string GetTranslation(string key)
        {
            object transObj = Context.Session[GetUmbracoCulture() + "." + key];
            if (transObj == null)
            {
                string result = GetDictionaryValue(key, GetUmbracoCulture());
                result = result.Replace("\\n", "<br/>");
                Context.Session[GetUmbracoCulture() + "." + key] = result;
                return result;
            }
            else return (string)transObj;
        }

        public static string GetDictionaryValue(string key, string iso)
        {
            var dictionaryItem = UmbracoContext.Current.Application.Services.LocalizationService.GetDictionaryItemByKey(key);
            if (dictionaryItem != null)
            {
                var translation = dictionaryItem.Translations.SingleOrDefault(x => x.Language.IsoCode.Equals(iso));
                if (translation != null)
                    return translation.Value;
            }
            return key;
        }

        private Dictionary<string, TranslatedString> GetTranslations(string[,] keyShouldRenderInClientPairs)
        {
            Dictionary<string, TranslatedString> dictionary = new Dictionary<string, TranslatedString>();
            for (int i = 0; i < keyShouldRenderInClientPairs.GetLength(0); i++)
            {
                var result = GetTranslation(keyShouldRenderInClientPairs[i, 0]);
                TranslatedString translatedString = result != null ?
                    new TranslatedString { Key = keyShouldRenderInClientPairs[i, 0], Value = result, RenderInClient = keyShouldRenderInClientPairs[i, 1] == "1" } :
                    new TranslatedString { Key = keyShouldRenderInClientPairs[i, 0], Value = keyShouldRenderInClientPairs[i, 0], RenderInClient = keyShouldRenderInClientPairs[i, 1] == "1" };
                dictionary.Add(keyShouldRenderInClientPairs[i, 0], translatedString);
            }
            return dictionary;
        }

    }
}