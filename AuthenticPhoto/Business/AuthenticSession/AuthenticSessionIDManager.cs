﻿using System;
using System.Linq;

namespace AuthenticPhoto.Business.AuthenticOrder
{
    public static class AuthenticOrderIDManager
    {
        public static string GenerateOrderID() {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();
            return new string(
                Enumerable.Repeat(chars, 10)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
        }
    }
}