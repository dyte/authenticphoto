﻿using System;
using System.Collections;
using System.IO;

using log4net.ObjectRenderer;

namespace AuthenticPhoto.Business.Logging
{
    public class Log4NetObjectLogger : IObjectRenderer
    {
        public void RenderObject(RendererMap rendererMap, object obj, TextWriter writer)
        {
            var ex = obj as Exception;

            //if its not an exception, dump it. If it's an exception, log extended details.
            if (ex == null)
            {
                //by default log up to 10 levels deep.
                ObjectDumper.Write(obj, 10, writer);
            }
            else
            {
                while (ex != null)
                {
                    RenderException(ex, writer);
                    ex = ex.InnerException;
                }
            }
        }

        private static void RenderException(Exception ex, TextWriter writer)
        {
            writer.WriteLine("Type: {0}", ex.GetType().FullName);
            writer.WriteLine("Message: {0}", ex.Message);
            writer.WriteLine("Source: {0}", ex.Source);
            writer.WriteLine("TargetSite: {0}", ex.TargetSite);
            RenderExceptionData(ex, writer);
            writer.WriteLine("StackTrace: {0}", ex.StackTrace);
        }

        private static void RenderExceptionData(Exception ex, TextWriter writer)
        {
            foreach (DictionaryEntry entry in ex.Data)
            {
                writer.WriteLine("{0}: {1}", entry.Key, entry.Value);
            }
        }
    }
}