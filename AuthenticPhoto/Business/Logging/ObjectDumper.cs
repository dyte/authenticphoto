﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;

namespace AuthenticPhoto.Business.Logging
{
    public class ObjectDumper
    {
        public static void Write(object element)
        {
            Write(element, 0);
        }

        public static void Write(object element, int depth)
        {
            Write(element, depth, Console.Out);
        }

        public static void Write(object element, int depth, TextWriter log)
        {
            var dumper = new ObjectDumper(depth) { writer = log };

            dumper.WriteObject(null, element);
        }

        TextWriter writer;

        int pos;
        int level;

        readonly int depth;

        private ObjectDumper(int depth)
        {
            this.depth = depth;
        }

        private void Write(string s)
        {
            if (s == null)
            {
                return;
            }

            this.writer.Write(s);
            this.pos += s.Length;
        }

        private void WriteIndent()
        {
            for (int i = 0; i < level; i++) writer.Write("  ");
        }

        private void WriteLine()
        {
            writer.WriteLine();
            pos = 0;
        }

        private void WriteTab()
        {
            Write("  ");
            while (pos % 8 != 0) Write(" ");
        }

        private void WriteObject(string prefix, object element)
        {
            if (element == null || element is ValueType || element is string)
            {
                WriteIndent();
                Write(prefix);
                WriteValue(element);
                WriteLine();
            }
            else
            {
                var enumerableElement = element as IEnumerable;

                if (enumerableElement != null)
                {
                    foreach (var item in enumerableElement)
                    {
                        if (item is IEnumerable && !(item is string))
                        {
                            WriteIndent();
                            Write(prefix);
                            Write("...");
                            WriteLine();

                            if (this.level >= this.depth)
                            {
                                continue;
                            }

                            this.level++;
                            this.WriteObject(prefix, item);
                            this.level--;
                        }
                        else
                        {
                            WriteObject(prefix, item);
                        }
                    }
                }
                else
                {
                    var members = element.GetType().GetMembers(BindingFlags.Public | BindingFlags.Instance);
                    
                    WriteIndent();
                    Write(prefix);
                    
                    var propWritten = false;
                    
                    foreach (var m in members)
                    {
                        var f = m as FieldInfo;
                        var p = m as PropertyInfo;

                        if (f == null && p == null)
                        {
                            continue;
                        }

                        if (propWritten)
                        {
                            this.WriteTab();
                        }
                        else
                        {
                            propWritten = true;
                        }
                        
                        this.Write(m.Name);
                        this.Write("=");
                        
                        var t = f != null ? f.FieldType : p.PropertyType;
                        
                        if (t.IsValueType || t == typeof(string))
                        {
                            this.WriteValue(f != null ? f.GetValue(element) : p.GetValue(element, null));
                        }
                        else
                        {
                            this.Write(typeof(IEnumerable).IsAssignableFrom(t) ? "..." : "{ }");
                        }
                    }

                    if (propWritten) WriteLine();

                    if (this.level >= this.depth)
                    {
                        return;
                    }
                    
                    foreach (var m in members)
                    {
                        var f = m as FieldInfo;
                        var p = m as PropertyInfo;

                        if (f == null && p == null)
                        {
                            continue;
                        }

                        var t = f != null ? f.FieldType : p.PropertyType;

                        if (t.IsValueType || t == typeof(string))
                        {
                            continue;
                        }
                        
                        var value = f != null ? f.GetValue(element) : p.GetValue(element, null);

                        if (value == null)
                        {
                            continue;
                        }

                        this.level++;
                        this.WriteObject(m.Name + ": ", value);
                        this.level--;
                    }
                }
            }
        }

        private void WriteValue(object o)
        {
            if (o == null)
            {
                Write("null");
            }
            else if (o is DateTime)
            {
                Write(((DateTime)o).ToShortDateString());
            }
            else if (o is ValueType || o is string)
            {
                Write(o.ToString());
            }
            else if (o is IEnumerable)
            {
                Write("...");
            }
            else
            {
                Write("{ }");
            }
        }
    }
}
