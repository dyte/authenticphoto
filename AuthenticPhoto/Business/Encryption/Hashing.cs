﻿using System.Security.Cryptography;
using System.Text;

namespace AuthenticPhoto.Business.Encryption
{
    public static class Hashing
    {
        public enum HashType
        {
            Sha1,
            Md5
        }

        public static string GetHash(string text, HashType hashType)
        {
            var hash = string.Empty;

            switch (hashType)
            {
                case HashType.Sha1:
                    hash = GetSha1(text);
                    break;
                case HashType.Md5:
                    hash = GetMd5(text);
                    break;
            }

            return hash;
        }

        public static bool IsValidHash(string original, string hashString, HashType hashType)
        {
            return (GetHash(original, hashType) == hashString);
        }

        private static string GetSha1(string text)
        {
            return X2(new SHA1Managed().ComputeHash(Encoding.ASCII.GetBytes(text)));
        }

        private static string GetMd5(string text)
        {
            return X2(MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(text)));
        }

        private static string X2(byte[] input)
        {
            var output = new StringBuilder();

            for (var i = 0; i < input.Length; i++)
            {
                output.Append(input[i].ToString("X2"));
            }

            return output.ToString();
        }
    }
}
