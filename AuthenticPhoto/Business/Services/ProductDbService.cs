﻿using System;
using System.Collections.Generic;
using System.Linq;

using AuthenticPhoto.Business.Modules.Synchronization;
using AuthenticPhoto.Data.Synchronization;
using AuthenticPhoto.Data.Synchronization.Results;

using Umbraco.Core;
using Umbraco.Core.Models;

namespace AuthenticPhoto.Business.Services
{
    public class ProductDbService
    {
        public ProductFamilySynchronizationResult AddProductFamily(ProductFamily productFamily)
        {
            var contentService = ApplicationContext.Current.Services.ContentService;
            var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
            var domainService = ApplicationContext.Current.Services.DomainService;

            var cultureCode = productFamily.CultureCode;
            var domainRootNode = domainService.GetAll(true).FirstOrDefault(x => x.LanguageIsoCode.InvariantEquals(cultureCode));

            if (domainRootNode?.RootContentId == null)
            {
                return new ProductFamilySynchronizationResult
                {
                    DocumentId = -1,
                    ProductFamily = productFamily,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Invalid culture code {cultureCode}" }
                };
            }

            var productFamilyExists =
                    contentService.GetContentOfContentType(contentTypeService.GetContentType("ProductFamily").Id)
                        .Count(
                            x =>
                            x.GetValue<string>("backofficeIdentifier").InvariantEquals(productFamily.ProductDbIdentifier)
                            && x.Path.Split(new [] {","}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).Contains(domainRootNode.RootContentId.Value)) > 0;

            if (productFamilyExists)
            {
                return new ProductFamilySynchronizationResult
                {
                    DocumentId = -1,
                    ProductFamily = productFamily,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Product family  {productFamily.ProductDbIdentifier} exists already" }
                };
            }

            var newProductFamily = contentService.CreateContent(
                productFamily.Name,
                domainRootNode.RootContentId.Value,
                contentTypeService.GetContentType("ProductFamily").Alias);

            newProductFamily.SetValue("backofficeIdentifier", productFamily.ProductDbIdentifier);

            contentService.Save(newProductFamily, raiseEvents:false);

            return new ProductFamilySynchronizationResult
                       {
                           DocumentId = newProductFamily.Id,
                           ProductFamily = productFamily,
                           ResultCode = ProductDbSynchronizationResultCodes.OK,
                           Success = true
                       };
        }

        public ProductFamilySynchronizationResult UpdateProductFamily(ProductFamily productFamily)
        {
            var contentService = ApplicationContext.Current.Services.ContentService;
            var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
            var domainService = ApplicationContext.Current.Services.DomainService;

            var cultureCode = productFamily.CultureCode;
            var domainRootNode = domainService.GetAll(true).FirstOrDefault(x => x.LanguageIsoCode.InvariantEquals(cultureCode));

            if (domainRootNode?.RootContentId == null)
            {
                return new ProductFamilySynchronizationResult
                {
                    DocumentId = -1,
                    ProductFamily = productFamily,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Invalid culture code {cultureCode}" }
                };
            }

            var productFamilyContent =
                contentService.GetContentOfContentType(contentTypeService.GetContentType("ProductFamily").Id)
                    .FirstOrDefault(x => x.GetValue<string>("backofficeIdentifier").InvariantEquals(productFamily.ProductDbIdentifier)
                        && x.Path.Split(new []{","}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).Contains(domainRootNode.RootContentId.Value));

            if (productFamilyContent == null)
            {
                return new ProductFamilySynchronizationResult
                {
                    DocumentId = -1,
                    ProductFamily = productFamily,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Product family  {productFamily.ProductDbIdentifier} does not exist" }
                };
            }

            productFamilyContent.Name = productFamily.Name;

            contentService.Save(productFamilyContent);

            if (!contentService.HasPublishedVersion(productFamilyContent.Id))
            {
                return new ProductFamilySynchronizationResult
                           {
                               DocumentId = productFamilyContent.Id,
                               ProductFamily = productFamily,
                               ResultCode = ProductDbSynchronizationResultCodes.OK,
                               Success = true
                           };
            }

            var publishStatusResult = contentService.PublishWithStatus(productFamilyContent);

            return new ProductFamilySynchronizationResult
                       {
                           DocumentId = productFamilyContent.Id,
                           ProductFamily = productFamily,
                           Success = false,
                           ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                           Feedback =
                               publishStatusResult.Success
                                   ? new List<string>()
                                   : new List<string>
                                         {
                                             $"Error publishing product family {productFamily.ProductDbIdentifier}"
                                         }
                       };
        }

        public ProductFamilySynchronizationResult DeleteProductFamily(ProductFamily productFamily, bool permanent)
        {
            var contentService = ApplicationContext.Current.Services.ContentService;
            var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
            var domainService = ApplicationContext.Current.Services.DomainService;

            var cultureCode = productFamily.CultureCode;
            var domainRootNode = domainService.GetAll(true).FirstOrDefault(x => x.LanguageIsoCode.InvariantEquals(cultureCode));

            if (domainRootNode?.RootContentId == null)
            {
                return new ProductFamilySynchronizationResult
                {
                    DocumentId = -1,
                    ProductFamily = productFamily,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Invalid culture code {cultureCode}" }
                };
            }

            var productFamilyContent =
                contentService.GetContentOfContentType(contentTypeService.GetContentType("ProductFamily").Id)
                    .FirstOrDefault(x => x.GetValue<string>("backofficeIdentifier").InvariantEquals(productFamily.ProductDbIdentifier)
                        && x.Path.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).Contains(domainRootNode.RootContentId.Value));

            if (productFamilyContent == null)
            {
                return new ProductFamilySynchronizationResult
                {
                    DocumentId = -1,
                    ProductFamily = productFamily,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Product family  {productFamily.ProductDbIdentifier} does not exist" }
                };
            }

            if (permanent)
            {
                contentService.Delete(productFamilyContent);
            }
            else
            {
                contentService.MoveToRecycleBin(productFamilyContent);
            }

            return new ProductFamilySynchronizationResult
            {
                DocumentId = productFamilyContent.Id,
                ProductFamily = productFamily,
                ResultCode = ProductDbSynchronizationResultCodes.OK,
                Success = true
            };
        }

        public ProductSynchronizationResult AddProduct(Product product)
        {
            var contentService = ApplicationContext.Current.Services.ContentService;
            var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
            var domainService = ApplicationContext.Current.Services.DomainService;

            var cultureCode = product.CultureCode;
            var domainRootNode = domainService.GetAll(true).FirstOrDefault(x => x.LanguageIsoCode.InvariantEquals(cultureCode));

            if (domainRootNode?.RootContentId == null)
            {
                return new ProductSynchronizationResult
                {
                    DocumentId = -1,
                    Product = product,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Invalid culture code {cultureCode}" }
                };
            }

            var productFamily =
                    contentService.GetContentOfContentType(contentTypeService.GetContentType("ProductFamily").Id)
                        .FirstOrDefault(
                            x =>
                            x.GetValue<string>("backofficeIdentifier").InvariantEquals(product.ProductFamily.ProductDbIdentifier)
                            && x.Path.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).Contains(domainRootNode.RootContentId.Value));

            if (productFamily == null)
            {
                return new ProductSynchronizationResult
                           {
                               DocumentId = -1,
                               Product = product,
                               Success = false,
                               ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                               Feedback =
                                   new List<string>
                                       {
                                           $"Product family {product.ProductFamily.ProductDbIdentifier} does not exist"
                                       }
                           };
            }

            var newProduct = contentService.CreateContent(
                product.Name,
                productFamily.Id,
                contentTypeService.GetContentType("Product").Alias);

            newProduct.SetValue("backofficeIdentifier", product.ProductDbIdentifier);

            contentService.Save(newProduct, raiseEvents: false);

            return new ProductSynchronizationResult
            {
                DocumentId = newProduct.Id,
                Product = product,
                ResultCode = ProductDbSynchronizationResultCodes.OK,
                Success = true
            };
        }

        public ProductSynchronizationResult UpdateProduct(Product product)
        {
            var contentService = ApplicationContext.Current.Services.ContentService;
            var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
            var domainService = ApplicationContext.Current.Services.DomainService;

            var cultureCode = product.CultureCode;
            var domainRootNode = domainService.GetAll(true).FirstOrDefault(x => x.LanguageIsoCode.InvariantEquals(cultureCode));

            if (domainRootNode?.RootContentId == null)
            {
                return new ProductSynchronizationResult
                {
                    DocumentId = -1,
                    Product = product,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Invalid culture code {cultureCode}" }
                };
            }

            var productFamilyContent = contentService.GetContentOfContentType(contentTypeService.GetContentType("ProductFamily").Id)
                .FirstOrDefault(x => x.GetValue<string>("backofficeIdentifier").InvariantEquals(product.ProductFamily.ProductDbIdentifier) 
                    && x.Path.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).Contains(domainRootNode.RootContentId.Value));

            if (productFamilyContent == null)
            {
                return new ProductSynchronizationResult
                {
                    DocumentId = -1,
                    Product = product,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Product family {product.ProductFamily.ProductDbIdentifier} does not exist" }
                };
            }

            var productContent = productFamilyContent
                .Children()
                .Where(x => x.ContentType.Alias.InvariantEquals("Product"))
                .FirstOrDefault(x => x.GetValue<string>("backofficeIdentifier").InvariantEquals(product.ProductDbIdentifier));

            if (productContent == null)
            {
                return new ProductSynchronizationResult
                {
                    DocumentId = -1,
                    Product = product,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Product {product.ProductDbIdentifier} does not exist" }
                };
            }

            productContent.Name = product.Name;

            contentService.Save(productContent);

            if (!contentService.HasPublishedVersion(productContent.Id))
            {
                return new ProductSynchronizationResult
                {
                    DocumentId = productFamilyContent.Id,
                    Product = product,
                    ResultCode = ProductDbSynchronizationResultCodes.OK,
                    Success = true
                };
            }

            var publishStatusResult = contentService.PublishWithStatus(productContent);

            return new ProductSynchronizationResult
            {
                DocumentId = productFamilyContent.Id,
                Product = product,
                Success = false,
                ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                Feedback = publishStatusResult.Success 
                    ? new List<string>() 
                    : new List<string> { $"Error publishing product {product.ProductDbIdentifier}" }
            };
        }

        public ProductSynchronizationResult DeleteProduct(Product product, bool permanent)
        {
            var contentService = ApplicationContext.Current.Services.ContentService;
            var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
            var domainService = ApplicationContext.Current.Services.DomainService;

            var cultureCode = product.CultureCode;
            var domainRootNode = domainService.GetAll(true).FirstOrDefault(x => x.LanguageIsoCode.InvariantEquals(cultureCode));

            if (domainRootNode?.RootContentId == null)
            {
                return new ProductSynchronizationResult
                {
                    DocumentId = -1,
                    Product = product,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Invalid culture code {cultureCode}" }
                };
            }

            var productFamilyContent =
                contentService.GetContentOfContentType(contentTypeService.GetContentType("ProductFamily").Id)
                    .FirstOrDefault(x => x.GetValue<string>("backofficeIdentifier").InvariantEquals(product.ProductFamily.ProductDbIdentifier)
                        && x.Path.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).Contains(domainRootNode.RootContentId.Value));

            if (productFamilyContent == null)
            {
                return new ProductSynchronizationResult
                {
                    DocumentId = -1,
                    Product = product,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Product family  {product.ProductFamily.ProductDbIdentifier} does not exist" }
                };
            }

            var productContent = productFamilyContent
                .Children()
                .Where(x => x.ContentType.Alias.InvariantEquals("Product"))
                .FirstOrDefault(x => x.GetValue<string>("backofficeIdentifier").InvariantEquals(product.ProductDbIdentifier));

            if (productContent == null)
            {
                return new ProductSynchronizationResult
                {
                    DocumentId = -1,
                    Product = product,
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Product {product.ProductDbIdentifier} does not exist" }
                };
            }

            if (permanent)
            {
                contentService.Delete(productContent);
            }
            else
            {
                contentService.MoveToRecycleBin(productContent);
            }

            return new ProductSynchronizationResult
            {
                DocumentId = productFamilyContent.Id,
                Product = product,
                ResultCode = ProductDbSynchronizationResultCodes.OK,
                Success = true
            };
        }
    }
}