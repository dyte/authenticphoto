﻿using System.Collections.Generic;
using System.Linq;
using AuthenticPhoto.Business.Modules.Mail;
using AuthenticPhoto.Data.Synchronization;
using AuthenticPhoto.Data.Synchronization.Results;

using Our.Umbraco.Vorto.Extensions;

using Umbraco.Core;
using Umbraco.Web;

namespace AuthenticPhoto.Business.Services
{
    public class MailService
    {
        public GetMailResult GetMail(string mailAlias)
        {
            var umbHelper = new UmbracoHelper(UmbracoContext.Current);
            var mail = umbHelper.TypedContentAtRoot()
                .First(x => x.DocumentTypeAlias.InvariantEquals("MailFolder"))
                .Children
                .FirstOrDefault(x => x.DocumentTypeAlias.InvariantEquals("Mail") && x.GetPropertyValue<string>("mailAlias").InvariantEquals(mailAlias));
            
            var languages = ApplicationContext.Current.Services.LocalizationService.GetAllLanguages()
                    .Select(x => new InstalledCulture { CultureCode = x.IsoCode })
                    .ToList();

            var template = ApplicationContext.Current.Services.FileService.GetTemplate(mail.TemplateId).Content;

            var mailDefinitions = languages.Select(x => new MailDefinition { CultureCode = x.CultureCode, View = template, Html = mail.GetVortoValue<string>("mailHtml", x.CultureCode) }).ToList();

            return mail != null 
                ? new GetMailResult { MailDefinitions = mailDefinitions } 
                : new GetMailResult { Success = false, ResultCode = MailResultCodes.ERROR, Feedback = new List<string> { "Mail alias not found" }, MailDefinitions = new List<MailDefinition>() };
        }

        public GetMailResult GetMail(string mailAlias, string language)
        {
            var umbHelper = new UmbracoHelper(UmbracoContext.Current);
            var mail = umbHelper.TypedContentAtRoot()
                .First(x => x.DocumentTypeAlias.InvariantEquals("MailFolder"))
                .Children
                .FirstOrDefault(x => x.DocumentTypeAlias.InvariantEquals("Mail") && x.GetPropertyValue<string>("mailAlias").InvariantEquals(mailAlias));

            var template = ApplicationContext.Current.Services.FileService.GetTemplate(mail.TemplateId).Content;

            return mail != null 
                ? new GetMailResult
                {
                    Success = true,
                    ResultCode = MailResultCodes.OK,
                    Feedback = new List<string>(),
                    MailDefinitions = new List<MailDefinition>
                    {
                        new MailDefinition
                        {
                            CultureCode = language,
                            View = template,
                            Html = mail.GetVortoValue<string>("mailHtml", language)
                        }
                    }
                }   
                : new GetMailResult
                {
                    Success = false,
                    ResultCode = MailResultCodes.ERROR,
                    Feedback = new List<string> { "Mail alias not found" },
                    MailDefinitions = new List<MailDefinition>()
                };
        }
    }
}