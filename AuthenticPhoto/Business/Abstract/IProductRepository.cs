﻿using AuthenticPhoto.Business.ProductTool.Entities;
using System.Collections.Generic;

namespace AuthenticPhoto.Business.Abstract
{
    public interface IProductRepository
    {
        IEnumerable<AuthenticUser> AuthenticUsers { get; }
        IEnumerable<Language> Languages { get; }
        IEnumerable<CheckoutProduct> CheckoutProducts { get; }
        IEnumerable<Translation> Translations { get; }
        IEnumerable<ProductFamily> ProductFamilies { get; }
        IEnumerable<Product> Products { get; }
        IEnumerable<OptionType> OptionTypes { get; }
        IEnumerable<OptionCategory> OptionCategories { get; }
        IEnumerable<Option> Options { get; }
        IEnumerable<DeliveryAddress> DeliveryAddresses { get; }

        CheckoutProduct GetCheckoutProduct(int i);
        Product GetProduct(int i);
        Option GetOption(int i);

        DeliveryAddress GetDeliveryAddress(int i);

        List<DeliveryAddress> GetDeliveryAddresses(int userID);

        void SaveDeliveryAddress(DeliveryAddress deliveryAddress);
        DeliveryAddress DeleteDeliveryAddress(int deliveryAddressId);
        List<ProductFamily> GetProductFamiliesWithChildListing();
        Product GetProductWithPopulatedChildren(int productID);

        List<RollSize> GetRollSizesForOption(int optionID);
        List<SheetSize> GetSheetSizesForOption(int optionID);
        OptionType GetOptionTypeWithPopulatedChildrenInclSizes(int optionTypeID);
    }
}
