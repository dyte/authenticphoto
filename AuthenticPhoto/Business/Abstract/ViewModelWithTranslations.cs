﻿using AuthenticPhoto.Business.ProductTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Business.Abstract
{
    public class ViewModelWithTranslations
    {
        public Dictionary<string, TranslatedString> Dictionary;
        public string GetTranslation(string key)
        {
            object trans;
            try
            {
                trans = Dictionary[key];
            }
            catch (KeyNotFoundException)
            {
                return key;
            }
            return trans == null ? key : ((TranslatedString)trans).Value;
        }

        public TranslatedString[] GetTranslationsToRender()
        {
            return Dictionary.Values.Where(ts => ts.RenderInClient == true).ToArray();
        }
    }
}