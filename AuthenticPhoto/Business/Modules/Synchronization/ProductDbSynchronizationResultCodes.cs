﻿namespace AuthenticPhoto.Business.Modules.Synchronization
{
    public static class ProductDbSynchronizationResultCodes
    {
        // ReSharper disable InconsistentNaming
        public static string OK = "200";
        public static string ERROR = "500";
        //public static string NON_TRUSTED_PARTY = "500";
        //public static string EXISTS_ALREADY = "501";
        //public static string DOES_NOT_EXIST = "502";
        //public static string INVALID_CULTURE_CODE = "503";
        //public static string PUBLISH_ERROR = "504";

        // ReSharper restore InconsistentNaming
    }
}