﻿using System;
using System.Web;
using System.Web.Services.Protocols;
using System.Xml;

using AuthenticPhoto.Business.Authentication;

namespace AuthenticPhoto.Business.Modules.Synchronization
{
    internal sealed class ProductDbSynchronizationModule : IHttpModule
    {
        private AuthenticationEventHandler eventHandler;

        public event AuthenticationEventHandler Authenticate
        {
            add { this.eventHandler += value; }
            remove
            {
                if (value != null)
                {
                    // ReSharper disable once DelegateSubtraction
                    this.eventHandler -= value;
                }
            }
        }

        public void Init(HttpApplication application)
        {
            application.AuthenticateRequest += this.OnEnter;
        }

        private void OnAuthenticate(SynchronizationAuthenticationEventArgs e)
        {
            if (this.eventHandler == null)
            {
                return;
            }

            this.eventHandler(this, e);
        }

        public string ModuleName
        {
            get { return "ProductDbSynchronizationModule"; }
        }

        void OnEnter(object source, EventArgs eventArgs)
        {
            var application = (HttpApplication)source;
            var context = application.Context;
            var httpStream = context.Request.InputStream;

            // Save the current position of stream.
            var position = httpStream.Position;

            // If the request contains an HTTP_SOAPACTION header, look at this message.
            if (context.Request.ServerVariables["HTTP_SOAPACTION"] == null)
            {
                return;
            }

            // Load the body of the HTTP message into an XML document.
            var xmlDocument = new XmlDocument();

            string soapPassphrase = string.Empty;

            try
            {
                xmlDocument.Load(httpStream);

                // Reset the stream position.
                httpStream.Position = position;

                // Bind to the Authentication header.
                var xmlNode = xmlDocument.GetElementsByTagName("Passphrase").Item(0);

                if (xmlNode != null)
                {
                    soapPassphrase = xmlNode.InnerText;
                }
            }
            catch (Exception e)
            {
                // Reset the position of stream.
                httpStream.Position = position;

                // Throw a SOAP exception.
                var name = new XmlQualifiedName("Load");
                var soapException = new SoapException("Unable to read SOAP request", name, e);

                throw soapException;
            }

            // Raise the custom global.asax event.
            this.OnAuthenticate(new SynchronizationAuthenticationEventArgs(context, soapPassphrase));
        }

        public void Dispose() { }
    }
}
