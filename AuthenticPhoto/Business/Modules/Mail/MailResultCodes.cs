﻿namespace AuthenticPhoto.Business.Modules.Mail
{
    public class MailResultCodes
    {
        // ReSharper disable InconsistentNaming
        public static string OK = "200";
        public static string ERROR = "500";
        // ReSharper restore InconsistentNaming
    }
}