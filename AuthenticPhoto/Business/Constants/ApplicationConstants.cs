﻿using System.Collections.Specialized;

using umbraco.presentation.webservices;

namespace AuthenticPhoto.Business.Constants
{
    public static class ApplicationConstants
    {
        private static readonly NameValueCollection ApplicationSettings;

        static ApplicationConstants()
        {
            ApplicationSettings = System.Configuration.ConfigurationManager.AppSettings;
        }

        public static string SynchronizationSalt { get { return ApplicationSettings["sync:salt"]; } }
    }
}