﻿using System.Web.Services.Protocols;

namespace AuthenticPhoto.Business.Authentication
{
    public class SynchronizationAuthenticationSoapHeader : SoapHeader
    {
        public string Passphrase { get; set; }
    }
}
