﻿namespace AuthenticPhoto.Business.Authentication
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;
    using System.Net.Mail;
    using System.IO;

    public class EmailService : IIdentityMessageService
    {
        #region methods

        public void Send(IdentityMessage message, string[] attachedImagePaths)
        {
            // Credentials:
            var credentialUserName = "upload@authenticbrussels.onmicrosoft.com";
            MailAddress sentFrom = new MailAddress("upload@authenticbrussels.onmicrosoft.com", "Authentic Visual Communication");
            var pwd = "Onedrive@1234";

            // Configure the client:
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("smtp.office365.com");

            client.Port = 587;
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.TargetName = "STARTTLS/smtp.office365.com";

            // Create the credentials:
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(credentialUserName, pwd);

            client.EnableSsl = true;
            client.Credentials = credentials;

            // Create the message:
            var mail = new System.Net.Mail.MailMessage(sentFrom, new MailAddress(message.Destination));
            mail.Subject = message.Subject;
            mail.Body = message.Body;
            mail.IsBodyHtml = true;

            AlternateView view = AlternateView.CreateAlternateViewFromString(mail.Body, null, "text/html");
            foreach (string attachedImagePath in attachedImagePaths)
            {
                LinkedResource linkedImg = new LinkedResource(attachedImagePath);
                linkedImg.ContentId = Path.GetFileNameWithoutExtension(attachedImagePath);
                view.LinkedResources.Add(linkedImg);
            }
            mail.AlternateViews.Add(view);

            // Send:
            client.Send(mail);
        }
        
        public Task SendAsync(IdentityMessage message)
        {
            Send(message, new string[0]);
            return Task.FromResult(0);
        }

        public Task SendAsync(IdentityMessage message, string[] attachedImagePaths)
        {
            Send(message, attachedImagePaths);
            return Task.FromResult(0);
        }

        #endregion
    }
}