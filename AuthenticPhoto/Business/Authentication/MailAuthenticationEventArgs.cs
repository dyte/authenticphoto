﻿using System;
using System.Security.Principal;
using System.Web;

namespace AuthenticPhoto.Business.Authentication
{
    public delegate void MailAuthenticationEventHandler(Object sender, MailAuthenticationEventArgs e);

    public class MailAuthenticationEventArgs : EventArgs
    {
        public MailAuthenticationEventArgs(HttpContext context)
        {
            this.Context = context;
        }

        public MailAuthenticationEventArgs(HttpContext context, string passphrase)
        {
            this.Context = context;
            this.Passphrase = passphrase;
        }

        public HttpContext Context { get; }

        public IPrincipal Principal { get; set; }

        public void Authenticate()
        {
            var i = new GenericIdentity(this.User);
            this.Principal = new GenericPrincipal(i, new String[0]);
        }

        public void Authenticate(string[] roles)
        {
            var i = new GenericIdentity(this.User);
            this.Principal = new GenericPrincipal(i, roles);
        }

        public string User { get; set; }

        public string Passphrase { get; set; }

        public bool HasCredentials => (this.User != null) && (this.Passphrase != null);
    }
}