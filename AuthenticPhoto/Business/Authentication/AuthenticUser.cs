﻿namespace AuthenticPhoto.Business.Authentication
{
    using System.Security.Claims;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Newtonsoft.Json.Linq;
    using ProductTool.Entities;

    public class AuthenticUser : IdentityUser<long, AuthenticLogin, AuthenticUserRole, AuthenticClaim>
    {
        private List<DeliveryAddress> deliveryAddresses = new List<DeliveryAddress>();
        private readonly List<CountryCode> _countryVatCodesEU = new List<CountryCode>(new CountryCode[] {
            new CountryCode { Code = "", Name= "" },
            new CountryCode { Code = "AT", Name= "AT" },
            new CountryCode { Code = "BE", Name= "BE" },
            new CountryCode { Code = "BG", Name= "BG" },
            new CountryCode { Code = "CY", Name= "CY" },
            new CountryCode { Code = "CZ", Name= "CZ" },
            new CountryCode { Code = "DE", Name= "DE" },
            new CountryCode { Code = "DK", Name= "DK" },
            new CountryCode { Code = "EE", Name= "EE" },
            new CountryCode { Code = "EL", Name= "EL" },
            new CountryCode { Code = "ES", Name= "ES" },
            new CountryCode { Code = "FI", Name= "FI" },
            new CountryCode { Code = "FR", Name= "FR" },
            new CountryCode { Code = "GB", Name= "GB" },
            new CountryCode { Code = "HR", Name= "HR" },
            new CountryCode { Code = "HU", Name= "HU" },
            new CountryCode { Code = "IE", Name= "IE" },
            new CountryCode { Code = "IT", Name= "IT" },
            new CountryCode { Code = "LT", Name= "LT" },
            new CountryCode { Code = "LU", Name= "LU" },
            new CountryCode { Code = "LV", Name= "LV" },
            new CountryCode { Code = "MT", Name= "MT" },
            new CountryCode { Code = "NL", Name= "NL" },
            new CountryCode { Code = "PL", Name= "PL" },
            new CountryCode { Code = "PT", Name= "PT" },
            new CountryCode { Code = "RO", Name= "RO" },
            new CountryCode { Code = "SE", Name= "SE" },
            new CountryCode { Code = "SI", Name= "SI" },
            new CountryCode { Code = "SK", Name= "SK" }
        });

        private string languageCode = "nl";
        private bool? female = false;
        private bool contactPermission = false;
        private bool isRegistered = false;
        private bool vatValid = false;

        private string password = "******"; // dummy password to pass validation
        private string confirmpassword = "******";


        [NotMapped]
        public IEnumerable<SelectListItem> CountryVatCodesEU
        {
            get { return new SelectList(_countryVatCodesEU, "Code", "Name"); }
        }

        #region properties
        [Required(ErrorMessageResourceType = typeof(Resources.ErrorMessages), ErrorMessageResourceName = "EmailRequired")]
        [EmailAddress]
        public override string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ErrorMessages), ErrorMessageResourceName = "PasswordRequired")]
        [NotMapped]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.ErrorMessages), ErrorMessageResourceName = "PasswordLength", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get { return password; } set { password = value; } }

        [DataType(DataType.Password)]
        [NotMapped]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources.ErrorMessages), ErrorMessageResourceName = "PasswordConfirmationMatch")]
        public string ConfirmPassword { get { return confirmpassword; } set { confirmpassword = value; } }

        public string FilemakerID { get; set; }
        public DateTime? FilemakerTimestamp { get; set; }
        public string StatusEmail { get; set; }
        public string OriginalData { get; set; }
        public string FilemakerContactKey { get; set; }
        public string CompanyKey { get; set; }

        public string DiscountsOnProducts { get; set; }

        [NotMapped]
        public string Title { get; set; }

        public bool? Female { get { return female; } set { female = value; } }

        public bool ContactPermission { get { return contactPermission; } set { contactPermission = value; } }

        public bool VatValid { get { return vatValid; } set { vatValid = value; } }

        public bool IsRegistered { get { return isRegistered; } set { isRegistered = value; } }

        public string FirstName { get; set; }
        public string FamilyName { get; set; }

        [Required]
        public string LanguageCode { get { return languageCode; } set { languageCode = value; } }

        public string Address { get; set; }

        public string AddressIndication { get; set; }

        public string PostalCode { get; set; }

        public string Place { get; set; }

        public string Country { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? Birthday { get; set; }

        public override string PhoneNumber { get; set; }
        public string PhoneNumberCountryCode { get; set; }
        public string MobileNumber { get; set; }
        public string MobileNumberCountryCode { get; set; }
        
        public string Company { get; set; }

        public string VatCountryCode { get; set; }
        
        public string Vat { get; set; }

        public string AcceptedTermsVersion { get; set; }

        public string PastOrders { get; set; }

        [NotMapped]
        public bool NewUser { get; set; }

        public string Source { get; set; }


        public List<DeliveryAddress> DeliveryAddresses { get { return deliveryAddresses; } set { deliveryAddresses = value; } }

        #endregion

        #region methods

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(AuthenticUserManager userManager)
        {
            var userIdentity = await userManager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public void UpdateWithNewData(AuthenticUser UpdatedUser)
        {
            LanguageCode = UpdatedUser.LanguageCode;
            Female = UpdatedUser.Female;
            Title = UpdatedUser.Title;
            FirstName = UpdatedUser.FirstName;
            FamilyName = UpdatedUser.FamilyName;
            Address = UpdatedUser.Address;
            PostalCode = UpdatedUser.PostalCode;
            Place = UpdatedUser.Place;
            Country = UpdatedUser.Country;
            Birthday = UpdatedUser.Birthday;
            Company = UpdatedUser.Company;
            PhoneNumber = UpdatedUser.PhoneNumber;
            PhoneNumberCountryCode = UpdatedUser.PhoneNumberCountryCode;
            MobileNumber = UpdatedUser.MobileNumber;
            MobileNumberCountryCode = UpdatedUser.MobileNumberCountryCode;
            Vat = UpdatedUser.Vat;
            VatCountryCode = UpdatedUser.VatCountryCode;
        }

        public string GetAddressJson()
        {
            var result = new JObject();
            result["FirstName"] = FirstName;
            result["FamilyName"] = FamilyName;
            result["Email"] = Email;
            result["Address"] = Address;
            result["PostalCode"] = PostalCode;
            result["AddressIndication"] = AddressIndication;
            result["Place"] = Place;
            result["Country"] = Country;
            result["PhoneNumberCountryCode"] = PhoneNumberCountryCode;
            result["PhoneNumber"] = PhoneNumber;
            result["Company"] = Company;
            return result.ToString();
        }

        public string GetPhoneNumber()
        {
            return PhoneNumberCountryCode + " " + PhoneNumber;
        }
        public string GetVat()
        {
            return string.IsNullOrWhiteSpace(Vat) ? "" : VatCountryCode + " " + Vat;
        }

        public bool HasRequiredFieldsFilledIn()
        {
            return
                !string.IsNullOrWhiteSpace(LanguageCode) &&
                !string.IsNullOrWhiteSpace(FirstName) &&
                !string.IsNullOrWhiteSpace(FamilyName) &&
                !string.IsNullOrWhiteSpace(Address) &&
                !string.IsNullOrWhiteSpace(PostalCode) &&
                !string.IsNullOrWhiteSpace(Place) &&
                !string.IsNullOrWhiteSpace(Country) &&
                !string.IsNullOrWhiteSpace(PhoneNumber) &&
                !string.IsNullOrWhiteSpace(Email);
        }

        public string GenerateInfoTableHtml()
        {
            return "<table width='900' style='border: 1px solid rgb(196,0,14)'>" +
                "<tr><td style='width: 25%'>Voornaam:</td><td style='width: 25%'>" + FirstName + "</td><td style='width: 25%'>Familienaam:</td><td style='width: 25%'>" + FamilyName + "</td></tr>" +
                "<tr><td>Bedrijf:</td><td>" + Company + "</td><td>Telefoonnummer:</td><td>" + GetPhoneNumber() + "</td></tr>" +
                "<tr><td>Adres:</td><td>" + Address + "</td><td>Postcode</td><td>" + PostalCode + "</td></tr>" +
                "<tr><td>Plaats:</td><td>" + Place + "</td><td>Land:</td><td>" + Country + "</td></tr>" +
                "<tr><td>Email:</td><td>" + GetVat() + "</td><td>Email:</td><td>" + Email + "</td></tr>" +
                "</table>";
        }

        public string GetFullName()
        {
            return FirstName + " " + FamilyName;
        }
        #endregion
    }
}



public class CountryCode
{
    public string Code { get; set; }
    public string Name { get; set; }
}
