﻿using AuthenticPhoto.Business.Authentication;

namespace AuthenticPhoto.Business.Authentication
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    using Microsoft.AspNet.Identity.EntityFramework;

    /// <summary>
    /// A basic implementation for an application database context compatible with ASP.NET Identity 2 using
    /// <see cref="long"/> as the key-column-type for all entities.
    /// </summary>
    /// <remarks>
    /// This type depends on some other types out of this assembly.
    /// </remarks>
    public class ApplicationDbContext : IdentityDbContext<AuthenticUser, AuthenticRole, long, AuthenticLogin, AuthenticUserRole, AuthenticClaim>
    {
        #region constructors and destructors

        public ApplicationDbContext()
            : base("EFDbContext")
        {
        }

        #endregion

        #region methods

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // Map Entities to their tables.
            modelBuilder.Entity<AuthenticUser>().ToTable("AuthenticUser");
            modelBuilder.Entity<AuthenticRole>().ToTable("Role");
            modelBuilder.Entity<AuthenticClaim>().ToTable("UserClaim");
            modelBuilder.Entity<AuthenticLogin>().ToTable("UserLogin");
            modelBuilder.Entity<AuthenticUserRole>().ToTable("UserRole");
            // Set AutoIncrement-Properties
            modelBuilder.Entity<AuthenticUser>().Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<AuthenticClaim>().Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<AuthenticRole>().Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            // Override some column mappings that do not match our default
            //modelBuilder.Entity<AuthenticUser>().Property(r => r.UserName).HasColumnName("Login");
            modelBuilder.Entity<AuthenticUser>().Property(r => r.PasswordHash).HasColumnName("Password");
        }

        #endregion
    }
}