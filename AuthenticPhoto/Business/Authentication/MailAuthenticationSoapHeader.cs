﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

namespace AuthenticPhoto.Business.Authentication
{
    public class MailAuthenticationSoapHeader : SoapHeader
    {
        public string Passphrase { get; set; }
    }
}