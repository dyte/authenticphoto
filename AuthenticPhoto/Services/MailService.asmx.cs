﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.Services.Protocols;

using AuthenticPhoto.Business.Authentication;
using AuthenticPhoto.Business.Constants;
using AuthenticPhoto.Business.Encryption;
using AuthenticPhoto.Business.Modules.Mail;
using AuthenticPhoto.Business.Modules.Synchronization;
using AuthenticPhoto.Data.Synchronization;
using AuthenticPhoto.Data.Synchronization.Results;

using Umbraco.Core;

namespace AuthenticPhoto.Services
{
    [WebService(Namespace = "http://www.AuthenticPhoto.be/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MailService : System.Web.Services.WebService
    {
        public MailAuthenticationSoapHeader Authentication { get; set; }

        [WebMethod(MessageName = "GetMail")]
        [SoapHeader("Authentication")]
        public GetMailResult GetMail(string mailAlias)
        {
            var trustedParty = this.CheckHash(mailAlias);

            if (!trustedParty)
            {
                return new GetMailResult
                {
                    Success = false,
                    ResultCode = MailResultCodes.ERROR,
                    Feedback = new List<string> { "Authentication failed" },
                    MailDefinitions = new List<MailDefinition>()
                };
            }

            return new Business.Services.MailService().GetMail(mailAlias);
        }

        [WebMethod(MessageName = "GetMailByLanguage")]
        [SoapHeader("Authentication")]
        public GetMailResult GetMail(string mailAlias, string language)
        {
            var trustedParty = this.CheckHash(mailAlias);

            if (!trustedParty)
            {
                return new GetMailResult
                {
                    Success = false,
                    ResultCode = MailResultCodes.ERROR,
                    Feedback = new List<string> { "Authentication failed" },
                    MailDefinitions = new List<MailDefinition>()
                };
            }

            return new Business.Services.MailService().GetMail(mailAlias, language);
        }

        [WebMethod(MessageName = "GetInstalledCultures")]
        [SoapHeader("Authentication")]
        public GetInstalledCulturesSynchronizationResult GetInstalledCultures()
        {
            return new GetInstalledCulturesSynchronizationResult
            {
                Success = true,
                ResultCode = ProductDbSynchronizationResultCodes.OK,
                CultureCodes = FetchCultureCodes()
            };
        }

        private static List<InstalledCulture> FetchCultureCodes()
        {
            return
                ApplicationContext.Current.Services.LocalizationService.GetAllLanguages()
                    .Select(x => new InstalledCulture { CultureCode = x.IsoCode })
                    .ToList();
        }

        private bool CheckHash(string identifier)
        {
            return Hashing.GetHash($"{identifier}-{ApplicationConstants.SynchronizationSalt}", Hashing.HashType.Sha1).Equals(this.Authentication.Passphrase);
        }
    }
}
