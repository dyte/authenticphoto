﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.Services.Protocols;

using AuthenticPhoto.Business.Authentication;
using AuthenticPhoto.Business.Constants;
using AuthenticPhoto.Business.Encryption;
using AuthenticPhoto.Business.Modules.Synchronization;
using AuthenticPhoto.Business.Services;
using AuthenticPhoto.Data.Synchronization;
using AuthenticPhoto.Data.Synchronization.Results;

using Umbraco.Core;

namespace AuthenticPhoto.Services
{
    
    [WebService(Namespace = "http://www.AuthenticPhoto.be/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ProductDbSynchronizationService : WebService
    {
        public SynchronizationAuthenticationSoapHeader Authentication { get; set; }

        [WebMethod(MessageName = "AddProductFamily")]
        [SoapHeader("Authentication")]
        public ProductFamilySynchronizationResult Add(ProductFamily productFamily)
        {
            var trustedParty = this.CheckHash(productFamily.ProductDbIdentifier);

            if (!trustedParty)
            {
                return new ProductFamilySynchronizationResult
                {
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { "Authentication failed" },
                    ProductFamily = null,
                    DocumentId = -1
                };
            }

            return new ProductDbService().AddProductFamily(productFamily);
        }

        [WebMethod(MessageName = "UpdateProductFamily")]
        [SoapHeader("Authentication")]
        public ProductFamilySynchronizationResult Update(ProductFamily productFamily)
        {
            var trustedParty = this.CheckHash(productFamily.ProductDbIdentifier);

            if (!trustedParty)
            {
                return new ProductFamilySynchronizationResult
                {
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Authentication failed" },
                    ProductFamily = null,
                    DocumentId = -1
                };
            }

            return new ProductDbService().UpdateProductFamily(productFamily);
        }

        [WebMethod(MessageName = "DeleteProductFamily")]
        [SoapHeader("Authentication")]
        public ProductFamilySynchronizationResult Delete(ProductFamily productFamily, bool permanent)
        {
            var trustedParty = this.CheckHash(productFamily.ProductDbIdentifier);

            if (!trustedParty)
            {
                return new ProductFamilySynchronizationResult
                {
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Authentication failed" },
                    ProductFamily = null,
                    DocumentId = -1
                };
            }

            return new ProductDbService().DeleteProductFamily(productFamily, permanent);
        }

        [WebMethod(MessageName = "AddProduct")]
        [SoapHeader("Authentication")]
        public ProductSynchronizationResult Add(Product product)
        {
            var trustedParty = this.CheckHash(product.ProductDbIdentifier);

            if (!trustedParty)
            {
                return new ProductSynchronizationResult
                {
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Authentication failed" },
                    Product = null,
                    DocumentId = -1
                };
            }

            return new ProductDbService().AddProduct(product);
        }

        [WebMethod(MessageName = "UpdateProduct")]
        [SoapHeader("Authentication")]
        public ProductSynchronizationResult Update(Product product)
        {
            var trustedParty = this.CheckHash(product.ProductDbIdentifier);

            if (!trustedParty)
            {
                return new ProductSynchronizationResult
                {
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Authentication failed" },
                    Product = null,
                    DocumentId = -1
                };
            }

            return new ProductDbService().UpdateProduct(product);
        }

        [WebMethod(MessageName = "DeleteProduct")]
        [SoapHeader("Authentication")]
        public ProductSynchronizationResult Delete(Product product, bool permanent)
        {
            var trustedParty = this.CheckHash(product.ProductDbIdentifier);

            if (!trustedParty)
            {
                return new ProductSynchronizationResult
                {
                    Success = false,
                    ResultCode = ProductDbSynchronizationResultCodes.ERROR,
                    Feedback = new List<string> { $"Authentication failed" },
                    Product = null,
                    DocumentId = -1
                };
            }

            return new ProductDbService().DeleteProduct(product, permanent);
        }

        [WebMethod(MessageName = "GetInstalledCultures")]
        [SoapHeader("Authentication")]
        public GetInstalledCulturesSynchronizationResult GetInstalledCultures()
        {
            return new GetInstalledCulturesSynchronizationResult
            {
                Success = true,
                ResultCode = ProductDbSynchronizationResultCodes.OK,
                CultureCodes = FetchCultureCodes()
            };
        }

        private static List<InstalledCulture> FetchCultureCodes()
        {
            return
                ApplicationContext.Current.Services.LocalizationService.GetAllLanguages()
                    .Select(x => new InstalledCulture { CultureCode = x.IsoCode })
                    .ToList();
        }

        private bool CheckHash(string identifier)
        {
            return Hashing.GetHash($"{identifier}-{ApplicationConstants.SynchronizationSalt}", Hashing.HashType.Sha1).Equals(this.Authentication.Passphrase);
        }
    }
}