//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	// Mixin content Type 1293 with alias "dummy"
	/// <summary>Dummy</summary>
	public partial interface IDummy : IPublishedContent
	{
		/// <summary>DummyField</summary>
		string DummyField { get; }
	}

	/// <summary>Dummy</summary>
	[PublishedContentModel("dummy")]
	public partial class Dummy : PublishedContentModel, IDummy
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "dummy";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Dummy(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Dummy, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// DummyField
		///</summary>
		[ImplementPropertyType("dummyField")]
		public string DummyField
		{
			get { return GetDummyField(this); }
		}

		/// <summary>Static getter for DummyField</summary>
		public static string GetDummyField(IDummy that) { return that.GetPropertyValue<string>("dummyField"); }
	}
}
