﻿using System;
using System.Web.Mvc;
using AuthenticPhoto.Business.ProductTool.Ingest;
using System.IO;
using Umbraco.Web.Mvc;
using Umbraco.Web;
using AuthenticPhoto.Business.AuthenticOrder;
using System.Configuration;
using System.Web;
using AuthenticPhoto.Business.ProductTool;
using System.Linq;
using AuthenticPhoto.Business.Abstract;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using AuthenticPhoto.Hubs;

namespace AuthenticPhoto.Controllers
{
    public class UploadController : SurfaceController
    {
        static string TOPROCESSDIR = ConfigurationManager.AppSettings["ToProcessFolder"];
        static string UPLOADTEMPDIR = ConfigurationManager.AppSettings["UploadTempFolder"];
        private DbConnector dbConnector;
        private IProductRepository productRepository;

        public UploadController(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
            dbConnector = new DbConnector(this.productRepository);
        }

        [HttpPost]
        public ActionResult UploadFile(FineUpload upload)
        {
            string uploadedTemp = Path.Combine(UPLOADTEMPDIR, upload.OrigFilename + upload.TotalFileSizeName);
            string uploadedLocation = Path.Combine(TOPROCESSDIR, upload.OrderID, "0");

            if (!Directory.Exists(uploadedTemp))
            {
                Directory.CreateDirectory(uploadedTemp);
            }

            string filePath = Path.Combine(uploadedTemp, fillWithZeros(upload.PartIndex, 4) + ".tmp");
            if (!System.IO.File.Exists(filePath))
            {
                Stream inputStream = upload.InputStream;

                using (FileStream fileStream = System.IO.File.OpenWrite(filePath))
                {
                    inputStream.CopyTo(fileStream);
                }
            }
            else
            {
                //chunk present, skipping" // TODO: log this
            }

            int completed = 0;

            if ( Convert.ToInt32(upload.PartIndex) == upload.TotalParts - 1 && Directory.GetFiles(uploadedTemp).Length == upload.TotalParts)
            {
                string newFilename;
                string extension = Path.GetExtension(upload.OrigFilename);
                if (upload.IsSignature)
                {
                    newFilename = $"signature_{upload.AccompanyingImageID}{extension}";
                } else
                {
                    newFilename = $"{upload.UuidName}{extension}";
                    dbConnector.AddPlaceholderImage(upload.UuidName, upload.OrigFilename, upload.OrderID);
                }
                mergeTempFiles(uploadedTemp, uploadedLocation, newFilename);
                completed = 1;
            }
            return new FineUploaderResult(true, new { completed = completed, uploadedImageID = upload.UuidName }); // params probably not in use
        }

        [HttpPost]
        public void StartPollingForCompletedImages(string orderID, string[] ids)
        {
            List<string> unprocessed = new List<string>();
            unprocessed.AddRange(ids);
            int delay = 1000;
            var cancellationTokenSource = new CancellationTokenSource();
            var token = cancellationTokenSource.Token;
            var context = GlobalHost.ConnectionManager.GetHubContext<UploadedFilesHub>();

            context.Clients.All.idsRemaining(unprocessed.ToArray());

            var listener = Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    try
                    {
                        string[] unprocessedImageIds = dbConnector.GetUnprocessedImages(orderID, ids);
                        if (unprocessedImageIds.Length == 0)
                        {
                            cancellationTokenSource.Cancel();
                            context.Clients.All.allFilesProcessed();
                        }
                        else if(unprocessedImageIds.Length != unprocessed.Count)
                        {
                            context.Clients.All.idsRemainingInProcessQueue(unprocessedImageIds);
                            unprocessed = unprocessedImageIds.ToList();
                        }
                        Thread.Sleep(delay);
                        if (token.IsCancellationRequested)
                            break;
                    }
                    catch (NoImagesInOrderException)
                    {
                        context.Clients.All.error("No images in DB for orderID " + orderID);
                        cancellationTokenSource.Cancel();
                    }
                }
            }, token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        private string GenerateImageID()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();
            return new string(
                Enumerable.Repeat(chars, 10)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
        }

        private string fillWithZeros(string orig, int totalLength)
        {
            string result = "";
            for (int i = 0; i < totalLength - orig.Length; i++)
            {
                result += "0";
            }
            return result + orig;
        }

        public void mergeTempFiles(string pathOrigin, string pathToSave, string filename)
        {
            string[] tmpfiles = Directory.GetFiles(pathOrigin, "*.tmp");

            if (!Directory.Exists(pathToSave))
            {
                Directory.CreateDirectory(pathToSave);
            }

            Array.Sort(tmpfiles);

            FileStream outPutFile = new FileStream(Path.Combine(pathToSave, filename), FileMode.Create, FileAccess.Write);

            foreach (string tempFile in tmpfiles)
            {
                int bytesRead = 0;
                byte[] buffer = new byte[1024];
                System.IO.FileStream inputTempFile = new System.IO.FileStream(tempFile, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Read);
                while ((bytesRead = inputTempFile.Read(buffer, 0, 1024)) > 0)
                    outPutFile.Write(buffer, 0, bytesRead);
                inputTempFile.Close();
                //System.IO.File.Delete(tempFile);
            }
            outPutFile.Close();
            outPutFile.Dispose();
        }

        public ActionResult Index()
        {
            return Content("Hello from the upload controller. Post to action UploadFile.");
        }

        [ValidateInput(false)]
        public string GetImageInfo(object imagepart)
        {
            string base64string = (string)((string[])imagepart)[0];
            base64string = base64string.Substring(base64string.IndexOf(",") + 1);

            byte[] bytearray = Convert.FromBase64String(base64string);

            AuthenticPhoto.Business.ProductTool.Ingest.ImageProcessor processor = new AuthenticPhoto.Business.ProductTool.Ingest.ImageProcessor();
            string jsoninfo = processor.GetInformation(bytearray);

            return jsoninfo;
        }

    }

    public class NoImagesInOrderException : Exception
    {

    }
}
