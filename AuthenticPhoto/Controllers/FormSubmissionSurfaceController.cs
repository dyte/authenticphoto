﻿using AuthenticPhoto.Business.Authentication;
using System;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Configuration;
using AuthenticPhoto.Business.ProductTool;
using System.Collections.Generic;

namespace AuthenticPhoto.Controllers
{
    public class FormSubmissionController : SurfaceController
    {
        private AuthenticUserManager _userManager;
        private static string adminEmail = ConfigurationManager.AppSettings["AdminEmail"];
        private DictionaryConnector dictionaryConnector;

        public AuthenticUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<AuthenticUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public FormSubmissionController()
        {
            dictionaryConnector = new DictionaryConnector(System.Web.HttpContext.Current);
        }


        private string GetHtmlTemplateFromSession(string name)
        {
            if (Session["HtmlTemplates"] == null)
            {
                return "Exception: html templates not found in session";
            }
            else
            {
                Dictionary<string, string> htmlTemplates = (Dictionary<string, string>)Session["HtmlTemplates"];
                return htmlTemplates.ContainsKey(name) ? htmlTemplates[name] : "Exception: " + name + " not found in mail templates";
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> NewsletterFormSubmitted(string firstName, string lastName, string email)
        {
            var existingUser = await UserManager.FindByEmailAsync(email);

            IdentityResult result;

            if(existingUser != null)
            {
                if(existingUser.ContactPermission)
                {
                    return Content(dictionaryConnector.GetTranslation("Form.Newsletter.AlreadySubscribed"));
                }
                else
                {
                    existingUser.ContactPermission = true;
                    existingUser.Source += "newsletter ";
                    result = await UserManager.UpdateAsync(existingUser);
                }
            }
            else
            {
                AuthenticUser newContact = new AuthenticUser
                {
                    FirstName = firstName,
                    FamilyName = lastName,
                    Email = email,
                    UserName = email,
                    Source = "newsletter ",
                    ContactPermission = true
                };
                result = await UserManager.CreateAsync(newContact);
            }            
            if (result.Succeeded)
            {
                EmailService service = new EmailService();
                IdentityMessage message = new IdentityMessage();
                message.Body = GetHtmlTemplateFromSession("NewsletterConfirmation_" + dictionaryConnector.GetUmbracoCulture());
                message.Body += AAccountController.ConstructCommercialFooter();
                message.Destination = email;
                message.Subject = GetHtmlTemplateFromSession("NewsletterConfirmation_subject_" + dictionaryConnector.GetUmbracoCulture());
                service.Send(message);

                return Content("0");
            }
            else
            {                
                return Content("500");
            }
        }

    }
}