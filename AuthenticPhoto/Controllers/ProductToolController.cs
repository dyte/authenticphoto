﻿using AuthenticPhoto.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using AuthenticPhoto.Business.ProductTool;
using System.Collections.Generic;
using AuthenticPhoto.Business.ProductTool.Entities;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using AuthenticPhoto.Business.Authentication;
using Microsoft.AspNet.Identity;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Umbraco.Web;
using AuthenticPhoto.ServiceReference1;
using Umbraco.Core.Models;
using System.Configuration;
using Newtonsoft.Json;
using Pulzonic.Multipartial;
using umbraco.MacroEngines;
using System.IO;
using AuthenticPhoto.Business.Abstract;
using System.Globalization;
using System.Threading;
using Umbraco.Web.Models;
using AuthenticPhoto.Business.AuthenticOrder;
using Umbraco.Web.Routing;

namespace AuthenticPhoto.Controllers
{

    public class ProductToolController : SurfaceController
    {
        private AuthenticUserManager _userManager;
        private DbConnector dbConnector;
        private DictionaryConnector dictionaryConnector;
        static string TOPROCESSDIR = ConfigurationManager.AppSettings["ToProcessFolder"];
        static string PROCESSEDDIR = ConfigurationManager.AppSettings["ProcessedFolder"];

        public AuthenticUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<AuthenticUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IProductRepository productRepository;

        public ProductToolController(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
            dbConnector = new DbConnector(this.productRepository);
            dictionaryConnector = new DictionaryConnector(System.Web.HttpContext.Current);
        }

        public AuthenticOrder CheckAuthenticOrder()
        {
            AuthenticOrder authenticOrder = GetAuthenticOrder();
            if (authenticOrder == null)
            {
                string orderID = Request.Cookies["authentic_orderid"] != null ? Request.Cookies["authentic_orderid"].Value : "";
                if (string.IsNullOrWhiteSpace(orderID))
                {
                    authenticOrder = dbConnector.AddOrder();
                    Response.Cookies.Add(createAuthenticOrderCookie(authenticOrder.OrderID));
                }
                else
                {
                    authenticOrder = dbConnector.GetOrder(orderID);
                    if (authenticOrder == null)
                    {
                        authenticOrder = dbConnector.AddOrder();
                        Response.Cookies.Add(createAuthenticOrderCookie(authenticOrder.OrderID));
                    }
                }
                SetAuthenticOrder(authenticOrder);
            }
            return authenticOrder;
        }

        private HttpCookie createAuthenticOrderCookie(string orderID)
        {
            HttpCookie orderCookie = new HttpCookie("authentic_orderid");
            orderCookie.Value = orderID;
            orderCookie.Expires = DateTime.Now.AddDays(30d);
            return orderCookie;
        }

        [HttpPost]
        public string GetImagesCountInOrder() // temp; basket should eventually show order count
        {
            CheckAuthenticOrder();
            return Convert.ToString(((AuthenticOrder)Session["AuthenticOrder"]).Images.Count);
        }

        //[HttpPost]
        //public string GetOrderCount()
        //{
        //    return Convert.ToString(((AuthenticOrder)Session["AuthenticOrder"]).Orders.Count);
        //}

        private IPublishedContent GetProductContentForBackofficeId(string backofficeID)
        {
            IPublishedContent productContent = null;
            foreach (IPublishedContent upf in GetUmbracoProductFamilies())
            {
                productContent = upf.Descendants().Where(d => d.DocumentTypeAlias == "Product").FirstOrDefault(p => p.GetPropertyValue("backofficeIdentifier").ToString().Equals(Convert.ToString(backofficeID)));
                if (productContent != null)
                {
                    break;
                }
            }
            return productContent;
        }

        private IPublishedContent GetProductContentForId(string id)
        {
            IPublishedContent productContent = null;
            foreach (IPublishedContent upf in GetUmbracoProductFamilies())
            {
                productContent = upf.Descendants().Where(d => d.DocumentTypeAlias == "Product").FirstOrDefault(p => p.Id == int.Parse(id));
                if (productContent != null)
                {
                    break;
                }
            }
            return productContent;
        }


        private string GetHtmlTemplateFromSession(string name)
        {
            if (Session["HtmlTemplates"] == null)
            {
                return "Exception: html templates not found in session";
            }
            else
            {
                Dictionary<string, string> htmlTemplates = (Dictionary<string, string>)Session["HtmlTemplates"];
                return htmlTemplates.ContainsKey(name) ? htmlTemplates[name] : "Exception: " + name + " not found in mail templates";
            }
        }

        private IPublishedContent[] GetUmbracoProductFamilies()
        {
            if (Session["UmbracoProductFamilies"] != null)
                return (IPublishedContent[])Session["UmbracoProductFamilies"];
            return null;
        }

        public ActionResult ProductInfo(string id)
        {
            bool renderGeneralInfo = (id == null || id.Trim().Equals("-1") || id.Trim().Equals("0"));

            if (!renderGeneralInfo)
            {
                IPublishedContent productContent = GetProductContentForBackofficeId(id);
                if (productContent == null)
                {
                    renderGeneralInfo = true;
                }
                else
                {
                    if(productContent.GetPropertyValue<string>("redirectsToAnotherProductPage").ToLower().Equals("true"))
                    {
                        productContent = GetProductContentForId(productContent.GetPropertyValue<string>("productIDToRedirectTo"));
                    }
                    var pcr = new PublishedContentRequest(new Uri(productContent.UrlAbsolute()), UmbracoContext.Current.RoutingContext);
                    pcr.Prepare();
                    UmbracoContext.PublishedContentRequest = pcr;
                    return PartialView("ProductInfo", productContent);
                }
            }
            if (renderGeneralInfo)
            {
                UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                IPublishedContent uploaderInfoContent;
                switch (dictionaryConnector.GetUmbracoCulture())
                {
                    case DictionaryConnector.UmbracoCultureNL: uploaderInfoContent = umbracoHelper.TypedContent(1688); break;
                    case DictionaryConnector.UmbracoCultureFR: uploaderInfoContent = umbracoHelper.TypedContent(1678); break;
                    default: uploaderInfoContent = umbracoHelper.TypedContent(1679); break;
                }
                var pcr = new PublishedContentRequest(new Uri(uploaderInfoContent.UrlAbsolute()), UmbracoContext.Current.RoutingContext);
                pcr.Prepare();
                UmbracoContext.PublishedContentRequest = pcr;
                return PartialView("UploaderInfo", uploaderInfoContent);
            }
            return new EmptyResult();
        }

        public ActionResult SwitchImagePreferences(string imgID)
        {
            AuthenticOrder order = GetAuthenticOrder();
            UploadedImage img = GetAuthenticOrder().Images.FirstOrDefault(i => i.ID == imgID);
            MultipartialResult multipartialResult = new MultipartialResult(this);
            multipartialResult.AddView("Finishing", "col_prefs_finishing", new FinishingModel
            {
                OrderID = order.OrderID,
                Image = img,
                ProductFamilies = productRepository.GetProductFamiliesWithChildListing(),
                Dictionary = dictionaryConnector.GetDictionaryFor("Finishing"),
                LanguageCode = dictionaryConnector.GetUmbracoCulture()
            });
            return multipartialResult;
        }

        public ActionResult SelectProductCalc(string newProductId, string preferencesSet)
        {
            UploadedImage img = GetCalcImageFromSession();
            if(newProductId.Equals("0") || newProductId.Equals("-1"))
            {
                img.SelectedProduct = null;
                img.Preferences.Clear();
                img.ResetPrices();
            }
            else
            {
                img.SelectedProduct = productRepository.GetProductWithPopulatedChildren(Convert.ToInt32(newProductId));
                SetDefaults(img, false);
            }            
            return GetUIUpdateData(img, false);
        }

        public ActionResult SelectProduct(string imgID, string newProductId)
        {
            AuthenticOrder order = GetAuthenticOrder();
            UploadedImage img = GetAuthenticOrder().Images.FirstOrDefault(i => i.ID == imgID);
            if (newProductId.Equals("0") || newProductId.Equals("-1"))
            {
                img.SelectedProduct = null;
                img.Preferences.Clear();
                img.BorderWidth = 0;
                img.ResetPrices();
            }
            else
            {
                img.SelectedProduct = productRepository.GetProductWithPopulatedChildren(Convert.ToInt32(newProductId));
                SetDefaults(img, true);
            }
            return GetUIUpdateData(img, true);
        }

        public MultipartialResult GetUIUpdateData(UploadedImage img, bool isOrder)
        {
            AuthenticOrder order = GetAuthenticOrder();
            FinishingModel model = new FinishingModel
            {
                OrderID = order.OrderID,
                Image = img,
                ProductFamilies = productRepository.GetProductFamiliesWithChildListing(),
                Dictionary = dictionaryConnector.GetDictionaryFor("Finishing"),
                LanguageCode = dictionaryConnector.GetUmbracoCulture()
            };

            MultipartialResult multipartialResult = new MultipartialResult(this);
            multipartialResult.AddView("PriceDetails", "phPriceDetails", new PriceDetailsViewModel { Image = img, OrderID = order.OrderID, Dictionary = dictionaryConnector.GetDictionaryFor("PriceDetails"), LanguageCode = dictionaryConnector.GetUmbracoCulture(), Context = PriceDetailsViewModel.PriceDetailsContext.Order });
            multipartialResult.AddView("OptionTypeSet", "optionTypesDiv", model);

            if (!isOrder) // calculator scenario
            {
                multipartialResult.AddContent(Business.ProductTool.Formatting.PrintAsKg(img.GetWeight()), "lblImageWeight");
            }
            else
            {
                multipartialResult.AddView("ShowPrice", "priceDiv", new PriceWeightViewModel
                {
                    Image = img,
                    ImagePrice = img.GetTotalPrice(),
                    TotalPrice = CalculateTotalPrice(),
                    ImageWeight = img.GetWeight(),
                    TotalWeight = CalculateTotalWeight(),
                    Dictionary = dictionaryConnector.GetDictionaryFor("ShowPrice")
                });
                multipartialResult.AddView("TotalPriceDetails", "totalPriceRecap",
                    new TotalPriceDetailsViewModel
                    {
                        Order = GetAuthenticOrder(),
                        Dictionary = dictionaryConnector.GetDictionaryFor("TotalPriceDetails")
                    });
            }
            return multipartialResult;
        }

        public void SetDefaults(UploadedImage image, bool updateInOrder)
        {
            image.Preferences.Clear();
            foreach (OptionType optionType in image.SelectedProduct.OptionTypes)
            {
                if (optionType.Required && (image.SelectedProduct.PreselectOptions || optionType.getAllOptions().Count == 1) )
                {
                    image.Preferences[optionType.OptionTypeID] = GetCheapestOptionFor(optionType);
                }
            }
            CalculateImagePrice(image, updateInOrder);
        }

        private void CalculateImagePrices(List<UploadedImage> images, bool updateInOrder)
        {
            Business.Authentication.AuthenticUser user = GetUserFromSession();
            string discountsOnProducts = user == null ? "" : user.DiscountsOnProducts;
            foreach (UploadedImage image in images)
            {
                image.CalculatePrice(discountsOnProducts);
                if (updateInOrder)
                {
                    UpdateImageInOrder(image);
                }
            }
        }

        private void CalculateImagePrice(UploadedImage image, bool updateInOrder)
        {
            Business.Authentication.AuthenticUser user = GetUserFromSession();
            string discountsOnProducts = (user == null || user.DiscountsOnProducts == null) ? "" : user.DiscountsOnProducts;
            image.CalculatePrice(discountsOnProducts);
            if (updateInOrder)
            {
                UpdateImageInOrder(image);
            }
        }

        public ActionResult OptionTypeSet(FinishingModel model)
        {
            return View("OptionTypeSet", model);
        }


        //public ActionResult OptionTypeSetCalculator(string sessionID, string orderID)
        //{
        //    FinishingModel model = new FinishingModel
        //    {
        //        SessionID = sessionID,
        //        OrderID = orderID,
        //        Image = GetCalcImageFromSession(),
        //        ProductFamilies = entityCacher.GetProductFamilies().ToList(),
        //        Dictionary = dictionaryConnector.GetDictionaryFor("Finishing"),
        //        LanguageCode = dictionaryConnector.GetUmbracoCulture()
        //    };
        //    return View("OptionTypeSet",model);
        //}

        //public Dictionary<string, string> ComposeIllegalCombos()
        //{
        //    Dictionary<string, string> uncombinableMsg = new Dictionary<string, string>();
        //    JObject illegalCombos = new JObject();
        //    foreach (Option option in repository.Options)
        //    {
        //        string illegalOptions = option.IllegalCombinations;
        //        if (!string.IsNullOrWhiteSpace(illegalOptions))
        //        {
        //            illegalCombos[Convert.ToString(option.OptionID)] = illegalOptions;
        //        }
        //    }
        //    return uncombinableMsg;
        //}



        public Option GetCheapestOptionFor(OptionType optionType)
        {
            PriceCalculator priceCalculator = new PriceCalculator(100, 100);
            decimal cheapest = decimal.MaxValue;
            decimal price;
            Option cheapestOption = null;

            OptionType populatedOptionType = productRepository.GetOptionTypeWithPopulatedChildrenInclSizes(optionType.OptionTypeID);

            foreach (OptionCategory optionCategory in populatedOptionType.OptionCategories)
            {
                foreach (Option option in optionCategory.Options)
                {

                    price = priceCalculator.Calculate(option);
                    if (price < cheapest)
                    {
                        cheapest = price;
                        cheapestOption = option;
                    }
                }
            }
            return cheapestOption;
        }

        public void SavePreferences(UploadedImage img, Dictionary<string,string> prefSet)
        {            
            img.Preferences.Clear();
            foreach (string key in prefSet.Keys)
            {
                if (key == "product")
                {
                    if (prefSet[key] != "0")
                    {
                        img.SelectedProduct = productRepository.GetProductWithPopulatedChildren(Convert.ToInt32(prefSet[key]));
                    }
                }
                else if (key == "noCertificate")
                {
                    img.Signature.SignatureNoWarrantyLabel = prefSet[key].ToLower().Equals("true");
                }
                else if (key.StartsWith("signature"))
                {
                    switch (key)
                    {
                        case "signatureActive":
                            img.Signature.SignatureActive = prefSet[key].ToLower().Equals("true"); break;
                        case "signatureLocation":
                            img.Signature.SignatureLocation = (SignLocation)Enum.Parse(typeof(SignLocation), prefSet[key]); break;
                        case "signaturePosition":
                            img.Signature.SignaturePosition = (SignPosition)Enum.Parse(typeof(SignPosition), prefSet[key]); break;
                        case "signatureLabelSize":
                            img.Signature.SignatureLabelSize = (SignLabelSize)Enum.Parse(typeof(SignLabelSize), prefSet[key]); break;
                        case "signatureDelivery":
                            img.Signature.SignatureDelivery = (SignDelivery)Enum.Parse(typeof(SignDelivery), prefSet[key]); break;
                        case "signatureNoWarrantyLabel":
                            img.Signature.SignatureNoWarrantyLabel = prefSet[key].ToLower().Equals("true"); break;
                    }
                }
                else if (key == "testprint")
                {
                    switch (prefSet[key].ToLower())
                    {
                        case "a4":
                            img.Testprint = UploadedImage.TestPrintOption.A4; img.Softproof = false; break;
                        case "oneone":
                            img.Testprint = UploadedImage.TestPrintOption.OneOne; img.Softproof = false; break;
                        default:
                            img.Testprint = UploadedImage.TestPrintOption.None; break;
                    }
                }
                else if (key == "softproof")
                {
                    if (prefSet[key].ToLower().Equals("true"))
                    {
                        img.Softproof = true;
                        img.Testprint = UploadedImage.TestPrintOption.None;
                    }
                    else
                    {
                        img.Softproof = false;
                    }
                }
                else if (key == "inspect")
                {
                    img.InspectBeforePrint = prefSet[key].ToLower().Equals("true");
                }
                else if (key == "dontcut")
                {
                    img.DontCut = prefSet[key].ToLower().Equals("true");
                }
                else if (key == "labmessage")
                {
                    img.LabMessage = prefSet[key];
                }
                else if (key == "qty")
                {
                    int quantity;
                    if (int.TryParse(prefSet[key], out quantity))
                    {
                        img.Quantity = quantity;
                    }
                }
                else if (key == "borderWidth")
                {
                    int borderWidth;
                    if (int.TryParse(prefSet[key], out borderWidth))
                    {
                        if(new int[] { 0, 1, 2, 3, 5, 8, 12 }.Contains(borderWidth))
                        {
                            img.BorderWidth = borderWidth;
                        }
                    }
                }
                else if (key == "usingCustomDimensions")
                {
                    img.UsingCustomDimensions = prefSet[key].ToLower().Equals("true");
                    if (!img.UsingCustomDimensions)
                    {
                        img.ResetDimensions();
                    }
                }
                else if (key == "customDim1")
                {
                    decimal customdim;
                    if (decimal.TryParse(prefSet[key], out customdim))
                    {
                        img.Dim1 = customdim * 10;
                    }
                }
                else if (key == "customDim2")
                {
                    decimal customdim;
                    if (decimal.TryParse(prefSet[key], out customdim))
                    {
                        img.Dim2 = customdim * 10;
                    }
                }
                else if (key == "customDpi")
                {
                    int customdpi;
                    if (int.TryParse(prefSet[key], out customdpi))
                    {
                        img.XResolution = customdpi;
                    }
                }
                else
                {
                    if (!prefSet[key].Equals("-1")) // nothing chosen
                    {
                        Option option = prefSet[key].Equals("0") ? null : productRepository.GetOption(Convert.ToInt32(prefSet[key]));
                        option.RollSizes = productRepository.GetRollSizesForOption(option.OptionID);
                        option.SheetSizes = productRepository.GetSheetSizesForOption(option.OptionID);
                        img.Preferences[Convert.ToInt32(key)] = option;
                    }
                }
            }
            CalculateImagePrice(img, false);
            UpdateImageInOrder(img);
        }

        public decimal CalculateTotalPrice()
        {
            AuthenticOrder order = GetAuthenticOrder();
            decimal total = 0;
            foreach (UploadedImage image in order.Images)
            {
                total += image.GetTotalPrice() * image.Quantity;
            }
            return total;
        }

        public double CalculateTotalWeight()
        {
            AuthenticOrder order = GetAuthenticOrder();
            double total = 0;
            foreach (UploadedImage image in order.Images)
            {
                total += image.Quantity * image.GetWeight();
            }
            return total;
        }


        public ActionResult SaveImagePreferences(string imageId, string preferencesSet = null)
        {
            AuthenticOrder order = GetAuthenticOrder();
            UploadedImage img = order.Images.FirstOrDefault(i => i.ID == imageId);
            Dictionary<string, string> prefSet = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(preferencesSet);

            if (!string.IsNullOrWhiteSpace(preferencesSet))
            {
                SavePreferences(img, prefSet);
            }

            MultipartialResult multipartialResult = new MultipartialResult(this);
            multipartialResult.AddView("ShowPrice", "priceDiv", new PriceWeightViewModel
            {
                Image = img,
                ImagePrice = img.GetTotalPrice(),
                TotalPrice = CalculateTotalPrice(),
                ImageWeight = img.GetWeight(),
                TotalWeight = CalculateTotalWeight(),
                Dictionary = dictionaryConnector.GetDictionaryFor("ShowPrice")
            });
            multipartialResult.AddView("PriceDetails", "phPriceDetails", new PriceDetailsViewModel
            {
                Image = img,
                OrderID = order.OrderID,
                Dictionary = dictionaryConnector.GetDictionaryFor("PriceDetails"),
                LanguageCode = dictionaryConnector.GetUmbracoCulture(),
                Context = PriceDetailsViewModel.PriceDetailsContext.Order
            });
            multipartialResult.AddView("TotalPriceDetails", "totalPriceRecap", new TotalPriceDetailsViewModel
            {
                Order = order,
                Dictionary = dictionaryConnector.GetDictionaryFor("TotalPriceDetails")
            });
            return multipartialResult;
        }

        public ActionResult SaveImagePreferencesCalc(string preferencesSet = null)
        {
            UploadedImage img = GetCalcImageFromSession();
            if (!string.IsNullOrWhiteSpace(preferencesSet))
            {
                var prefSet = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(preferencesSet);
                img.Preferences.Clear();
                img.Testprint = UploadedImage.TestPrintOption.None;
                img.Softproof = false;

                foreach (string key in prefSet.Keys)
                {
                    if (key == "dim1")
                    {
                        decimal dim;
                        if (decimal.TryParse(prefSet[key], out dim))
                            img.Dim1Cm = dim;
                    }
                    else if (key == "dim2")
                    {
                        decimal dim;
                        if (decimal.TryParse(prefSet[key], out dim))
                            img.Dim2Cm = dim;
                    }
                    else if (key == "product")
                    {
                        if (prefSet[key] != "0")
                        {
                            img.SelectedProduct = productRepository.GetProductWithPopulatedChildren(Convert.ToInt32(prefSet[key]));
                        }
                    }
                    else if (key == "extra")
                    {
                        switch (prefSet[key])
                        {
                            case "tpa4":
                                img.Testprint = UploadedImage.TestPrintOption.A4;
                                break;
                            case "tp11":
                                img.Testprint = UploadedImage.TestPrintOption.OneOne;
                                break;
                            case "sp":
                                img.Softproof = true;
                                break;
                        }
                    }
                    else // is option
                    {
                        if (!prefSet[key].Equals("-1")) // nothing chosen
                        {
                            Option option = prefSet[key].Equals("0") ? null : productRepository.GetOption(Convert.ToInt32(prefSet[key]));
                            option.RollSizes = productRepository.GetRollSizesForOption(option.OptionID);
                            option.SheetSizes = productRepository.GetSheetSizesForOption(option.OptionID);
                            img.Preferences[Convert.ToInt32(key)] = option;
                        }
                    }
                }
                CalculateImagePrice(img, false);
                UpdateCalcImageInSession(img);
            }
            MultipartialResult multipartialResult = new MultipartialResult(this);
            multipartialResult.AddView("PriceDetails", "phPriceDetails", new PriceDetailsViewModel { Image = img, Dictionary = dictionaryConnector.GetDictionaryFor("PriceDetails"), LanguageCode = dictionaryConnector.GetUmbracoCulture(), Context = PriceDetailsViewModel.PriceDetailsContext.Calculator });
            multipartialResult.AddContent(Business.ProductTool.Formatting.PrintAsKg(img.GetWeight()), "lblImageWeight");
            return multipartialResult;
        }

        public ActionResult ShowPrice(string orderID, UploadedImage img)
        {
            return View("ShowPrice", new PriceWeightViewModel
            {
                Image = img,
                ImagePrice = img.GetTotalPrice(),
                TotalPrice = CalculateTotalPrice(),
                ImageWeight = img.GetWeight(),
                TotalWeight = CalculateTotalWeight(),
                Dictionary = dictionaryConnector.GetDictionaryFor("ShowPrice")
            });
        }

        private Business.Authentication.AuthenticUser GetUserFromSession()
        {
            if (Session["AuthenticUser"] == null)
            {
                string currentUsername = User.Identity.Name;
                if (!string.IsNullOrWhiteSpace(currentUsername))
                {
                    Business.Authentication.AuthenticUser currentUser = UserManager.FindByName(currentUsername);
                    Session["AuthenticUser"] = currentUser;
                    return currentUser;
                }
                return null;
            }
            return (Business.Authentication.AuthenticUser)Session["AuthenticUser"];
        }

        private void SaveUser(Business.Authentication.AuthenticUser user)
        {
            UserManager.Update(user);
            Session["AuthenticUser"] = user;
        }

        public ActionResult Finishing(string orderID, UploadedImage image, List<ProductFamily> productFamilies)
        {
            FinishingModel model = new FinishingModel
            {
                OrderID = orderID,
                Image = image,
                ProductFamilies = productFamilies,
                Dictionary = dictionaryConnector.GetDictionaryFor("Finishing"),
                LanguageCode = dictionaryConnector.GetUmbracoCulture()
            };
            return View(model);
        }


        public ActionResult PriceDetails(UploadedImage img, string orderID)
        {
            return View(new PriceDetailsViewModel { Image = img, OrderID = orderID, Dictionary = dictionaryConnector.GetDictionaryFor("PriceDetails"), LanguageCode = dictionaryConnector.GetUmbracoCulture(), Context = PriceDetailsViewModel.PriceDetailsContext.Order });
        }

        public ActionResult TotalPriceDetailsRow(UploadedImage img, string orderID)
        {
            return View(new TotalPriceDetailsRowViewModel { Image = img, OrderID = orderID, Dictionary = dictionaryConnector.GetDictionaryFor("TotalPriceDetailsRow"), LanguageCode = dictionaryConnector.GetUmbracoCulture() });
        }
        public ActionResult TotalPriceDetailsCheckoutProductRow(CheckoutProduct checkoutProduct)
        {
            return View(new CheckoutProductViewModel { CheckoutProduct = checkoutProduct, Dictionary = dictionaryConnector.GetDictionaryFor("TotalPriceDetailsCheckoutProductRow") });
        }

        public ActionResult OrderSummary(string invoiceCountry, bool vatValid)
        {
            return View("OrderSummary", new OrderSummaryViewModel { Order = GetAuthenticOrder(), Dictionary = dictionaryConnector.GetDictionaryFor("OrderSummary"), InvoiceCountry = invoiceCountry, VatValid = vatValid });
        }

        public ActionResult TotalPriceDetails(bool checkoutPageView = false)
        {
            AuthenticOrder order = GetAuthenticOrder();
            return View("TotalPriceDetails",
                new TotalPriceDetailsViewModel
                {
                    Order = order,
                    CheckoutPageView = checkoutPageView,
                    Dictionary = dictionaryConnector.GetDictionaryFor("TotalPriceDetails")
                });
        }

        public string GetProcessedImagesAndTotalImageCount(string[] alreadyProcessed)
        {
            AuthenticOrder order = GetAuthenticOrder();
            string processedImagesJson = null;
            List<UploadedImage> uploadedImages = order.Images;

            //if (candidates != null && candidates.Length > 0)
            //{
            if (alreadyProcessed == null)
            {
                alreadyProcessed = new string[] { };
            }
            processedImagesJson = dbConnector.GetProcessedImagesJson(order.OrderID, alreadyProcessed);
            JObject processedImgInfo = JsonConvert.DeserializeObject<JObject>(processedImagesJson);

            if (!string.IsNullOrWhiteSpace(processedImagesJson) && !processedImagesJson.Equals("[]"))
            {
                foreach (var imgInfoStringKeyValuePair in processedImgInfo)
                {
                    string imgInfoString = imgInfoStringKeyValuePair.Value.Value<string>();

                    JObject imgInfo = JObject.Parse(imgInfoString);
                    string id = imgInfo.SelectToken("id").ToString();
                    UploadedImage imgToComplete = uploadedImages.FirstOrDefault(img => img.ID == id);
                    if (imgToComplete != null)
                    {
                        int thumbH;
                        if (int.TryParse(imgInfo.SelectToken("thumbheight").ToString(), out thumbH))
                        {
                            imgToComplete.ThumbHeight = thumbH;
                        }
                        int thumbW;
                        if (int.TryParse(imgInfo.SelectToken("thumbwidth").ToString(), out thumbW))
                        {
                            imgToComplete.ThumbWidth = thumbW;
                        }

                        bool dimensionsUpdated = false;

                        int origDim1;
                        if (int.TryParse(imgInfo.SelectToken("origdim1").ToString(), out origDim1))
                        {
                            if (imgToComplete.OrigDim1 != origDim1)
                            {
                                dimensionsUpdated = true;
                                imgToComplete.OrigDim1 = origDim1;
                                imgToComplete.Dim1 = origDim1;
                            }
                        }
                        int origDim2;
                        if (int.TryParse(imgInfo.SelectToken("origdim2").ToString(), out origDim2))
                        {
                            if (imgToComplete.OrigDim2 != origDim2)
                            {
                                dimensionsUpdated = true;
                                imgToComplete.OrigDim2 = origDim2;
                                imgToComplete.Dim2 = origDim2;
                            }
                        }
                        int origResolution;
                        if (int.TryParse(imgInfo.SelectToken("origresolution").ToString(), out origResolution))
                        {
                            if (imgToComplete.OrigXResolution != origResolution)
                            {
                                dimensionsUpdated = true;
                                imgToComplete.OrigXResolution = origResolution;
                                imgToComplete.XResolution = origResolution;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(imgInfo.SelectToken("originalfilename").ToString()))
                        {
                            imgToComplete.OriginalFilename = imgInfo.SelectToken("originalfilename").ToString();
                        }
                        if (!string.IsNullOrWhiteSpace(imgInfo.SelectToken("filenameannex").ToString()))
                        {
                            imgToComplete.FilenameAnnex = imgInfo.SelectToken("filenameannex").ToString();
                        }

                        if (dimensionsUpdated)
                        {
                            imgToComplete.CalculatePrice();
                        }
                        imgToComplete.IsProcessed = true;
                    }
                }
                //}
                //processedImgInfo["totalImageCount"] = Convert.ToString(uploadedImages.Count);
                processedImagesJson = JsonConvert.SerializeObject(processedImgInfo);
            }
            return processedImagesJson;
        }


        public ActionResult InitializePreferences(string imgInfoJson, string preselectedImageID)
        {
            //OLD
            AuthenticOrder order = GetAuthenticOrder();
            List<UploadedImage> uploadedImages = dbConnector.GetImagesInOrder(order.OrderID); // fetch from db; image processor might have changed the status of some images

            bool containsNewImages = imgInfoJson != null;

            if (containsNewImages)
            {
                List<UploadedImage> newImages = ComposeImagesFromJson(imgInfoJson);
                AddImages(newImages);
                //TODO: (old) Hier breakpoint -> GetImagesInSession zonder force db fetch bevat niet de nieuwe images? (back in ef days, test again?)
            }

            if (uploadedImages.Count > 0)
            {
                return PartialView("Preferences", new PreferencesModel
                {
                    OrderID = order.OrderID,
                    ProductFamilies = productRepository.GetProductFamiliesWithChildListing(),
                    UploadedImages = uploadedImages,
                    ActiveImageID = string.IsNullOrWhiteSpace(preselectedImageID) ? uploadedImages.First().ID : preselectedImageID,
                    Dictionary = dictionaryConnector.GetDictionaryFor("Preferences"),
                });
            }
            else
            {
                return new EmptyResult();
            }

        }

        private List<UploadedImage> ComposeImagesFromJson(string imgInfoJson)
        {
            AuthenticOrder order = GetAuthenticOrder();
            List<UploadedImage> uploadedImages = new List<UploadedImage>();

            JObject imgInfo = JObject.Parse(imgInfoJson);
            foreach (JToken child in imgInfo.Children()) // foreach image
            {
                UploadedImage uploadedImage = new UploadedImage();

                foreach (JToken grandChild in child)
                {
                    string id = grandChild.SelectToken("id").ToString();
                    string originalFilename = grandChild.SelectToken("originalfilename").ToString();
                    string workname = grandChild.SelectToken("workname").ToString();
                    string width = grandChild.SelectToken("width").ToString();
                    string height = grandChild.SelectToken("height").ToString();
                    string xresolution = grandChild.SelectToken("xresolution").ToString();

                    string thumbwidth = (grandChild.SelectToken("thumbwidth") != null) ? grandChild.SelectToken("thumbwidth").ToString() : "400";
                    string thumbheight = (grandChild.SelectToken("thumbheight") != null) ? grandChild.SelectToken("thumbheight").ToString() : "300";

                    uploadedImage.ID = id;

                    uploadedImage.OriginalFilename = originalFilename;
                    uploadedImage.ThumbWidth = Convert.ToInt32(thumbwidth);
                    uploadedImage.ThumbHeight = Convert.ToInt32(thumbheight);
                    uploadedImage.DateAdded = DateTime.Now;
                    uploadedImage.OrderID = order.OrderID;

                    if (!string.IsNullOrWhiteSpace(width))
                    {
                        uploadedImage.DimPx1 = Convert.ToInt32(width);
                        uploadedImage.DimPx2 = Convert.ToInt32(height);
                        uploadedImage.OrigXResolution = Convert.ToInt32(xresolution);
                        uploadedImage.OrigDim1 = pxToMm(uploadedImage.DimPx1, uploadedImage.OrigXResolution);
                        uploadedImage.OrigDim2 = pxToMm(uploadedImage.DimPx2, uploadedImage.OrigXResolution);
                        uploadedImage.XResolution = uploadedImage.OrigXResolution;
                        uploadedImage.Dim1 = uploadedImage.OrigDim1;
                        uploadedImage.Dim2 = uploadedImage.OrigDim2;
                    }

                    uploadedImages.Add(uploadedImage);
                }

            }
            return uploadedImages;
        }
        private List<CheckoutProduct> ComposeCheckoutProductsFromJson(string copInfoJson)
        {
            List<CheckoutProduct> checkoutProducts = new List<CheckoutProduct>();

            JObject copInfo = JObject.Parse(copInfoJson);
            foreach (JToken child in copInfo.Children())
            {
                foreach (JToken grandChild in child)
                {
                    string id = grandChild.SelectToken("id").ToString();
                    string qty = grandChild.SelectToken("qty").ToString();
                    CheckoutProduct checkoutProduct = productRepository.GetCheckoutProduct(int.Parse(id));
                    checkoutProduct.Qty = int.Parse(qty);
                    checkoutProducts.Add(checkoutProduct);
                }
            }
            return checkoutProducts;
        }

        private decimal pxToMm(int px, int dpi)
        {
            return Math.Round((px * 25.4m) / dpi);
        }

        private List<CheckoutProduct> GenerateCheckoutProducts(List<UploadedImage> images)
        {
            List<CheckoutProduct> coProducts = new List<CheckoutProduct>();

            List<Option> selectedOptions = new List<Option>();
            List<Option> availableOptionsToCheck = new List<Option>();
            foreach (UploadedImage image in images)
            {
                selectedOptions.AddRange(image.Preferences.Values);
                availableOptionsToCheck.AddRange(
                    image.SelectedProduct.OptionTypes.SelectMany(ot => ot.OptionCategories).SelectMany(oc => oc.Options) // All options available to the user
                    .Where(o => (!string.IsNullOrWhiteSpace(o.CheckoutProductsOnSelected)) || (!string.IsNullOrWhiteSpace(o.CheckoutProductsOnNotSelected))) // where a rule concerning checkout products has been set
                    );
            }
            foreach (Option option in availableOptionsToCheck)
            {
                string[] coProductsOnSelected = option.CheckoutProductsOnSelected != null ?
                    option.CheckoutProductsOnSelected.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries) :
                    new string[0];
                string[] coProductsOnNotSelected = option.CheckoutProductsOnNotSelected != null ?
                    option.CheckoutProductsOnNotSelected.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries) :
                    new string[0];

                if (coProductsOnSelected.Length > 0 && selectedOptions.FirstOrDefault(o => o.OptionID == option.OptionID) != null)
                {
                    foreach (string copId in coProductsOnSelected)
                    {
                        if (coProducts.FirstOrDefault(cop => cop.CheckoutProductID == int.Parse(copId)) == null)
                            coProducts.Add(productRepository.GetCheckoutProduct(int.Parse(copId)));
                    }
                }
                if (coProductsOnNotSelected.Length > 0 && selectedOptions.FirstOrDefault(o => o.OptionID == option.OptionID) == null)
                {
                    foreach (string copId in coProductsOnNotSelected)
                    {
                        if (coProducts.FirstOrDefault(cop => cop.CheckoutProductID == int.Parse(copId)) == null)
                            coProducts.Add(productRepository.GetCheckoutProduct(int.Parse(copId)));
                    }
                }
            }
            return coProducts;
        }

        public ActionResult ThumbsList(List<UploadedImage> images, string activeImageID)
        {
            return View(new ThumbsListModel { UploadedImages = images, ActiveImageID = activeImageID, Dictionary = dictionaryConnector.GetDictionaryFor("ThumbsList") });
        }

        [HttpPost]
        public ActionResult PlaceOrder()
        {
            AuthenticOrder order = GetAuthenticOrder();

            order.GeneratePublicOrderID();

            //Order.Payment = Request.Params["payment"]; // already provided via ajax call
            //Order.Delivery = Request.Params["delivery"]; // already provided via ajax call
            //Order.SelectedCheckoutProducts = ComposeCheckoutProductsFromJson(Request.Params["copinfo"]); // already provided via ajax call

            order.Reference = Request.Params["tbCustomerReference"];
            order.DiscountCode = Request.Params["tbDiscountCode"];

            Business.Authentication.AuthenticUser user = GetUserFromSession();

            user.Country = Request.Params["selectCountry"];
            string title = Request.Params["selectTitle"];
            user.Female = title.EndsWith("v");
            user.LanguageCode = title.Substring(0, 2);
            user.FirstName = Request.Params["User.FirstName"];
            user.FamilyName = Request.Params["User.FamilyName"];
            user.Address = Request.Params["User.Address"];
            user.PostalCode = Request.Params["User.PostalCode"];
            user.Place = Request.Params["User.Place"];
            DateTime bd;
            if (DateTime.TryParseExact(Request.Params["User.Birthday"],
                                   "dd-MM-yyyy",
                                   CultureInfo.InvariantCulture,
                                   DateTimeStyles.None,
                                   out bd))
            {
                user.Birthday = bd;
            }
            user.PhoneNumberCountryCode = Request.Params["User.PhoneNumberCountryCode"];
            user.PhoneNumber = Request.Params["User.PhoneNumber"];
            user.Company = Request.Params["User.Company"];
            user.VatCountryCode = Request.Params["User.VatCountryCode"];
            user.Vat = Request.Params["User.Vat"];

            Business.Authentication.AuthenticUser UserInDb = UserManager.FindByName(this.User.Identity.Name);

            if (order.Delivery.Equals("transport"))
            {
                string selDeliveryAddresses = Request.Form["selDeliveryAddresses"];

                DeliveryAddress deliveryAddress;

                switch (selDeliveryAddresses)
                {
                    case "0":
                        deliveryAddress = new DeliveryAddress
                        {
                            FirstName = Request.Form["delCustomAddress_firstName_tb"],
                            FamilyName = Request.Form["delCustomAddress_familyName_tb"],
                            Company = Request.Form["delCustomAddress_company_tb"],
                            Address = Request.Form["delCustomAddress_address_tb"],
                            PostalCode = Request.Form["delCustomAddress_postcode_tb"],
                            Place = Request.Form["delCustomAddress_place_tb"],
                            Country = Request.Form["delCustomAddress_country_tb"],
                            PhoneNumber = Request.Form["delCustomAddress_telephone_tb"],
                            Email = Request.Form["delCustomAddress_email_tb"],
                            AuthenticUserID = UserInDb.Id
                        };
                        productRepository.SaveDeliveryAddress(deliveryAddress);
                        break;
                    case "user":
                        deliveryAddress = new DeliveryAddress
                        {
                            FirstName = UserInDb.FirstName,
                            FamilyName = UserInDb.FamilyName,
                            Company = UserInDb.Company,
                            Address = UserInDb.Address,
                            PostalCode = UserInDb.PostalCode,
                            Place = UserInDb.Place,
                            Country = UserInDb.Country,
                            PhoneNumber = UserInDb.PhoneNumber,
                            Email = UserInDb.Email,
                            AuthenticUserID = UserInDb.Id
                        };
                        break;
                    default:
                        int id;
                        if (int.TryParse(selDeliveryAddresses, out id))
                        {
                            deliveryAddress = productRepository.GetDeliveryAddress(id);
                        }
                        else
                        {
                            deliveryAddress = new DeliveryAddress(); // ?
                        }
                        break;
                }
                order.DeliveryAddress = deliveryAddress;
            }

            UserInDb.UpdateWithNewData(user);
            SaveUser(UserInDb);

            string OrderTableHtml = Request.Params["tblorderhtml"];
            if (!string.IsNullOrWhiteSpace(OrderTableHtml))
            {
                OrderTableHtml = HttpUtility.UrlDecode(OrderTableHtml);
            }

            SendOrderConfirmationMail(order, OrderTableHtml, user);

            dbConnector.UpdateOrderOnSubmit(order, user.Id);
            AuthenticOrder newOrder = dbConnector.AddOrder();
            SetAuthenticOrder(newOrder);

            return PartialView("Thankyou", new ThankyouViewModel
            {
                Dictionary = dictionaryConnector.GetDictionaryFor("Thankyou")
            });

        }

        public void SendOrderConfirmationMail(AuthenticOrder Order, string OrderTableHtml, Business.Authentication.AuthenticUser user)
        {
            EmailService service = new EmailService();
            IdentityMessage message = new IdentityMessage();

            List<string> attachedImagePaths = new List<string>();
            foreach (UploadedImage image in Order.Images)
            {
                string path = image.GetThumbSource();
                if (!attachedImagePaths.Contains(path))
                {
                    attachedImagePaths.Add(path);
                }
            }

            message.Body = PopulateConfirmationEmail(Order, GetHtmlTemplateFromSession("OrderConfirmationMail_" + dictionaryConnector.GetUmbracoCulture()), OrderTableHtml, attachedImagePaths.ToArray(), user);
            message.Body += AAccountController.ConstructCommercialFooter();
            message.Destination = user.Email;
            message.Subject = GetHtmlTemplateFromSession("OrderConfirmationMail_subject_" + dictionaryConnector.GetUmbracoCulture());

            List<string> attachedImageServerPaths = new List<string>();
            foreach (string attachedImagePath in attachedImagePaths)
            {
                attachedImageServerPaths.Add(Server.MapPath(attachedImagePath));
            }

            service.Send(message, attachedImageServerPaths.ToArray());
        }

        public string PopulateConfirmationEmail(AuthenticOrder Order, string html, string orderTableHtml, string[] imagePaths, Business.Authentication.AuthenticUser user)
        {
            html = html.Replace("&amp;ORDERNUMBER&amp;", Order.PublicOrderID);
            html = html.Replace("&amp;PAYMENTOPTION&amp;", Order.Payment); //TODO: readable (now values online, banktransfer, cash)
            html = html.Replace("&amp;DELIVERYOPTION&amp;", Order.Delivery); //TODO: readable (now values pickup, transport, owntransport)

            string extraInfoRow = "";

            if (!string.IsNullOrWhiteSpace(Order.DiscountCode) || !string.IsNullOrWhiteSpace(Order.Reference))
            {
                extraInfoRow = "<table style='width: 100%'>";
                if (!string.IsNullOrWhiteSpace(Order.DiscountCode))
                {
                    extraInfoRow += "<tr><td width='25%'>" + dictionaryConnector.GetTranslation("Checkout.DiscountCode") + "</td><td>" + Order.DiscountCode + "</td></tr>";
                }
                if (!string.IsNullOrWhiteSpace(Order.Reference))
                {
                    extraInfoRow += "<tr><td width='25%'>" + dictionaryConnector.GetTranslation("Checkout.Reference") + "</td><td>" + Order.Reference + "</td></tr>";
                }
                extraInfoRow += "</table>";
            }

            html = html.Replace("&amp;EXTRAINFOROW&amp;", extraInfoRow);

            foreach (string imagePath in imagePaths)
            {
                string cid = Path.GetFileNameWithoutExtension(imagePath);
                orderTableHtml = orderTableHtml.Replace(imagePath, "cid:" + cid);
            }

            html = html.Replace("&amp;ORDER_SUMMARY&amp;", "<h1 style='font-size: 12px'>Samenvatting</h1>" + Order.GenerateSummaryHtml());
            html = html.Replace("&amp;ORDER_RECAP&amp;", "<h1 style='font-size: 12px'>Overzicht</h1>" + orderTableHtml);
            html = html.Replace("&amp;USER_DATA&amp;", "<h1 style='font-size: 12px'>Uw gegevens</h1>" + user.GenerateInfoTableHtml());

            string deliveryAddressHtml = "";
            if (Order.Delivery.Equals("transport"))
            {
                deliveryAddressHtml = "<h1 style='font-size: 12px'>Leveringsadres</h1>" + Order.DeliveryAddress.GenerateInfoTableHtml();
            }
            html = html.Replace("&amp;DELIVERY_ADDRESS&amp;", deliveryAddressHtml);
            return html;
        }

        public ActionResult CheckoutDelivery(OrderStep3Model model)
        {
            return View(model);
        }

        public ActionResult CheckoutOrderPreferences(OrderStep3Model model)
        {
            return View("CheckoutOrderPreferences", model);
        }

        public ActionResult CheckVat(string vatCC, string vatNr, bool returnSummary)
        {
            checkVatPortTypeClient req = new checkVatPortTypeClient();
            bool valid;
            string companyName;
            string companyAddress;
            vatNr = cleanVat(vatNr);
            req.checkVat(ref vatCC, ref vatNr, out valid, out companyName, out companyAddress);
            if (returnSummary)
            {
                Business.Authentication.AuthenticUser user = GetUserFromSession();
                user = UserManager.FindById(user.Id);
                user.VatValid = valid;
                SaveUser(user);
                return OrderSummary(vatCC, valid);
            }
            return Content(valid ? "1" : "0");
        }
        private string cleanVat(string vat)
        {
            return new string(vat.Where(c => char.IsLetterOrDigit(c)).ToArray());
        }

        public ActionResult UpdateOrderWithCheckoutProducts(string copInfoJson)
        {
            AuthenticOrder order = GetAuthenticOrder();
            List<CheckoutProduct> selectedCheckoutProducts = ComposeCheckoutProductsFromJson(copInfoJson);
            // save to db
            dbConnector.UpdateOrderWithCheckoutProducts(order.OrderID, selectedCheckoutProducts);
            // save in net session object
            order.SelectedCheckoutProducts = selectedCheckoutProducts;
            return TotalPriceDetails(false);
        }
        public ActionResult UpdateOrderWithDeliveryOption(string delivery)
        {
            AuthenticOrder order = GetAuthenticOrder();
            // save to db
            dbConnector.UpdateOrderWithDeliveryOption(order.OrderID, delivery);
            // save in net session object            
            order.Delivery = delivery;

            string vatCountryCode = "BE";
            Business.Authentication.AuthenticUser user = GetUserFromSession();
            bool vatValid = false;
            if (user != null)
            {
                vatCountryCode = user.VatCountryCode;
                vatValid = user.VatValid;
            }
            return OrderSummary(vatCountryCode, vatValid);
        }
        public ActionResult UpdateOrderWithPaymentOption(string orderID, string payment)
        {
            // save to db
            dbConnector.UpdateOrderWithPaymentOption(orderID, payment);
            // save in net session object
            AuthenticOrder order = GetAuthenticOrder();
            order.Payment = payment;

            string vatCountryCode = "BE";
            Business.Authentication.AuthenticUser user = GetUserFromSession();
            bool vatValid = false;
            if (user != null)
            {
                vatCountryCode = user.VatCountryCode;
                vatValid = user.VatValid;
            }
            return OrderSummary(vatCountryCode, vatValid);
        }


        public ActionResult CompletedAccount(
                string User_Id,
                string User_Title,
                string User_FirstName,
                string User_FamilyName,
                string User_Address,
                string User_PostalCode,
                string User_Place,
                string user_Country,
                string user_Birthday,
                string user_PhoneNumberCountryCode,
                string user_PhoneNumber,
                string user_MobileNumberCountryCode,
                string user_MobileNumber,
                string User_Company,
                string User_VatCC,
                string user_VatNr
            )
        {
            Business.Authentication.AuthenticUser user = UserManager.FindById(long.Parse(User_Id));

            // user_title values: nlm, frm, enm, nlv, frv, env
            user.Female = User_Title.EndsWith("v");
            user.LanguageCode = User_Title.Substring(0, 2);
            user.FirstName = User_FirstName;
            user.FamilyName = User_FamilyName;
            user.Address = User_Address;
            user.PostalCode = User_PostalCode;
            user.Place = User_Place;
            user.Country = user_Country;

            DateTime birthday;
            if (DateTime.TryParse(user_Birthday, out birthday))
            {
                user.Birthday = birthday;
            }

            user.PhoneNumberCountryCode = string.IsNullOrWhiteSpace(user_PhoneNumberCountryCode) ? "+32" : user_PhoneNumberCountryCode;
            user.PhoneNumber = user_PhoneNumber;
            user.MobileNumberCountryCode = string.IsNullOrWhiteSpace(user_MobileNumberCountryCode) ? "+32" : user_MobileNumberCountryCode;
            user.MobileNumber = user_MobileNumber;
            user.Company = User_Company;
            user.VatCountryCode = User_VatCC;
            user.Vat = user_VatNr;

            SaveUser(user);

            return CheckoutOrderPreferences(constructOrderStep3Model());
        }

        public MultipartialResult DeleteImageFromOrder(string imgID)
        {
            // save to db
            dbConnector.DeleteImage(imgID);

            // save in net session object
            AuthenticOrder order = GetAuthenticOrder();
            order.Images = order.Images.Where(img => img.ID != imgID).ToList();

            MultipartialResult multipartialResult = new MultipartialResult(this);
            multipartialResult.AddView("TotalPriceDetails", "totalPriceRecap",
                    new TotalPriceDetailsViewModel
                    {
                        Order = order,
                        Dictionary = dictionaryConnector.GetDictionaryFor("TotalPriceDetails")
                    });
            multipartialResult.AddContent(Business.ProductTool.Formatting.PrintAsEuro(CalculateTotalPrice()), "lblTotalPrice");
            multipartialResult.AddContent(Business.ProductTool.Formatting.PrintAsKg(CalculateTotalWeight()), "lblTotalWeight");

            return multipartialResult;
        }

        private void AddImages(List<UploadedImage> imagesToSave)
        {
            // save it to db
            dbConnector.AddImages(imagesToSave);

            // save it in session object
            GetAuthenticOrder().Images.AddRange(imagesToSave);
        }

        private void UpdateImageInOrder(UploadedImage uploadedImage)
        {
            // save it to db
            dbConnector.SaveImage(uploadedImage);

            // save it in session object
            AuthenticOrder order = GetAuthenticOrder();
            for (int i = 0; i < order.Images.Count; i++)
            {
                if (order.Images[i].ID == uploadedImage.ID)
                {
                    order.Images[i] = uploadedImage;
                    break;
                }
            }
        }

        public UploadedImage GetCalcImageFromSession()
        {
            if (Session["CalcImage"] != null)
            {
                return (UploadedImage)Session["CalcImage"];
            }
            else
            {
                UploadedImage CalcImage = UploadedImage.CreateCalcImage();
                Session["CalcImage"] = CalcImage;
                return CalcImage;
            }
        }

        private void UpdateCalcImageInSession(UploadedImage img)
        {
            Session["CalcImage"] = img;
        }

        public AuthenticOrder GetAuthenticOrder()
        {
            return (AuthenticOrder)Session["AuthenticOrder"];
        }
        public void SetAuthenticOrder(AuthenticOrder order)
        {
            Session["AuthenticOrder"] = order;
        }

        //public Order GetFirstOrderInSession(string sessionID, bool forceDbFetch)
        //{
        //    if (forceDbFetch || Session["Order"] == null)
        //    {
        //        Session["Order"] = dbConnector.GetOrdersInSession(sessionID).First();
        //    }
        //    return (Order)Session["Order"];
        //}

        public ActionResult OrderStep1()
        {
            return View(new OrderStep1Model { AuthenticOrder = CheckAuthenticOrder() });
        }
        public ActionResult OrderStep2(string preselectedImageID)
        {
            AuthenticOrder order = CheckAuthenticOrder();
            order.Images = dbConnector.GetImagesInOrder(order.OrderID);
            return PartialView("OrderStep2", new OrderStep2Model
            {
                AuthenticOrder = order,
                ProductFamilies = order.Images.Count > 0 ? productRepository.GetProductFamiliesWithChildListing() : new List<ProductFamily>(),
                ActiveImageID = order.Images.Count > 0 && string.IsNullOrWhiteSpace(preselectedImageID) ? order.Images.First().ID : preselectedImageID,
                Dictionary = order.Images.Count > 0 ? dictionaryConnector.GetDictionaryFor("OrderStep2") : new Dictionary<string, TranslatedString>()
            });
        }
        public ActionResult OrderStep3()
        {
            return PartialView("OrderStep3", constructOrderStep3Model());
        }

        public OrderStep3Model constructOrderStep3Model()
        {
            AuthenticOrder order = CheckAuthenticOrder();
            order.Images = dbConnector.GetImagesInOrder(order.OrderID);

            Business.Authentication.AuthenticUser currentUser = GetUserFromSession();
            currentUser.DeliveryAddresses = productRepository.GetDeliveryAddresses((int)currentUser.Id).ToList();
            return new OrderStep3Model
            {
                User = currentUser,
                AuthenticOrder = order,
                AvailableCheckoutProducts = GenerateCheckoutProducts(order.Images),
                Dictionary = dictionaryConnector.GetDictionaryFor("Checkout"),
                LanguageCode = dictionaryConnector.GetUmbracoCulture()
            };
        }

        public ActionResult Calculator()
        {
            return View();
        }
        public ActionResult ShowCalculator()
        {
            if (Session["UmbracoHomepage"] == null)
            {
                Session["UmbracoHomepage"] = this.CurrentPage.AncestorOrSelf("Homepage");
            }
            return View("ShowCalculator", new FinishingModel
            {
                OrderID = "",
                Image = GetCalcImageFromSession(),
                ProductFamilies = productRepository.GetProductFamiliesWithChildListing(),
                Dictionary = dictionaryConnector.GetDictionaryFor("Finishing"),
                LanguageCode = dictionaryConnector.GetUmbracoCulture(),
            });
        }
    }
}
