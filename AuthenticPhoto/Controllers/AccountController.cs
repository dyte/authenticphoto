﻿namespace AuthenticPhoto.Controllers
{
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Business.Authentication;
    using Models;
    using Umbraco.Web.Mvc;
    using System;
    using System.Collections.Generic;
    using Business.ProductTool;
    using System.Security.Principal;
    using System.Web.Security;
    using System.Web.Configuration;
    using System.Data.Entity.Validation;
    using System.Globalization;

    [Authorize]
    public class AAccountController : SurfaceController
    {
        #region constants

        private const string XsrfKey = "XsrfId";

        #endregion

        #region member vars 

        private AuthenticUserManager _userManager;
        private DictionaryConnector dictionaryConnector;


        #endregion

        #region constructors and destructors

        public AAccountController()
        {
            dictionaryConnector = new DictionaryConnector(System.Web.HttpContext.Current);
        }

        public AAccountController(AuthenticUserManager userManager)
        {
            UserManager = userManager;
            dictionaryConnector = new DictionaryConnector(System.Web.HttpContext.Current);
        }

        #endregion

        #region enums

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        #endregion

        #region properties

        public AuthenticUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<AuthenticUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        #endregion

        #region methods

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(long userId, string code)
        {
            if (userId == 0 || code == null)
            {
                return View("Error");
            }
            string decodedCode = code.Contains(" ") ? code.Replace(" ", "+") : code;
            var result = await UserManager.ConfirmEmailAsync(userId, decodedCode);            
            string websiteRoot = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(userId);                
                await SignInAsync(user, false);
                Session["AuthenticUser"] = user;
                Session["msg"] = dictionaryConnector.GetTranslation("Account.Activation.Success");
                return Redirect(websiteRoot);
            }
            //AddErrors(result);
            Session["msg"] = dictionaryConnector.GetTranslation("Account.Activation.Error");
            return Redirect(websiteRoot);
        }


        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return PartialView(new ForgotPasswordViewModel { Dictionary = dictionaryConnector.GetDictionaryFor("Account") });
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    ModelState.AddModelError("", "The user either does not exist or is not confirmed.");
                    return View(new ForgotPasswordViewModel { Dictionary = dictionaryConnector.GetDictionaryFor("Account") });
                }

                //For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                //Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                string confirmEmailUrl = string.Format("{0}://{1}{2}/{3}/{4}/{5}/{6}?userId={7}&code={8}",
                        Request.Url.Scheme, Request.Url.Authority, Url.Content("~"), "umbraco", "Surface", "AAccount", "ShowResetPassword", user.Id, code);

                string mailBody = PopulateForgotPasswordEmail(GetHtmlTemplateFromSession("ForgotPassword_nl"), confirmEmailUrl);
                mailBody += ConstructCommercialFooter();
                await UserManager.SendEmailAsync(user.Id, GetHtmlTemplateFromSession("ForgotPassword_subject_nl"), mailBody);

                return Content("<span id='lblSuccess'></span>");
            }

            // If we got this far, something failed, redisplay form
            model.Dictionary = dictionaryConnector.GetDictionaryFor("Account");
            return PartialView(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView(new LoginViewModel { Dictionary = dictionaryConnector.GetDictionaryFor("Account") });
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.Email, model.Password);
                bool inCheckoutFlow = !string.IsNullOrWhiteSpace(Request.Params["incheckoutflow"]);
                if (user != null && user.IsRegistered)
                {
                    await SignInAsync(user, model.RememberMe);
                    if (user.EmailConfirmed)
                    {
                        Session["AuthenticUser"] = user;
                        return Content("<span id='lblSuccess'>" + dictionaryConnector.GetTranslation("Account.LoginSuccesful") + "</span>" + (inCheckoutFlow ? "<input type='hidden' name='incheckoutflow'/>":""));
                    }
                    else
                    {
                        Request.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);
                        ModelState.AddModelError("", dictionaryConnector.GetTranslation("Account.NotActivated"));
                        ViewBag.ConstructResendActivationLink = true;
                        ViewBag.UserId = user.Id;
                    }
                }
                else
                {
                    ModelState.AddModelError("", dictionaryConnector.GetTranslation("Account.InvalidLogin"));
                }
            }

            // If we got this far, something failed, redisplay form
            model.Dictionary = dictionaryConnector.GetDictionaryFor("Account");
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult EditUserForm(string UserID)
        {
            var user = UserManager.FindById(long.Parse(UserID));
            return View("Edit", new EditUserViewModel { User = user, ConfirmRequired = false, Dictionary = dictionaryConnector.GetDictionaryFor("Account"), CheckoutContext = false });
        }

        public ActionResult Edit(AuthenticUser user, bool checkoutContext)
        {
            return View(new EditUserViewModel { User = user, ConfirmRequired = false, Dictionary = dictionaryConnector.GetDictionaryFor("Account"), ActiveCulture = dictionaryConnector.GetUmbracoCulture(), CheckoutContext = checkoutContext });
        }

        [HttpPost]
        public ActionResult EditUser(EditUserViewModel model)
        {
            model.User.Female = !string.IsNullOrEmpty(model.User.Title) && model.User.Title.Equals("v");
            model.User.Country = Request.Params["selectCountry"];
            model.User.Title = Request.Params["selectTitle"];
            model.User.LanguageCode = model.User.Title.Substring(0, 2);

            AuthenticUser UserInDb = UserManager.FindByName(this.User.Identity.Name);
            UserInDb.UpdateWithNewData(model.User);
            
            if(model.User.Password != "********")
            {
                UserInDb.Password = model.User.Password;
                UserInDb.ConfirmPassword = model.User.ConfirmPassword;
            }

            var result = UserManager.Update(UserInDb);
            if (result.Succeeded)
            {
                return View("Edit", new EditUserViewModel { User = UserInDb, ConfirmRequired = false, FeedbackMessage = dictionaryConnector.GetTranslation("Account.InformationUpdated"), Dictionary = dictionaryConnector.GetDictionaryFor("Account"), CheckoutContext = model.CheckoutContext });
            }

            return View("Edit", new EditUserViewModel { User = model.User, ConfirmRequired = false, Dictionary = dictionaryConnector.GetDictionaryFor("Account"), CheckoutContext = false });

            // If we got this far, something failed, redisplay form
            //return View("Edit", new EditUserViewModel { User = model.User, ConfirmRequired = false, Dictionary = dictionaryConnector.GetDictionaryFor("Account"), CheckoutContext = false });

            //User.Country = Request.Params["selectCountry"];
            //string title = Request.Params["selectTitle"];
            //User.Female = title.EndsWith("v");
            //User.LanguageCode = title.Substring(0, 2);
            //AuthenticUser UserInDb = UserManager.FindByName(this.User.Identity.Name);
            //UserInDb.UpdateWithNewData(User);
            //SaveUser(UserInDb);
            //return View("Edit", new EditUserViewModel { User = User, ConfirmRequired = false, FeedbackMessage = dictionaryConnector.GetTranslation("Account.InformationUpdated"), Dictionary = dictionaryConnector.GetDictionaryFor("Account"), CheckoutContext = model.CheckoutContext });
        }

        private void SaveUser(Business.Authentication.AuthenticUser user)
        {
            UserManager.Update(user);
            Session["AuthenticUser"] = user;
        }

        [AllowAnonymous]
        public ActionResult QuickRegister()
        {
            return PartialView(new QuickRegisterViewModel { User = new AuthenticUser(), Dictionary = dictionaryConnector.GetDictionaryFor("Account") });
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View(new AuthenticUser());
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> QuickRegister(QuickRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                AuthenticUser user = model.User;
                var existingUser = await UserManager.FindByEmailAsync(model.User.Email);
                bool newUser = true;

                if(existingUser != null)
                {
                    if(string.IsNullOrWhiteSpace(existingUser.PasswordHash)) // = no password set; user was already in DB but not properly registered (newsletter, ..)
                    {
                        user = existingUser;
                        newUser = false;
                    }
                }

                user.UserName = model.User.Email;
                user.FirstName = model.FirstName;
                user.FamilyName = model.FamilyName;
                user.Address = model.Address;
                user.PostalCode = model.PostalCode;
                user.Place = model.Place;
                

                user.Female = !string.IsNullOrEmpty(model.User.Title) && model.User.Title.Equals("v");
                user.IsRegistered = true;
                user.Country = Request.Params["selectCountry"];
                DateTime bd;
                if (DateTime.TryParseExact(Request.Params["User.Birthday"],
                                       "dd-MM-yyyy",
                                       CultureInfo.InvariantCulture,
                                       DateTimeStyles.None,
                                       out bd))
                {
                    user.Birthday = bd;
                }
                user.Company = Request.Params["User.Company"];
                user.Title = Request.Params["selectTitle"];
                user.PhoneNumberCountryCode = Request.Params["User.PhoneNumberCountryCode"];
                user.PhoneNumber = Request.Params["User.PhoneNumber"];
                user.MobileNumberCountryCode = Request.Params["User.MobileNumberCountryCode"];
                user.MobileNumber = Request.Params["User.MobileNumber"];

                IdentityResult result;

                if(newUser)
                {
                    user.ContactPermission = model.User.ContactPermission; // don't change ContactPermission if user isn't new; if user already subscribed to newsletter, need explicit unsubscribe
                    user.Source = "registration ";
                    result = await UserManager.CreateAsync(user, model.User.Password);
                } else
                {
                    user.Source += "registration ";
                    await UserManager.AddPasswordAsync(user.Id, model.User.Password);
                    result = await UserManager.UpdateAsync(user);
                } 
                if (result.Succeeded)
                {
                    await SendConfirmationCodeEmail(user.Id);
                    return Content("<span id='lblSuccess'>" + dictionaryConnector.GetTranslation("Account.Registration.Success") + "</span>");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(new QuickRegisterViewModel { User = model.User, Dictionary = dictionaryConnector.GetDictionaryFor("Account") });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResendConfirmationCodeEmail(long userId)
        {
            await SendConfirmationCodeEmail(userId);
            return View("Login", new LoginViewModel { Dictionary = dictionaryConnector.GetDictionaryFor("Account") });
        }

        private async Task SendConfirmationCodeEmail(long userId)
        {
            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            // Send an email with this link
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userId);

            string confirmEmailUrl = string.Format("{0}://{1}{2}/{3}/{4}/{5}/{6}?userId={7}&code={8}",
                Request.Url.Scheme, Request.Url.Authority, Url.Content("~"), "umbraco", "Surface", "AAccount", "ConfirmEmail", userId, code);
            try
            {
                string mailBody = PopulateRegistrationConfirmationEmail(GetHtmlTemplateFromSession("RegistrationConfirmation_nl"), confirmEmailUrl);
                mailBody += ConstructCommercialFooter();
                await UserManager.SendEmailAsync(userId, dictionaryConnector.GetTranslation("Account.ConfirmYourAccount"), mailBody);
            }
            catch (Exception)
            {// TODO: LOG!!
                ModelState.AddModelError("", dictionaryConnector.GetTranslation("Account.Registration.Error"));
            }
        }

        public static string ConstructCommercialFooter()
        {
            Dictionary<string, TranslatedString> dict = new DictionaryConnector(System.Web.HttpContext.Current).GetDictionaryFor("MailFooter");
            return
                "<p style='color: #2e3a88; margin-top: 50px'>" + dict["Email.Footer.SentBy"].Value + "</p>" +
                "<p>" + "<span style='color: #c5000f; font-size: 1.2em'>" + dict["Company.FullName"].Value + "</span><br/><span style='color: #2e3a88; font-size: 1.1em'>" + dict["Company.ProductsOverview"].Value + "</span>" + "</p>" +
                "<p>" +  "<span>" + dict["Company.StreetNr"].Value + "</span><br/><span>" + dict["Company.CityPostalCode"].Value  + " - " + dict["Company.Country"].Value + "</span><br/><span>Tel: " + dict["Company.TelephoneCommercial"].Value + "</span><br/><a href='mailto:commercial@AuthenticPhoto.be'>commercial@AuthenticPhoto.be</a><br/><a href='http://www.AuthenticPhoto.be'>www.AuthenticPhoto.be</a>" + "</p>";

        }

        private string PopulateRegistrationConfirmationEmail(string template, string url)
        {
            return template.Replace("&amp;URL&amp;", "<a href=\"" + url + "\">" + url + "</a>");
        }

        private string PopulateForgotPasswordEmail(string template, string url)
        {
            return template.Replace("&amp;URL&amp;", "<a href=\"" + url + "\">" + url + "</a>");
        }

        private string GetHtmlTemplateFromSession(string name)
        {
            if (Session["HtmlTemplates"] == null)
            {
                return "Exception: html templates not found in session";
            }
            else
            {
                Dictionary<string, string> htmlTemplates = (Dictionary<string, string>)Session["HtmlTemplates"];
                return htmlTemplates.ContainsKey(name) ? htmlTemplates[name] : "Exception: " + name + " not found in html templates";
            }
        }

        //
        // GET: /Account/ExternalLoginFailure

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(long.Parse(User.Identity.GetUserId()));
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        [AllowAnonymous]
        public ActionResult ShowResetPassword(long userId, string code)
        {
            if (userId == 0 || code == null)
            {
                return View("Error");
            }
            string websiteRoot = string.Format("{0}://{1}{2}{3}?rp_userId={4}&rp_code={5}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"), dictionaryConnector.GetUmbracoCulture().Substring(0, 2).ToLower(), userId, code);
            Session["postbackaction"] = PostbackActions.PasswordReset;
            return Redirect(websiteRoot);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(long userId, string code)
        {
            return View(new ResetPasswordViewModel { UserId = userId, Code = code, Dictionary = dictionaryConnector.GetDictionaryFor("Account") });
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> DoResetPassword(ResetPasswordViewModel model)
        {
            if (model.Dictionary == null)
            {
                model.Dictionary = dictionaryConnector.GetDictionaryFor("Account");
            }
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(model.UserId);
                if (user == null)
                {
                    ModelState.AddModelError("", "No user found.");
                    return View();
                }
                string decodedCode = model.Code.Contains(" ") ? model.Code.Replace(" ", "+") : model.Code;
                var result = await UserManager.ResetPasswordAsync(user.Id, decodedCode, model.Password);
                string websiteRoot = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

                if (result.Succeeded)
                {
                    Session["msg"] = dictionaryConnector.GetTranslation("Account.PasswordReset");
                    return Content("<span id='lblSuccess'></span>");
                }
                AddErrors(result);
                return View("ResetPassword", model);

            }
            // If we got this far, something failed, redisplay form

            return View("ResetPassword", model);
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(long.Parse(User.Identity.GetUserId()));
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        private void SendEmail(string email, string callbackUrl, string subject, string message)
        {
            // For information on sending mail, please visit http://go.microsoft.com/fwlink/?LinkID=320771
        }

        private async Task SignInAsync(AuthenticUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(
                new AuthenticationProperties
                {
                    IsPersistent = isPersistent
                },
                await user.GenerateUserIdentityAsync(UserManager));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        //
        // GET: /Account/ForgotPassword

        //
        //// POST: /Account/Disassociate
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        //{
        //    ManageMessageId? message = null;
        //    var result = await UserManager.RemoveLoginAsync(long.Parse(User.Identity.GetUserId()), new UserLoginInfo(loginProvider, providerKey));
        //    if (result.Succeeded)
        //    {
        //        var user = await UserManager.FindByIdAsync(long.Parse(User.Identity.GetUserId()));
        //        await SignInAsync(user, false);
        //        message = ManageMessageId.RemoveLoginSuccess;
        //    }
        //    else
        //    {
        //        message = ManageMessageId.Error;
        //    }
        //    return RedirectToAction(
        //        "Manage",
        //        new
        //        {
        //            Message = message
        //        });
        //}

        //
        // GET: /Account/Manage

        //
        // POST: /Account/ExternalLogin
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult ExternalLogin(string provider, string returnUrl)
        //{
        //    // Request a redirect to the external login provider
        //    return new ChallengeResult(
        //        provider,
        //        Url.Action(
        //            "ExternalLoginCallback",
        //            "AAccount",
        //            new
        //            {
        //                ReturnUrl = returnUrl
        //            }));
        //}

        ////
        //// GET: /Account/ExternalLoginCallback
        //[AllowAnonymous]
        //public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction("Login");
        //    }

        //    // Sign in the user with this external login provider if the user already has a login
        //    var user = await UserManager.FindAsync(loginInfo.Login);
        //    if (user != null)
        //    {
        //        await SignInAsync(user, false);
        //        return RedirectToLocal(returnUrl);
        //    }
        //    // If the user does not have an account, then prompt the user to create an account
        //    ViewBag.ReturnUrl = returnUrl;
        //    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
        //    return View(
        //        "ExternalLoginConfirmation",
        //        new ExternalLoginConfirmationViewModel
        //        {
        //            Email = loginInfo.Email
        //        });
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        return RedirectToAction("Manage");
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        // Get the information about the user from the external login provider
        //        var info = await AuthenticationManager.GetExternalLoginInfoAsync();
        //        if (info == null)
        //        {
        //            return View("ExternalLoginFailure");
        //        }
        //        var user = new AuthenticUser
        //        {
        //            UserName = model.Email,
        //            Email = model.Email,
        //            IsRegistered = true
        //        };
        //        var result = await UserManager.CreateAsync(user);
        //        if (result.Succeeded)
        //        {
        //            result = await UserManager.AddLoginAsync(user.Id, info.Login);
        //            if (result.Succeeded)
        //            {
        //                await SignInAsync(user, false);

        //                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
        //                // Send an email with this link
        //                // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
        //                // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
        //                // SendEmail(user.Email, callbackUrl, "Confirm your account", "Please confirm your account by clicking this link");

        //                return RedirectToLocal(returnUrl);
        //            }
        //        }
        //        AddErrors(result);
        //    }

        //    ViewBag.ReturnUrl = returnUrl;
        //    return View(model);
        //}

        //[AllowAnonymous]
        //public ActionResult ExternalLoginFailure()
        //{
        //    return View();
        //}
        ////
        //// POST: /Account/LinkLogin
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LinkLogin(string provider)
        //{
        //    // Request a redirect to the external login provider to link a login for the current user
        //    return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        //}

        //
        // GET: /Account/LinkLoginCallback
        //public async Task<ActionResult> LinkLoginCallback()
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction(
        //            "Manage",
        //            new
        //            {
        //                Message = ManageMessageId.Error
        //            });
        //    }
        //    var result = await UserManager.AddLoginAsync(long.Parse(User.Identity.GetUserId()), loginInfo.Login);
        //    if (result.Succeeded)
        //    {
        //        return RedirectToAction("Manage");
        //    }
        //    return RedirectToAction(
        //        "Manage",
        //        new
        //        {
        //            Message = ManageMessageId.Error
        //        });
        //}

        //
        // POST: /Account/ExternalLoginConfirmation
        //public ActionResult Manage(ManageMessageId? message)
        //{
        //    ViewBag.StatusMessage = message == ManageMessageId.ChangePasswordSuccess
        //        ? "Your password has been changed."
        //        : message == ManageMessageId.SetPasswordSuccess
        //            ? "Your password has been set."
        //            : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed." : message == ManageMessageId.Error ? "An error has occurred." : "";
        //    ViewBag.HasLocalPassword = HasPassword();
        //    ViewBag.ReturnUrl = Url.Action("Manage");
        //    return View();
        //}

        ////
        //// POST: /Account/Manage
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Manage(ManageUserViewModel model)
        //{
        //    var hasPassword = HasPassword();
        //    ViewBag.HasLocalPassword = hasPassword;
        //    ViewBag.ReturnUrl = Url.Action("Manage");
        //    if (hasPassword)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            var result = await UserManager.ChangePasswordAsync(long.Parse(User.Identity.GetUserId()), model.OldPassword, model.NewPassword);
        //            if (result.Succeeded)
        //            {
        //                var user = await UserManager.FindByIdAsync(long.Parse(User.Identity.GetUserId()));
        //                await SignInAsync(user, false);
        //                return RedirectToAction(
        //                    "Manage",
        //                    new
        //                    {
        //                        Message = ManageMessageId.ChangePasswordSuccess
        //                    });
        //            }
        //            AddErrors(result);
        //        }
        //    }
        //    else
        //    {
        //        // User does not have a password so remove any validation errors caused by a missing OldPassword field
        //        var state = ModelState["OldPassword"];
        //        if (state != null)
        //        {
        //            state.Errors.Clear();
        //        }

        //        if (ModelState.IsValid)
        //        {
        //            var result = await UserManager.AddPasswordAsync(long.Parse(User.Identity.GetUserId()), model.NewPassword);
        //            if (result.Succeeded)
        //            {
        //                return RedirectToAction(
        //                    "Manage",
        //                    new
        //                    {
        //                        Message = ManageMessageId.SetPasswordSuccess
        //                    });
        //            }
        //            AddErrors(result);
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        #endregion

        private class ChallengeResult : HttpUnauthorizedResult
        {
            #region constructors and destructors

            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            #endregion

            #region properties

            public string LoginProvider { get; set; }

            public string RedirectUri { get; set; }

            public string UserId { get; set; }

            #endregion

            #region methods

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties
                {
                    RedirectUri = RedirectUri
                };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);

            }

            #endregion
        }
    }
}