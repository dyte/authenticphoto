﻿namespace AuthenticPhoto.Data.Synchronization.Results
{
    public class ProductFamilySynchronizationResult : SynchronizationResult
    {
        public ProductFamily ProductFamily { get; set; }
        public int DocumentId { get; set; }
    }
}