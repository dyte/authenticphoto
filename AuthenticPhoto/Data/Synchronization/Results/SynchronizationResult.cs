﻿using System.Collections.Generic;

namespace AuthenticPhoto.Data.Synchronization.Results
{
    public abstract class SynchronizationResult
    {
        public bool Success { get; set; }
        public string ResultCode { get; set; }
        public List<string> Feedback { get; set; }
    }
}