﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool;
using AuthenticPhoto.Business.ProductTool.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Models
{
    public class PreferencesModel: ViewModelWithTranslations
    {
        List<UploadedImage> uploadedImages = new List<UploadedImage>();
        public List<UploadedImage> UploadedImages { get { return uploadedImages.OrderBy(img => img.DateAdded).ToList(); } set { this.uploadedImages = value; } }
        public string ActiveImageID { get; set; }
        public string OrderID { get; set; }

        public List<ProductFamily> ProductFamilies { get; set; }

        public UploadedImage GetActiveImage()
        {
            return uploadedImages.FirstOrDefault(i => i.ID == ActiveImageID);
        }
    }

}