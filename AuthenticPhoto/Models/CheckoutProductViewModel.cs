﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool.Entities;

namespace AuthenticPhoto.Models
{
    public class CheckoutProductViewModel: ViewModelWithTranslations
    {
        public CheckoutProduct CheckoutProduct { get; set; }

    }
}