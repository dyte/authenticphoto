﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.Authentication;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AuthenticPhoto.Models
{
    public class QuickRegisterViewModel: ViewModelWithTranslations
    {
        public AuthenticUser User { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessageResourceType = typeof(Resources.ErrorMessages), ErrorMessageResourceName = "AcceptTerms")]
        public bool AcceptedTerms { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ErrorMessages), ErrorMessageResourceName = "FirstNameRequired")]
        public string FirstName { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ErrorMessages), ErrorMessageResourceName = "FamilyNameRequired")]
        public string FamilyName { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ErrorMessages), ErrorMessageResourceName = "AddressRequired")]
        public string Address { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ErrorMessages), ErrorMessageResourceName = "PostalCodeRequired")]
        public string PostalCode { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ErrorMessages), ErrorMessageResourceName = "PlaceRequired")]
        public string Place { get; set; }
    }
}