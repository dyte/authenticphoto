﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Models
{
    public class TotalPriceDetailsRowViewModel: ViewModelWithTranslations
    {
        public UploadedImage Image { get; set; }
        public string OrderID { get; set; }

        public string LanguageCode { get; set; }

        public string GetSignatureDetailsString()
        {
            Signature signature = Image.Signature;

            string result =
            GetTranslation("ShowPreferences.Tabs.Signature." + signature.SignatureLocation.ToString()) + ", " +
            GetTranslation("ShowPreferences.Tabs.Signature." + signature.SignaturePosition.ToString()) + ", " +
            GetTranslation("ShowPreferences.Tabs.Signature." + signature.SignatureLabelSize.ToString()) + ", " +
            GetTranslation("ShowPreferences.Tabs.Signature." + signature.SignatureDelivery.ToString());
            return result;

        }
    }
}