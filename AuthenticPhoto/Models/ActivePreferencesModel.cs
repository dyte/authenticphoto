﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool;
using AuthenticPhoto.Business.ProductTool.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace AuthenticPhoto.Models
{
    public class FinishingModel: ViewModelWithTranslations
    {
        public static string OptionImagesPath = ConfigurationManager.AppSettings["OptionImagesPath"];

        public UploadedImage Image { get; set; }
        public List<ProductFamily> ProductFamilies { get; set; }
        public decimal TotalPrice { get; set; }
        public string OrderID { get; set; }

        public string LanguageCode { get; set; }

        public string GetOptionImagePath(int optionCategoryId, int optionId)
        {
            return OptionImagesPath + Convert.ToString(optionCategoryId) + "/" + optionId + ".jpg";
        }
        public string GetProductImagePath(int productId)
        {
            return OptionImagesPath + "products/" + Convert.ToString(productId) + ".jpg";
        }
        
        public IEnumerable<OptionType> GetNonEmptyOptionTypes()
        {
            return Image.SelectedProduct == null ? new List<OptionType>() : Image.SelectedProduct.OptionTypes.Where(ot => ot.OptionCategories.Count() > 0 || ot.OptionTypeType == "borders").OrderBy(ot => ot.Seq).ToList();
        }

        public string SignatureEnumToString(string val) //TEMP
        {
            switch (val)
            {
                case "OnBackBehindPlexi":
                    return "Handtekening op de Rug, achter de Diasec Plexi";
                case "OnBack":
                    return "Handtekening op de Rug, op de achterzijde";
                case "InImage":
                    return "Digitale Handtekening in het Beeld";
                case "BelowLeft":
                    return "Linksonder";
                case "BelowRight":
                    return "Rechtsonder";
                case "S105x42":
                    return "105 x 42 mm";
                case "S105x70":
                    return "105 x 70 mm";
                case "S200x143":
                    return "200 x 143 mm";
                case "SelfSignOnBackBehindPlexi":
                    return "Ik kom Zelf Signeren op de rug achter de Diasec Plexi";
                case "SelfSignOnBack":
                    return "Ik kom Zelf Signeren op de Achterzijde";
                case "SelfSignInImage":
                    return "Ik kom Zelf Signeren op de Voorzijde";
                case "UploadNow":
                    return "Ik wil de Handtekening nu uploaden";
                case "SendLater":
                    return "Ik wil de Handtekening later doorsturen naar planning@AuthenticPhoto.be";
            }
            return val;
        }

        public int GetActiveProductID() // in use in calculator
        {
            int id = 0;
            if (Image != null)
            {
                id = Image.SelectedProduct == null ? 0 : Image.SelectedProduct.ProductID;
            }
            return id;
        }

    }

    


}