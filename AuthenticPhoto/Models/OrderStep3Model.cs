﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool;
using AuthenticPhoto.Business.ProductTool.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace AuthenticPhoto.Models
{
    public class OrderStep3Model: ViewModelWithTranslations
    {
        public static string OptionImagesPath = ConfigurationManager.AppSettings["OptionImagesPath"];

        public string LanguageCode { get; set; }

        public Business.Authentication.AuthenticUser User { get; set; }
        public AuthenticOrder AuthenticOrder { get; set; }

        public List<CheckoutProduct> AvailableCheckoutProducts { get; set; }

        public string GetCheckoutOptionThumbPath(int copId)
        {
            return Path.Combine(OptionImagesPath, "checkoutproducts", Convert.ToString(copId) + ".jpg");
        }

        public string GetQtyOfCheckoutProduct(int ID)
        {
            CheckoutProduct product = AuthenticOrder.SelectedCheckoutProducts.FirstOrDefault(cop => cop.CheckoutProductID == ID);
            if (product == null)
            {
                return "0";
            } else
            {
                return Convert.ToString(product.Qty);
            }
        }
    }
}