﻿using Umbraco.Core.Models;
using Umbraco.Web;

namespace AuthenticPhoto.Models
{
    public class LoginPartialViewModel
    {
        public IPublishedContent Homepage { get; set; }
        public UmbracoContext UmbracoContext { get; set; }
    }
}