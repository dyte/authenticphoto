﻿using System.Collections.Generic;
using System.Globalization;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace AuthenticPhoto.Models
{
    public class UploadViewModel: RenderModel
    {
        public UploadViewModel() : this(new UmbracoHelper(UmbracoContext.Current).TypedContent(UmbracoContext.Current.PageId)) { }
        public UploadViewModel(IPublishedContent content, CultureInfo culture) : base(content, culture) { }
        public UploadViewModel(IPublishedContent content) : base(content) { }

        public string SessionID { get; set; }
        public string OrderID { get; set; }

    }
}