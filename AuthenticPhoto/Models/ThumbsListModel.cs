﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool;
using System.Collections.Generic;
using System.Linq;

namespace AuthenticPhoto.Models
{
    public class ThumbsListModel: ViewModelWithTranslations
    {
        List<UploadedImage> uploadedImages = new List<UploadedImage>();
        public List<UploadedImage> UploadedImages { get { return uploadedImages.OrderBy(img => img.DateAdded).ToList(); } set { this.uploadedImages = value; } }
        public string ActiveImageID { get; set; }
    }

}