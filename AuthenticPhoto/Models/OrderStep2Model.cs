﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool;
using AuthenticPhoto.Business.ProductTool.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Models
{
    public class OrderStep2Model: ViewModelWithTranslations
    {
        public AuthenticOrder AuthenticOrder { get; set; }

        public string ActiveImageID { get; set; }
        public List<ProductFamily> ProductFamilies { get; set; }
        public UploadedImage GetActiveImage()
        {
            return AuthenticOrder.Images.FirstOrDefault(i => i.ID == ActiveImageID);
        }
        public int GetActiveProductID()
        {
            int id = 0;
            UploadedImage firstImage = AuthenticOrder.Images.FirstOrDefault(i => i.ID == ActiveImageID);
            if(firstImage != null)
            {
                id = firstImage.SelectedProduct == null ? 0 : firstImage.SelectedProduct.ProductID;
            }
            return id;
        }
    }
}