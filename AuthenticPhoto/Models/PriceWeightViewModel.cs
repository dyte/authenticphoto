﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Models
{
    public class PriceWeightViewModel: ViewModelWithTranslations
    {
        public UploadedImage Image { get; set; }
        public decimal ImagePrice { get; set; }
        public decimal TotalPrice { get; set; }
        public double ImageWeight { get; set; }
        public double TotalWeight { get; set; }
        
    }
}