﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Models
{
    public class OrderSummaryViewModel:ViewModelWithTranslations
    {
        public AuthenticOrder Order { get; set; }

        public string InvoiceCountry { get; set; }

        public bool VatValid { get; set; }

        public decimal CalculateVAT(decimal totalPriceExclVatHandling, bool isTransport)
        {
            decimal vat = 0;
            if (!isTransport)
            {
                if(VatValid)
                {
                    string EuCountryCodes = ConfigurationManager.AppSettings["EUVatFreeCountryList"];
                    if (EuCountryCodes.Contains(InvoiceCountry))
                    {
                        return 0;
                    }
                }
                vat = Math.Round(totalPriceExclVatHandling * (decimal)0.21, 2);
            }            
            return vat;
        }
    }
}