﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.Authentication;

namespace AuthenticPhoto.Models
{
    public class EditUserViewModel: ViewModelWithTranslations
    {
        public AuthenticUser User { get; set; }
        public bool ConfirmRequired{ get; set; }
        public string FeedbackMessage { get; set; }

        public bool CheckoutContext { get; set; }

        public string ActiveCulture { get; set; }

        public string Birthday { get; set; }
    }
}