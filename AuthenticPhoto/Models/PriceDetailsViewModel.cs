﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool;

namespace AuthenticPhoto.Models
{
    public class PriceDetailsViewModel: ViewModelWithTranslations
    {
        public UploadedImage Image { get; set; }

        public string LanguageCode { get; set; }

        public string OrderID { get; set; }

        public enum PriceDetailsContext
        {
            Calculator, Order
        }

        public PriceDetailsContext Context { get; set; }

        public string GetSignatureDetailsString()
        {
            Signature signature = Image.Signature;

            string result =
            GetTranslation("ShowPreferences.Tabs.Signature." + signature.SignatureLocation.ToString()) + ", " +
            GetTranslation("ShowPreferences.Tabs.Signature." + signature.SignaturePosition.ToString()) + ", " +
            GetTranslation("ShowPreferences.Tabs.Signature." + signature.SignatureLabelSize.ToString()) + ", " +
            GetTranslation("ShowPreferences.Tabs.Signature." + signature.SignatureDelivery.ToString());
            return result;
            
        }
    }
}