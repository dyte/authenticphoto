﻿using AuthenticPhoto.Business.ProductTool.Entities;
using System.Collections.Generic;
using AuthenticPhoto.Business.ProductTool;
using System.Web;
using System.Linq;
using AuthenticPhoto.Business.Abstract;
using Umbraco.Core.Models;

namespace AuthenticPhoto.Models
{
    public class ProductsOverviewViewModel
    {
        public IPublishedContent ProductFamily { get; set; }
        public int ActiveProductId { get; set; }
    }
}