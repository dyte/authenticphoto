﻿using AuthenticPhoto.Business.Abstract;
using AuthenticPhoto.Business.ProductTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticPhoto.Models
{
    public class TotalPriceDetailsViewModel: ViewModelWithTranslations
    {
        public AuthenticOrder Order { get; set; }
        public bool CheckoutPageView { get; set; }

        public decimal CalculateVAT(decimal price)
        {
            return Math.Round(price * (decimal)0.21, 2);
        }
    }
}